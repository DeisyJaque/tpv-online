package com.saraworld.tpvonline.controller;

import static java.lang.Math.toIntExact;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
/*ej empleados*/
/*import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;*/
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saraworld.tpvonline.model.MarEmpSucUsuRol;
import com.saraworld.tpvonline.model.Marca;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.model.Usuario;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saraworld.tpvonline.model.BitaUserTipoAccion;
import com.saraworld.tpvonline.model.Bitacora;
import com.saraworld.tpvonline.model.Catalogo;
import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.FormaPago;
import com.saraworld.tpvonline.service.MarEmpSucUsuRolService;
import com.saraworld.tpvonline.service.RolService;

import com.saraworld.tpvonline.service.UsuarioService;
import com.saraworld.tpvonline.util.PageRender;
import com.saraworld.tpvonline.service.BitacoraService;
import com.saraworld.tpvonline.service.BitaUserTipoAccionService;
import com.saraworld.tpvonline.service.TipoAccionService;


@Controller 

public class BitacoraController {

	@Autowired
	private UsuarioService userService;
	@Autowired
	private RolService rolService;
	@Autowired
	private MarEmpSucUsuRolService marEmpSucUsuRolService;
	@Autowired
	private BitacoraService bitacoraService;
	@Autowired
	private TipoAccionService tipoaccionService;
	@Autowired
	private BitaUserTipoAccionService bitausertipoaccionService;

	

	@RequestMapping(value = "/admin/bitacora", method=RequestMethod.GET )
	public ModelAndView listar(RedirectAttributes flash,@RequestParam(name = "page", defaultValue="0") int page) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		boolean userbitatipoaccion= bitausertipoaccionService.verifyUserInTipoAccion(user.getId());
		Long bitauser;
		boolean userini,usercierr,userrecup;
	
					
			if (rolAdmin != null) {
				List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
				List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
				
					
				if (modulosPermisos.contains("Admin - Bitacora")) {
					if(userbitatipoaccion==true) {
						bitauser = bitausertipoaccionService.IdUserBitaAccion(user.getId());
						modelAndView.addObject("user", user );
						modelAndView.addObject("modulosPermisos", modulosPermisos);
						modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
						modelAndView.addObject("bitausertipoaccion", bitausertipoaccionService.findOne(bitauser));
						
						List<MarEmpSucUsuRol> empresas= marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
						Integer countEmp= empresas.size();
						List <Bitacora> bitacorabyemp= new ArrayList<>();
						List <Bitacora> bitacoraAll= new ArrayList<>();
						List <Usuario> users= new ArrayList<>();
						List <Usuario> uerbyemp= new ArrayList<>();;
						for (int i = 0; i < countEmp; i++) {
							uerbyemp.addAll(marEmpSucUsuRolService.findListUsuariosByIdOfEmpresaforBita(empresas.get(i).getEmpresa().getId()));
							
						}
						 Map<Long, Usuario> mapPersonas = new HashMap<Long, Usuario>(uerbyemp.size());
						 for(Usuario p : uerbyemp) {
							 mapPersonas.put(p.getId(), p);
							 }
						 for(Entry<Long, Usuario> p : mapPersonas.entrySet()) {
							 users.add(p.getValue());
							
						 }
						 for (int i = 0; i < uerbyemp.size(); i++) {
							 bitacorabyemp.addAll(bitacoraService.findListBitacoraByUserId(uerbyemp.get(i).getId()));
							
							}
						 Map<Long, Bitacora> mapBitacora = new HashMap<Long, Bitacora>(bitacorabyemp.size());
						 for(Bitacora b : bitacorabyemp) {
							 mapBitacora.put(b.getId(), b);
							 }
						 for(Entry<Long, Bitacora> b : mapBitacora.entrySet()) {
							 bitacoraAll.add(b.getValue());
							
						 }
						 List <Bitacora> bitacoraCierr= bitacoraAll;
						 
						Page<Bitacora> bitacora = convertListToPage(page, bitacoraAll);
						Page<Bitacora> bitacoracierre = convertListToPage(page, bitacoraCierr);
						PageRender<Bitacora> pageRender = new PageRender<>("/admin/bitacora",bitacora);
						PageRender<Bitacora> pageRendercierr = new PageRender<>("/admin/bitacora",bitacoracierre);
						modelAndView.addObject("bitacoraInicio", bitacora);
						modelAndView.addObject("bitacoraCierr", bitacoracierre);
						modelAndView.addObject("page", pageRender);
						modelAndView.addObject("pagecierr", pageRendercierr);
						userini=bitacoraService.findListBitacoraByIdBitacoraInicio((long) 1).isEmpty();
						usercierr=bitacoraService.findListBitacoraByIdBitacoraInicio((long) 2).isEmpty();
						userrecup=bitacoraService.findListBitacoraByIdBitacoraInicio((long) 3).isEmpty();
						modelAndView.addObject("bitacoraini", userini);
						modelAndView.addObject("bitacierr", usercierr);
						modelAndView.addObject("bitarecup", userrecup);
						
						modelAndView.setViewName("administracion/bitacora/listar");
						return modelAndView;
						
					}else {
					modelAndView.addObject("user", user );
					modelAndView.addObject("modulosPermisos", modulosPermisos);
					modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
					modelAndView.addObject("bitacoraInicio", bitacoraService.findAll());
					modelAndView.setViewName("administracion/bitacora/listar");
					return modelAndView;
				}
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/bitacora/crear", method=RequestMethod.GET )
	public ModelAndView paginaCrearBitacora(@ModelAttribute @Valid BitaUserTipoAccion bitausertipoaccion,RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		boolean userbitatipoaccion= bitausertipoaccionService.verifyUserInTipoAccion(user.getId());
		Long idbita;
		if (rolAdmin != null) {
			
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			if(userbitatipoaccion==false) {
				
				bitausertipoaccion.setUsuario(user);
				bitausertipoaccionService.saveBitaAccion(bitausertipoaccion);
				 idbita = bitausertipoaccion.getId();
			}else {
				 idbita = bitausertipoaccionService.IdUserBitaAccion(user.getId());
				 
				 
			}
			
			
			if (modulosPermisos.contains("Admin - Bitacora")) {
				modelAndView.addObject("user", user );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("bitausertipoaccion", bitausertipoaccionService.findOne(idbita));
				modelAndView.addObject("tipoaccion", tipoaccionService.findAll());
				modelAndView.setViewName("administracion/bitacora/crear");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	@RequestMapping(value="/admin/bitacora/crear", method = RequestMethod.POST)
	public ModelAndView crear( @ModelAttribute @Valid BitaUserTipoAccion bitausertipoaccion,  BindingResult result, ModelAndView modelAndView, RedirectAttributes flash,SessionStatus status ){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
	
		boolean userini,usercierr,userrecup;
		modelAndView.addObject("user", user);
        modelAndView.addObject("modulosPermisos", modulosPermisos);
        modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		
		if(result.hasErrors()) {
			modelAndView.setViewName("administracion/bitacora/crear");
	        modelAndView.addObject("bitausertipoaccion", bitausertipoaccion);
	        return modelAndView;
		}
		
		userini=bitacoraService.findListBitacoraByIdBitacoraInicio((long) 1).isEmpty();
		usercierr=bitacoraService.findListBitacoraByIdBitacoraInicio((long) 2).isEmpty();
		userrecup=bitacoraService.findListBitacoraByIdBitacoraInicio((long) 3).isEmpty();
		
		if(userini==true) {
			bitausertipoaccion.setIdInicio(null);
			//flash.addFlashAttribute("warning", "No hay nada para listar");
			
		}else {
			modelAndView.addObject("bitacoraini", userini);
			
		}
		if(usercierr==true) {
			bitausertipoaccion.setIdCierre(null);
			//flash.addFlashAttribute("warning", "No hay nada para listar");
			
		}else {
			modelAndView.addObject("bitacierr", usercierr);
			
		}
		if(userrecup==true) {
			bitausertipoaccion.setIdRecup(null);
			//flash.addFlashAttribute("warning", "No hay nada para listar");
		
		}else {
			modelAndView.addObject("bitarecup", userrecup);
			
		}
		bitausertipoaccionService.saveBitaAccion(bitausertipoaccion);
		status.setComplete();
		modelAndView.setViewName("redirect:/admin/bitacora");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/bitacora/{id}", method=RequestMethod.GET)
	public ModelAndView verEmpresa(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		
		if (rolAdmin!=null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			
			//Solo permite consultar empresas a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("4")) {
				Bitacora registro = bitacoraService.findOne(id);
				Long iduser= bitacoraService.findBitacoraByIdUsuario(id);	
				
				if (registro == null) {
					flash.addFlashAttribute("error", "El registro de bitácora que desea ver no existe");
					modelAndView.setViewName("redirect:/admin/bitacora");
					return modelAndView;
				}
				
				//Datos del usuario logueado en el sistema
				modelAndView.addObject("user", user);
				modelAndView.addObject("rol", rolAdmin.getRol() );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				
				//Datos del registro consultado

				
				MarEmpSucUsuRol rolAdminVer = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(iduser);
				List<Marca> marcasclean = new ArrayList<Marca> ();
				 
				List<Marca> marcas = marEmpSucUsuRolService.findListMarcasByIdOfUser(iduser);
				Map<Long, Marca> mapMarcas = new HashMap<Long, Marca>(marcas.size());
				List<List<Empresa>> listaEmpresaPorMarca = new ArrayList<List<Empresa>> ();
				List<MarEmpSucUsuRol> empresasysucursales= marEmpSucUsuRolService.findSucursalAndEmpresaByIdOfUsuario(iduser);
				List<List<Sucursal>> listaSucursalesPorEmpresa = new ArrayList<List<Sucursal>> ();
				List<Empresa> empresas = marEmpSucUsuRolService.findListEmpresasByIdOfUsuario(iduser);
				List<Empresa> empresasPorMarca = new ArrayList<Empresa> ();
				List<Sucursal> sucursalesbyuser = marEmpSucUsuRolService.findListSucursalesByIdAndRolOfUsuario(iduser, rolAdminVer.getRol().getId());
				Map<Long,Long> listasucursales = new  HashMap<Long, Long>(sucursalesbyuser.size());
				List<Sucursal> sucursalesForIdUser = marEmpSucUsuRolService.findListSucursalesByIdAndRolOfUsuario(iduser, rolAdminVer.getRol().getId());
				for(Marca marca : marcas) {
				empresasPorMarca = marEmpSucUsuRolService.findListEmpresasByIdUsuarioAndByIdMarca(iduser, marca.getId());
					;
					
					mapMarcas.put(marca.getId(), marca);
					listaEmpresaPorMarca.add(empresasPorMarca);
					for(Empresa empresa : empresas) {
						
						List<Sucursal> sucursalesDeEmpresa = marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(empresa.getId());
						 sucursalesForIdUser = marEmpSucUsuRolService.findListSucursalesByIdAndRolOfUsuario(iduser, rolAdminVer.getRol().getId());
						List<Sucursal> sucursaless = new ArrayList<Sucursal> ();
						for(Sucursal sucursal : sucursalesForIdUser ) {
							if( sucursalesDeEmpresa.contains(sucursal) ) {
								sucursaless.add(sucursal);
								listasucursales.put(empresa.getId(), sucursal.getId());
							}
						}
						
						listaSucursalesPorEmpresa.add(sucursaless);
					
					}
					
				}
				 
				 for(Entry<Long, Marca> m : mapMarcas.entrySet()) {
					 marcasclean.add(m.getValue());
				 }
				
			
				modelAndView.addObject("bitacora", registro);
				modelAndView.addObject("empresasbymarca",listaEmpresaPorMarca );
				modelAndView.addObject("sucursales",sucursalesForIdUser );
				modelAndView.addObject("empresas",empresasysucursales );
				modelAndView.addObject("listaSucursalesPorEmpresa",listaSucursalesPorEmpresa);
				modelAndView.addObject("marcas", marcasclean);
				modelAndView.setViewName("administracion/bitacora/ver");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	@RequestMapping(value="/admin/bitacora/{id}/eliminar")
	public ModelAndView eliminarEmpresa(@PathVariable(value="id") Long id , RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		//boolean userini;
		if (rolAdmin!=null) {
			
			//SOlo permite eliminar empresas a usuarios que tenga el rol con ID:1 que es el rol "admin"
			if (rolAdmin.getRol().getId()==1 ) {
				Bitacora bitacora = bitacoraService.findOne(id);
				Long bitauser=bitacoraService.findBitacoraByIdUsuario(id);
				BitaUserTipoAccion tipouser= bitausertipoaccionService.findUserById(bitauser);
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				
				if (bitacora == null) {
					flash.addFlashAttribute("error", "El registro que desea eliminar no existe");
					modelAndView.setViewName("redirect:/admin/empresas");
					return modelAndView;
				}
				
				flash.addFlashAttribute("success","El registro de la bitácora del usuario '" + bitacora.getUsuario().getNombre() + "' cuya operación fue registrada con día/hora'"+bitacora.getFecha_creacion() +"'fue eliminada con éxito'");
				Integer lenini = bitacoraService.findListBitacoraByIdBitacoraInicio((long) 1).size();
				Integer lencierr = bitacoraService.findListBitacoraByIdBitacoraInicio((long) 2).size();
				Integer lenrecup = bitacoraService.findListBitacoraByIdBitacoraInicio((long) 3).size();
				bitacora.setBorradolog(true);
				bitacora.setBorrado(timestamp);
				bitacoraService.saveBitacora(bitacora);
				
				
				if(lenini==1 && bitacora.getTipo_accion().getId()==1) {
					
					tipouser.setIdInicio(null);
					bitausertipoaccionService.saveBitaAccion(tipouser);
				
				}
				if(lencierr==1 && bitacora.getTipo_accion().getId()==2) {
					
					tipouser.setIdCierre(null);
					bitausertipoaccionService.saveBitaAccion(tipouser);
				
				}
				if(lenrecup==1 && bitacora.getTipo_accion().getId()==3) {
				
					tipouser.setIdRecup(null);
					bitausertipoaccionService.saveBitaAccion(tipouser);
				
				}
			
				modelAndView.setViewName("redirect:/admin/bitacora");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
		
	}
	
	private Page<Bitacora> convertListToPage(int page, List<Bitacora> listabitacora) {
		Pageable pageable = new PageRequest(page, 10);
		
		//Porción de código que permite convertir un objeto List a un objeto Page
		int inicio = pageable.getOffset();
		int fin = (inicio + pageable.getPageSize()) > listabitacora.size() ? listabitacora.size() : (inicio + pageable.getPageSize());
		Page<Bitacora> bitacora = new PageImpl<Bitacora>(listabitacora.subList(inicio, fin), pageable, listabitacora.size());
		
		return bitacora;
	}
	

	
	@GetMapping(value = "/admin/bitacora/buscar/{nombre}")
	public @ResponseBody String buscarFormasPago(@PathVariable(value = "nombre") String nombre) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		List<MarEmpSucUsuRol> empresas= marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
		Integer countEmp= empresas.size();
		List <Bitacora> bitacorabyemp= new ArrayList<>();
		List <Bitacora> bitacoraAll= new ArrayList<>();
		List <Usuario> users= new ArrayList<>();
		List <Usuario> uerbyemp= new ArrayList<>();;
		for (int i = 0; i < countEmp; i++) {
			uerbyemp.addAll(marEmpSucUsuRolService.findListUsuariosByIdOfEmpresaforBita(empresas.get(i).getEmpresa().getId()));
			
		}
		 Map<Long, Usuario> mapPersonas = new HashMap<Long, Usuario>(uerbyemp.size());
		 for(Usuario p : uerbyemp) {
			 mapPersonas.put(p.getId(), p);
			 }
		 for(Entry<Long, Usuario> p : mapPersonas.entrySet()) {
			 users.add(p.getValue());
			
		 }
		 for (int i = 0; i < uerbyemp.size(); i++) {
			 bitacorabyemp.addAll(bitacoraService.findListBitacoraByUserId(uerbyemp.get(i).getId()));
			
			}
		 Map<Long, Bitacora> mapBitacora = new HashMap<Long, Bitacora>(bitacorabyemp.size());
		 for(Bitacora b : bitacorabyemp) {
			 mapBitacora.put(b.getId(), b);
			 }
		 for(Entry<Long, Bitacora> b : mapBitacora.entrySet()) {
			 bitacoraAll.add(b.getValue());
			
		 }
		List<Bitacora> bitacora = bitacoraAll;
		
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		String json = objGson.toJson(bitacora);
		return json;

	}
	
	
	@GetMapping(value = "/admin/bitacora/ajax")
	@ResponseBody
	public ModelAndView listarBitacora() {
		ModelAndView modelAndView = new ModelAndView();
		List<Bitacora> bitacora = new ArrayList<Bitacora> ();
		
		
		//Para ordenar los registros del listado 
		Collections.sort(bitacora, (t1, t2) -> toIntExact(t1.getId()) - toIntExact(t2.getId()));

		modelAndView.addObject("bitacora",bitacora);
		modelAndView.setViewName("administracion/empresas/FormasPago");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/bitacora/cierre", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getCierre(@RequestParam(name = "pagecierr", defaultValue="0") int page){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		List <Bitacora> bitacoraAll= new ArrayList<>();
		Long bitauser = bitausertipoaccionService.IdUserBitaAccion(user.getId());
		boolean userbitatipoaccion= bitausertipoaccionService.verifyUserInTipoAccion(user.getId());
		if(userbitatipoaccion==true) {
			
			List<MarEmpSucUsuRol> empresas= marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
			Integer countEmp= empresas.size();
			List <Bitacora> bitacorabyemp= new ArrayList<>();
			
			List <Usuario> users= new ArrayList<>();
			List <Usuario> uerbyemp= new ArrayList<>();;
			for (int i = 0; i < countEmp; i++) {
				uerbyemp.addAll(marEmpSucUsuRolService.findListUsuariosByIdOfEmpresaforBita(empresas.get(i).getEmpresa().getId()));
				
			}
			 Map<Long, Usuario> mapPersonas = new HashMap<Long, Usuario>(uerbyemp.size());
			 for(Usuario p : uerbyemp) {
				 mapPersonas.put(p.getId(), p);
				 }
			 for(Entry<Long, Usuario> p : mapPersonas.entrySet()) {
				 users.add(p.getValue());
				
			 }
			 for (int i = 0; i < uerbyemp.size(); i++) {
				 bitacorabyemp.addAll(bitacoraService.findListBitacoraByUserId(uerbyemp.get(i).getId()));
				
				}
			 Map<Long, Bitacora> mapBitacora = new HashMap<Long, Bitacora>(bitacorabyemp.size());
			 for(Bitacora b : bitacorabyemp) {
				 mapBitacora.put(b.getId(), b);
				 }
			 for(Entry<Long, Bitacora> b : mapBitacora.entrySet()) {
				 bitacoraAll.add(b.getValue());
				
			 }
		
		}
		Page<Bitacora> bitacoraCierr = convertListToPage(page, bitacoraAll);
		PageRender<Bitacora> pageRender = new PageRender<>("/admin/bitacora",bitacoraCierr);
		modelAndView.addObject("bitausertipoaccion", bitausertipoaccionService.findOne(bitauser));
		modelAndView.addObject("bitacoraCierr", bitacoraCierr);
		modelAndView.addObject("pagecierr", pageRender);
		modelAndView.setViewName("administracion/bitacora/listaCierre");
		return modelAndView;
		
	}
	@RequestMapping(value = "/admin/bitacora/inicio", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getInicio(@RequestParam(name = "page", defaultValue="0") int page){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		List <Bitacora> bitacoraAll= new ArrayList<>();
		Long bitauser = bitausertipoaccionService.IdUserBitaAccion(user.getId());
		boolean userbitatipoaccion= bitausertipoaccionService.verifyUserInTipoAccion(user.getId());
		if(userbitatipoaccion==true) {
			
			List<MarEmpSucUsuRol> empresas= marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
			Integer countEmp= empresas.size();
			List <Bitacora> bitacorabyemp= new ArrayList<>();
			
			List <Usuario> users= new ArrayList<>();
			List <Usuario> uerbyemp= new ArrayList<>();;
			for (int i = 0; i < countEmp; i++) {
				uerbyemp.addAll(marEmpSucUsuRolService.findListUsuariosByIdOfEmpresaforBita(empresas.get(i).getEmpresa().getId()));
				
			}
			 Map<Long, Usuario> mapPersonas = new HashMap<Long, Usuario>(uerbyemp.size());
			 for(Usuario p : uerbyemp) {
				 mapPersonas.put(p.getId(), p);
				 }
			 for(Entry<Long, Usuario> p : mapPersonas.entrySet()) {
				 users.add(p.getValue());
				
			 }
			 for (int i = 0; i < uerbyemp.size(); i++) {
				 bitacorabyemp.addAll(bitacoraService.findListBitacoraByUserId(uerbyemp.get(i).getId()));
				
				}
			 Map<Long, Bitacora> mapBitacora = new HashMap<Long, Bitacora>(bitacorabyemp.size());
			 for(Bitacora b : bitacorabyemp) {
				 mapBitacora.put(b.getId(), b);
				 }
			 for(Entry<Long, Bitacora> b : mapBitacora.entrySet()) {
				 bitacoraAll.add(b.getValue());
				
			 }
		
		}
		Page<Bitacora> bitacoraInicio = convertListToPage(page, bitacoraAll);
		PageRender<Bitacora> pageRender = new PageRender<>("/admin/bitacora",bitacoraInicio);
		modelAndView.addObject("bitausertipoaccion", bitausertipoaccionService.findOne(bitauser));
		modelAndView.addObject("bitacoraInicio", bitacoraInicio);
		modelAndView.addObject("page", pageRender);
		modelAndView.setViewName("administracion/bitacora/listaInicio");
		return modelAndView;
		
	}
}
