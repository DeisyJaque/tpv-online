package com.saraworld.tpvonline.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
/*ej empleados*/
/*import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;*/
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.Attribute;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.PrinterInfo;
import javax.print.attribute.standard.PrinterIsAcceptingJobs;
import javax.print.attribute.standard.PrinterLocation;
import javax.print.attribute.standard.PrinterMakeAndModel;
import javax.print.attribute.standard.PrinterState;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saraworld.tpvonline.model.MarEmpSucUsuRol;
import com.saraworld.tpvonline.model.Usuario;
import com.saraworld.tpvonline.model.Venta;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saraworld.tpvonline.model.Agenda;
import com.saraworld.tpvonline.model.Articulo;
import com.saraworld.tpvonline.model.Cliente;
import com.saraworld.tpvonline.model.Coleccion;
import com.saraworld.tpvonline.model.Marca;
import com.saraworld.tpvonline.model.Movimiento;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.Factura;
import com.saraworld.tpvonline.model.Familia;
import com.saraworld.tpvonline.model.Lotes;
import com.saraworld.tpvonline.model.Gastos;
import com.saraworld.tpvonline.model.Pago;
import com.saraworld.tpvonline.model.FormaPago;
import com.saraworld.tpvonline.service.ClienteService;
import com.saraworld.tpvonline.service.EmpresaService;
import com.saraworld.tpvonline.service.FacturaService;
import com.saraworld.tpvonline.service.MarEmpSucUsuRolService;
import com.saraworld.tpvonline.service.MarcaService;
import com.saraworld.tpvonline.service.RolService;
import com.saraworld.tpvonline.service.SucursalService;
import com.saraworld.tpvonline.service.UsuarioService;
import com.saraworld.tpvonline.service.VentaService;
import com.saraworld.tpvonline.service.PagoService;
import com.saraworld.tpvonline.service.GastosService;
import com.saraworld.tpvonline.service.LoteService;
import com.saraworld.tpvonline.service.MovimientoService;
import com.saraworld.tpvonline.service.FormaPagoService;

@Controller 

public class FacturaController {

	@Autowired
	private UsuarioService userService;
	@Autowired
	private RolService rolService;
	@Autowired
	private MarEmpSucUsuRolService marEmpSucUsuRolService;
	@Autowired
	private SucursalService sucursalService;
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private FacturaService facturaService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private VentaService ventaService;
	@Autowired
	private PagoService pagoService;
	@Autowired
	private FormaPagoService formapagoService;
	@Autowired
	private LoteService loteService;
	@Autowired
	private GastosService gastosService;
	@Autowired
	private MovimientoService movimientoService;
	@Value("${spring.datasource.url}")
	private String urlBD;
	
	@Value("${spring.datasource.username}")
	private String usuarioBD;

	@Value("${spring.datasource.password}")
	private String passBD;
	
	@RequestMapping(value = "/tpv/factura", method=RequestMethod.GET )
	public ModelAndView listar(RedirectAttributes flash, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		boolean nuevoUsuario = marEmpSucUsuRolService.verifyIfIsNewUsuario(user.getId());
		
		if (rolAdmin != null && nuevoUsuario == false ) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
	
			List<Pago> pago = new ArrayList <Pago>();
			String query= (String)request.getSession().getAttribute("ListConsultados");	
			if(query!=null) {
			try {
				Connection conexion;
				Statement consulta;            
				ResultSet data;

				request.getSession().setAttribute("ListConsultados",query);
				// Conectamos con la base de datos			
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
				    
				consulta = conexion.createStatement();
				
					
				
				data = consulta.executeQuery(query);
				
				while (data.next()) {
					Pago pagos = new Pago();
					Long Venta;
					Long FormaPago;
					pagos.setId(data.getLong("id"));
					pagos.setImporte( data.getDouble("importe") );
					pagos.setFecha(data.getTimestamp("fecha"));
					pagos.setHora(data.getTimestamp("hora"));
					FormaPago =data.getLong("forma_pago_id");
				
					
					if( !data.wasNull() ) {
						pagos.setForma_pago(formapagoService.findOne(FormaPago));
					}
					Venta = data.getLong("venta_id");
					
					if( !data.wasNull() ) {
						pagos.setVenta( ventaService.findOne(Venta) );
					}
					
					pago.add(pagos);
			        
			    }
				
				data.close();
				
				
			}catch(Exception e) {
				
				System.out.println(e);
				
			}
			modelAndView.addObject("ventas",pago);
			}else {
				modelAndView.addObject("ventas",null);
			}
			
		
		

			modelAndView.setViewName("tpv/factura/listados");
			return modelAndView;
			
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	
	
	@RequestMapping(value = "/tpv/factura/gastos", method=RequestMethod.GET )
	public ModelAndView gastos(RedirectAttributes flash, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		boolean nuevoUsuario = marEmpSucUsuRolService.verifyIfIsNewUsuario(user.getId());
		
		if (rolAdmin != null && nuevoUsuario == false ) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
			List<Gastos> gasto = new ArrayList <Gastos>();
			String query= (String)request.getSession().getAttribute("ListConsultadosGastos");	
			Double sumBase=0.00;
			Double sumIva=0.00;
			if(query!=null) {
			try {
				Connection conexion;
				Statement consulta;            
				ResultSet data;

				request.getSession().setAttribute("ListConsultadosGastos",query);
				// Conectamos con la base de datos			
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
				    
				consulta = conexion.createStatement();
				
					
				
				data = consulta.executeQuery(query);
				
				while (data.next()) {
					Long marc_id;
					Long emp_id;
					Long suc_id;
					Long user_id;
					Gastos gastos = new Gastos();
					gastos.setId(data.getLong("id"));
					gastos.setNro_Factura(data.getString("nro_factura"));
					gastos.setFecha(data.getTimestamp("fecha"));
					gastos.setCIF(data.getString("cif"));
					gastos.setComentario(data.getString("comentario"));
					gastos.setConcepto(data.getString("concepto"));
					gastos.setFecha_pago(data.getTimestamp("fecha_pago"));
					gastos.setBase_imponible(data.getDouble("base_imponible"));
					gastos.setAnular(data.getBoolean("anular"));
					gastos.setPorc_deduccion(data.getDouble("porc_deduccion"));
					gastos.setIva(data.getDouble("iva"));
					gastos.setTipo_gasto(data.getString("tipo_gasto"));
					marc_id = data.getLong("marca_id");
					emp_id=data.getLong("empresa_id");
					suc_id=data.getLong("sucursal_id");
					user_id=data.getLong("usuario_id");
					Double base=data.getDouble("base_imponible");
					if(base!=null) {
						sumBase=sumBase+base;
					}
					Double iva=data.getDouble("iva");
					if(iva!=null) {
						sumIva=sumIva+iva;
					}
					Marca marca_gasto= marcaService.findOne(marc_id);
					if( !data.wasNull() ) {
						gastos.setMarca(marca_gasto);
					}
					
					Empresa empre_gasto= empresaService.findOne(emp_id);
					if( !data.wasNull() ) {
						gastos.setEmpresa(empre_gasto);
					}
					
					Sucursal sucur_gasto= sucursalService.findOne(suc_id);
					if( !data.wasNull() ) {
						gastos.setSucursal(sucur_gasto);
						
					}
				
					Usuario us= userService.findOne(user_id);
					if( !data.wasNull() ) {
						gastos.setEmpleado(us);
						
					}
					
					gasto.add(gastos);
			        
			    }
				
				data.close();
				
				
			}catch(Exception e) {
				
				System.out.println(e);
				
			}
			modelAndView.addObject("gastoslist",gasto);
			modelAndView.addObject("base",sumBase);
			modelAndView.addObject("iva",sumIva);
			}else {
				modelAndView.addObject("gastoslist",gastosService.GastosBySucursal(idSucursal, idEmpresa, idMarca));
			}
			
			if(movimientoService.findMovimientosByIdSucursal(idSucursal,idEmpresa,idMarca).size()==0) {
				modelAndView.addObject("movimientos",null);
			}else {
			modelAndView.addObject("movimientos", movimientoService.findMovimientosByIdSucursal(idSucursal,idEmpresa,idMarca));
			}
			String fechaDesde = (String)request.getSession().getAttribute("filtroFechaDesde");
			String fechaHasta = (String)request.getSession().getAttribute("filtroFechaHasta");
			Date date = new Date(Calendar.getInstance().getTime().getTime());
			modelAndView.addObject("totalGastos", movimientoService.findTotalMovimientosEntradaByIdSucursalAndFechasTotales(idSucursal,date.toString(),date.toString(),idMarca,idEmpresa));

			modelAndView.setViewName("tpv/factura/gastos");
			return modelAndView;
			
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	@GetMapping(value="/tpv/factura/gastos/crear")
	@ResponseBody
	public  ModelAndView crear(HttpServletRequest request,  ModelAndView modelAndView,
			@RequestParam String nro_factura, @RequestParam String fecha_pago, @RequestParam String cif, @RequestParam String proveedor, 
			@RequestParam String tipo_gasto, @RequestParam String concepto ,@RequestParam Double base_imponible, @RequestParam Double iva, 
			@RequestParam Double porc_deduccion, @RequestParam String comentario,@RequestParam Long empleado,  BindingResult result, RedirectAttributes flash ,SessionStatus status){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
		Empresa empresa = empresaService.findOne(idEmpresa);
		Marca marca = marcaService.findOne(idMarca);
		Sucursal sucursal = sucursalService.findOne(idSucursal);
		Gastos gastos = new Gastos();
		boolean flagError=false;
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		SimpleDateFormat formatoFechaHora = new SimpleDateFormat("yyyy-MM-dd");
	
		List<String> codeResponde = new ArrayList<String>();;
		String[] fechaPago = fecha_pago.split("/");
		try {
			java.util.Date fecha_pago_gastos = formatoFechaHora.parse( fechaPago[2] +"-"+ fechaPago[1]+"-"+fechaPago[0] ) ;
	
			gastos.setFecha_pago(fecha_pago_gastos);
			
		
			
		} catch (ParseException e) {
			e.printStackTrace();
			codeResponde.add( e.toString() );
			
		}
		
		Usuario empselec= userService.findOne(empleado);
		gastos.setFecha(date);
		gastos.setEmpleado(empselec);
		gastos.setMarca(marca);
		gastos.setEmpresa(empresa);
		gastos.setSucursal(sucursal);
		gastos.setFecha_creacion(date);
		gastos.setNro_Factura(nro_factura);
		gastos.setCIF(cif);
		gastos.setProveedor(proveedor);
		gastos.setConcepto(concepto);
		gastos.setIva(iva);
		gastos.setBase_imponible(base_imponible);
		gastos.setPorc_deduccion(porc_deduccion);
		gastos.setTipo_gasto(tipo_gasto);
		gastos.setComentario(comentario);
		
		gastosService.saveGastos(gastos);
		
		modelAndView.addObject("user", user);
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
		modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		if(result.hasErrors() || flagError==true) {
			
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
			modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.setViewName("tpv/factura/gastos");
			return modelAndView;
		}
			
			String mensajeFlash = "El  gasto fue creado con éxito";	
			status.setComplete();
			flash.addFlashAttribute("success",mensajeFlash);
			
			modelAndView.setViewName("redirect:/tpv/factura/gastos");
		
		
		
		return modelAndView;
	}
	
	
	
	@RequestMapping(value="/tpv/factura/gastos/guardar")
	@ResponseBody
	public  ModelAndView guardarGastos(HttpServletRequest request,  ModelAndView modelAndView,
			@RequestParam Long id,@RequestParam String nro_factura, @RequestParam String fecha_pago, @RequestParam String cif, @RequestParam String proveedor, 
			@RequestParam String tipo_gasto, @RequestParam String concepto ,@RequestParam Double base_imponible, @RequestParam Double iva, 
			@RequestParam Double porc_deduccion, @RequestParam String comentario,@RequestParam Long empleado,  BindingResult result, RedirectAttributes flash ,SessionStatus status){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
		Empresa empresa = empresaService.findOne(idEmpresa);
		Marca marca = marcaService.findOne(idMarca);
		Sucursal sucursal = sucursalService.findOne(idSucursal);
		Gastos gastos = gastosService.findOne(id);
		boolean flagError=false;
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		SimpleDateFormat formatoFechaHora = new SimpleDateFormat("yyyy-MM-dd");
	
		List<String> codeResponde = new ArrayList<String>();;
		String[] fechaPago = fecha_pago.split("/");
		try {
			java.util.Date fecha_pago_gastos = formatoFechaHora.parse( fechaPago[2] +"-"+ fechaPago[1]+"-"+fechaPago[0] ) ;
	
			gastos.setFecha_pago(fecha_pago_gastos);
			
		
			
		} catch (ParseException e) {
			e.printStackTrace();
			codeResponde.add( e.toString() );
			
		}
		
		Usuario empselec= userService.findOne(empleado);
		
		gastos.setFecha(date);
		gastos.setEmpleado(empselec);
		gastos.setNro_Factura(nro_factura);
		gastos.setCIF(cif);
		gastos.setProveedor(proveedor);
		gastos.setConcepto(concepto);
		gastos.setIva(iva);
		gastos.setBase_imponible(base_imponible);
		gastos.setPorc_deduccion(porc_deduccion);
		gastos.setTipo_gasto(tipo_gasto);
		gastos.setComentario(comentario);
		
		gastosService.saveGastos(gastos);
		
		modelAndView.addObject("user", user);
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
		modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		if(result.hasErrors() || flagError==true) {
			
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
			modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.setViewName("tpv/factura/gastos");
			return modelAndView;
		}
			
			String mensajeFlash = "El  gasto fue creado con éxito";	
			status.setComplete();
			flash.addFlashAttribute("success",mensajeFlash);
			
			modelAndView.setViewName("redirect:/tpv/factura/gastos");
		
		
		
		return modelAndView;
	}
	
	@RequestMapping(value="/tpv/factura/generar/ajax/{valores}")
	@ResponseBody
	public ModelAndView GenerarFacturas(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "valores") List<String> valores,BindingResult result,  RedirectAttributes flash ,SessionStatus status) throws ParseException {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
		Empresa empresa = empresaService.findOne(idEmpresa);
		Marca marca = marcaService.findOne(idMarca);
		Sucursal sucursal = sucursalService.findOne(idSucursal);
		
		modelAndView.addObject("user", user);
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
		modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		boolean flagError=false;
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		Integer count=0;
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		Set<Factura> facturas = new HashSet<Factura>(); 
		Double total_pvp=0.00;
		Lotes lote= new Lotes();
		lote.setEmpresa(empresa);
		lote.setFecha_creacion(date);
		lote.setMarca(marca);
		lote.setSucursal(sucursal);
		lote.setEmpleado(user);
		lote.setPresentado(false);
		
		lote.setAnular(false);
	if(loteService.findAll().size()==0) {
		lote.setId_Lote("Id_"+1);
	}else {
		lote.setId_Lote("Id_"+loteService.findAll().size()+1);
	}
		Double sumPagado=0.00;
		Double ivas=0.00;
		Double totalCobrado=0.00;
		while (count<valores.size()) {	
			long id = Long.parseLong(valores.get(count));
			Venta venta = ventaService.findOne(id);
			if(venta.getServicios()!=null) {
				Integer cont=0;
				
				while (cont<venta.getServicios().size()) {	
					total_pvp+=venta.getServicios().get(cont).getPvp();
					cont++;
				 }
			}
			DateFormat dfy = new SimpleDateFormat("yy");
			DateFormat dfm = new SimpleDateFormat("MM");
			Calendar cal = Calendar.getInstance();
			String idVentaSucursal = String.format("%03d", Integer.valueOf(idSucursal.intValue()));
			String idVentaAno = dfy.format(Calendar.getInstance().getTime());
			String idVentaMes = dfm.format(Calendar.getInstance().getTime());
			String idVentaDia = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
			String idFacturaNumero = String.format("%05d", venta.getNumero_venta());
			String idLargoTicket = 'F' + idVentaAno + idVentaMes + idVentaDia + idFacturaNumero;
			
			Factura factura= new Factura();
			factura.setNro_Factura(idLargoTicket);
			factura.setEmpleado(user);
			factura.setEmpresa(empresa);
			factura.setMarca(marca);
			factura.setSucursal(sucursal);
			factura.setTipo("");
			factura.setVenta(venta);
			factura.setFecha(date);
			factura.setFecha_creacion(date);
			factura.setHora(date);
			factura.setImporte(venta.getTotal());
			factura.setIva(venta.getTotal_iva());
			factura.setPVP(total_pvp);
			factura.setId_ticket(venta.getId_corto_ticket());
			factura.setPresentado(false);
			factura.setEstado("Facturado");
			factura.setConcepto("");
			facturaService.SaveFactura(factura);
			
			facturas.add(factura);
			venta.setFactura(factura);
			venta.setLote(lote);
			sumPagado+=venta.getPagado();
			ivas+=venta.getTotal_iva();
			totalCobrado+=venta.getTotal();
			ventaService.saveVenta(venta);
			 count++;
		 }
		lote.setTotalFactura(sumPagado);
		lote.setFactura(facturas);
		lote.setTotalGastos(totalCobrado);
		lote.setIvaDeducible(ivas);
		lote.setIvaDevegando(ivas);
		loteService.saveLotes(lote);
		if(result.hasErrors() || flagError==true) {
			
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
			modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.setViewName("redirect:/tpv/factura");
			return modelAndView;
		}
		modelAndView.setViewName("redirect:/tpv/factura");
		return modelAndView;
	}
	
	@GetMapping(value = "/tpv/factura/gastos/editar/{id}")
	@ResponseBody
	public List<String> editarGasto(HttpServletRequest request, @PathVariable(value = "id") Long id, RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		List<String> valores= new ArrayList<String>();
		request.getSession().setAttribute("idClientSel",id);
		Gastos gasto = gastosService.findOne(id);
		valores.add(gasto.nro_factura);
		valores.add(gasto.fecha_pago.toString());
		valores.add(gasto.cif);
		valores.add(gasto.proveedor);
		valores.add(gasto.tipo_gasto);
		valores.add(gasto.concepto);
		valores.add(gasto.base_imponible.toString());
		valores.add(gasto.iva.toString());
		valores.add(gasto.porc_deduccion.toString());
		valores.add(gasto.comentario);
		if(gasto.empleado!=null) {
			valores.add(gasto.empleado.getId().toString());
		}else {
			valores.add("0");
		}
		
		
		if (rol!=null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			
				modelAndView.addObject("user", user);
				modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		        modelAndView.addObject("marca", marcaService.findOne(idMarca));

				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		
				
			}
		return valores;
		
	}
	
	@RequestMapping(value = "/tpv/factura/gastos/anular/{id}",method=RequestMethod.POST)
	@ResponseBody
	public String anularGasto(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "id") Long id, RedirectAttributes flash,SessionStatus status) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);

		Gastos gasto = gastosService.findOne(id);
		gasto.setAnular(true);
		gastosService.saveGastos(gasto);
		String idGasto= id.toString();
		if (rol!=null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			
				modelAndView.addObject("user", user);

				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		        modelAndView.addObject("marca", marcaService.findOne(idMarca));
		        modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		
				
			}
		String mensajeFlash = "El  gasto fue creado con éxito";	
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		
		modelAndView.setViewName("redirect:/tpv/factura/gastos");
		return idGasto;
		
	}
	
	@RequestMapping(value = "/tpv/factura/anular/{lista}",method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView anularFactura(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "lista") List<String> lista, RedirectAttributes flash,SessionStatus status) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		
		Integer count=0;
		while (count<lista.size()) {	
			Venta venta = ventaService.findOne(Long.parseLong(lista.get(count)));
		
			venta.setAnular(true);
			ventaService.saveVenta(venta);
			count++;
		}
		

		if (rol!=null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			
				modelAndView.addObject("user", user);

				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		        modelAndView.addObject("marca", marcaService.findOne(idMarca));
		        modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				List<Pago> pago = new ArrayList <Pago>();
				List<String> nro_factura = new ArrayList <String>();
				String query= (String)request.getSession().getAttribute("ListConsultados");	
				if(query!=null) {
				try {
					Connection conexion;
					Statement consulta;            
					ResultSet data;

					request.getSession().setAttribute("ListConsultados",query);
					// Conectamos con la base de datos			
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
					    
					consulta = conexion.createStatement();
					
						
					
					data = consulta.executeQuery(query);
					
					while (data.next()) {
						Pago pagos = new Pago();
						Long Venta;
						Long FormaPago;
						pagos.setId(data.getLong("id"));
						pagos.setImporte( data.getDouble("importe") );
						pagos.setFecha(data.getTimestamp("fecha"));
						pagos.setHora(data.getTimestamp("hora"));
						FormaPago =data.getLong("forma_pago_id");
					
						
						if( !data.wasNull() ) {
							pagos.setForma_pago(formapagoService.findOne(FormaPago));
						}
						Venta = data.getLong("venta_id");
						
						if( !data.wasNull() ) {
							pagos.setVenta( ventaService.findOne(Venta) );
						}
						nro_factura.add(pagos.getVenta().getFactura().getNro_Factura());
						pago.add(pagos);
				        
				    }
					
					data.close();
					
					
				}catch(Exception e) {
					
					System.out.println(e);
					
				}
				modelAndView.addObject("ventas",pago);
				modelAndView.addObject("nro_factura",nro_factura);
				}else {
					modelAndView.addObject("ventas",null);
				}
				
			}
		String mensajeFlash = "El  gasto fue anulado con éxito";	
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		
		modelAndView.setViewName("redirect:/tpv/factura");
		return modelAndView;
		
	}

	
	@RequestMapping(value = "/tpv/factura/habilitar/ajax/{valor}",method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView habilitarFactura(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "valor") Long valor, RedirectAttributes flash,SessionStatus status) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);

			Venta venta = ventaService.findOne(valor);
			if(venta.getAnular()==true) {
				venta.setAnular(false);
				ventaService.saveVenta(venta);
			}
			
		if (rol!=null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			
				modelAndView.addObject("user", user);

				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		        modelAndView.addObject("marca", marcaService.findOne(idMarca));
		        modelAndView.addObject("gastoslist", gastosService.GastosBySucursal(idSucursal,idEmpresa,idMarca) );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				@SuppressWarnings("unchecked")
				List<Pago> pagos= (List<Pago>)request.getSession().getAttribute("ListConsultados");	
				if(pagos!=null) {
					modelAndView.addObject("ventas",pagos);
				}
				
			}
		String mensajeFlash = "El  gasto fue anulado con éxito";	
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		
		modelAndView.setViewName("redirect:/tpv/factura");
		return modelAndView;
		
	}
	@RequestMapping(value = "/tpv/factura/lotes", method=RequestMethod.GET )
	public ModelAndView lotes(RedirectAttributes flash, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		boolean nuevoUsuario = marEmpSucUsuRolService.verifyIfIsNewUsuario(user.getId());
		
		if (rolAdmin != null && nuevoUsuario == false ) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());

			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			List<Lotes> lote = new ArrayList <Lotes>();
			
			try {
				Connection conexion;
				Statement consulta;            
				ResultSet data;
				String query=(String)request.getSession().getAttribute("ListConsultadosLotes");	
				
				// Conectamos con la base de datos			
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				System.out.print(query);
				conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
				    
				consulta = conexion.createStatement();
				
					
				
				data = consulta.executeQuery(query);
				
				while (data.next()) {
					Long marc_id;
					Long emp_id;
					Long suc_id;
					Long user_id;
					Lotes lotes = new Lotes();
					
					marc_id = data.getLong("marca_id");
					emp_id=data.getLong("empresa_id");
					suc_id=data.getLong("sucursal_id");
					user_id=data.getLong("usuario_id");
					lotes.setId(data.getLong("Id"));
					
					lotes.setFecha_creacion(data.getDate("fecha_creacion"));
					
					Set <Factura> facturas =loteService.findOne(data.getLong("Id")).getFactura();
				
					lotes.setFactura(facturas);
					lotes.setIvaDeducible(data.getDouble("iva_deducible"));
					lotes.setIvaDevegando(data.getDouble("iva_devegando"));
					lotes.setTotalGastos(data.getDouble("total_gastos"));
					lotes.setTotalFactura(data.getDouble("total_facturado"));
					lotes.setFecha_envio(data.getDate("fecha_envio"));
					lotes.setEmail_Receptor(data.getString("email_receptor"));
					lotes.setFecha_presentado(data.getDate("fecha_presentado"));
					lotes.setPresentado(data.getBoolean("presentado"));
					lotes.setAnular(data.getBoolean("anular"));
					Marca marca_gasto= marcaService.findOne(marc_id);
					if( !data.wasNull() ) {
						
						lotes.setMarca(marca_gasto);
					}
					
					Empresa empre_gasto= empresaService.findOne(emp_id);
					if( !data.wasNull() ) {
						lotes.setEmpresa(empre_gasto);
					}
					
					Sucursal sucur_gasto= sucursalService.findOne(suc_id);
					if( !data.wasNull() ) {
						lotes.setSucursal(sucur_gasto);
						
					}
				
					Usuario us= userService.findOne(user_id);
					if( !data.wasNull() ) {
						lotes.setEmpleado(us);
					}
					
					lote.add(lotes);
			        
			    }
				
				data.close();
			
				
			}catch(Exception e) {
				
				System.out.println(e);
				
			}
			if(lote.size()==0) {
				modelAndView.addObject("ventas", loteService.LotesBySucursal(idSucursal));
			}else {
				modelAndView.addObject("ventas",lote);
			}
			
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.setViewName("tpv/factura/lotes");
			return modelAndView;
			
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/factura/resumen", method=RequestMethod.GET )
	public ModelAndView resumen(RedirectAttributes flash, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		boolean nuevoUsuario = marEmpSucUsuRolService.verifyIfIsNewUsuario(user.getId());
		
		if (rolAdmin != null && nuevoUsuario == false ) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());

			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
			modelAndView.addObject("ventas", ventaService.findByIdSucursaltoListTipo(idSucursal));
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.setViewName("tpv/factura/resumen");
			return modelAndView;
			
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/tpv/factura/buscar/{criterio}/ajax")
	@ResponseBody
	public ModelAndView listarFacturasSegunBusqueda(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "criterio") List<String> criterio)  {
	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		//List<Cliente> cliente = clienteService.;
			
		
		
		List<Pago> pago = new ArrayList <Pago>();
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		List<String> nro_factura = new ArrayList <String>();
		SimpleDateFormat formatoFechaHora = new SimpleDateFormat("yyyy-MM-dd");
		List<String> codeResponde = new ArrayList<String>();
		try {
			Connection conexion;
			Statement consulta;            
			ResultSet data;
			String query="";
			 query = "Select v.saldo,p.venta_id, v.id,p.fecha,p.hora,p.id,(select Distinct sum(p.importe) from pago p where p.venta_id=v.id) as importe,p.forma_pago_id from Pago p,Venta v,forma_pago f,Factura fac, Lotes l where  p.sucursal_id="+idSucursal+" and p.empresa_id="+idEmpresa+" and p.marca_id="+idMarca+"and p.venta_id=v.id ";
			if(criterio.get(6).equals("")==false) {
				if(criterio.get(6).equals("1")) {
					query=query+" and v.saldo>=0.00";
				}
				if(criterio.get(6).equals("2")) {
					 query = "select v.id_corto_ticket,v.id,v.saldo, from venta ve where  ve.saldo<0 and v.sucursal_id="+idSucursal+" and v.empresa_id="+idEmpresa+" and v.marca_id="+idMarca;
				}
			}
			if(criterio.get(0).equals("")==false) {
				query=query+" and v.fecha>='"+criterio.get(0)+"'";
			}
			if(criterio.get(1).equals("")==false) {
				query=query+" and v.fecha<='"+criterio.get(1)+"'";
			}
			if(criterio.get(2).equals("")==false) {
				
				query=query+" and p.forma_pago_id=f.id and f.nombre LIKE '"+criterio.get(2)+"%'";
			}
			if(criterio.get(3).equals("")==false) {
				
				query=query+" and v.factura_id="+criterio.get(3);
			}
			if(criterio.get(4).equals("")==false && criterio.get(3).equals("")==false) {
				
				query=query+" and fac.estado='"+criterio.get(4)+"'";
			}

			if(criterio.get(4).equals("")==false && criterio.get(3).equals("")==true) {
				
				query=query+" and v.factura_id=fac.id and fac.estado='"+criterio.get(4)+"'";
			}
				
			
			if(criterio.get(5).equals("")==false) {
				
				query=query+" and v.lote_id="+criterio.get(5);
			}
		
			
			query = query + "  group by  v.saldo,p.venta_id,v.id,p.fecha,p.hora,p.id,p.forma_pago_id,v.fecha order by v.fecha asc";
			System.out.println(query);
			
			request.getSession().setAttribute("ListConsultados",query);
			// Conectamos con la base de datos			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
			    
			consulta = conexion.createStatement();
			data = consulta.executeQuery(query);
			while (data.next()) {
				Pago pagos = new Pago();
				Long Venta;
				Long FormaPago;
				pagos.setId(data.getLong("id"));
				pagos.setImporte( data.getDouble("importe") );
				pagos.setFecha(data.getTimestamp("fecha"));
				pagos.setHora(data.getTimestamp("hora"));
				FormaPago =data.getLong("forma_pago_id");
			
				
				if( !data.wasNull() ) {
					pagos.setForma_pago(formapagoService.findOne(FormaPago));
				}
				Venta = data.getLong("venta_id");
				
				if( !data.wasNull() ) {
					if(ventaService.findOne(Venta).getId_corto_relacion()!=null) {
						
						Venta ventemp=new Venta();
						List<Venta> vent=ventaService.findDevolucionByIdCortoTicketAndSucursalVenta(ventaService.findOne(Venta).getId_corto_ticket(),idSucursal);
						
						ventemp.setId(vent.get(1).getId());
						ventemp.setId_largo_ticket('D'+vent.get(1).getId_corto_ticket());
						ventemp.setFecha(vent.get(1).getFecha());
						ventemp.setHora(vent.get(1).getHora());
						ventemp.setPagado(vent.get(1).getPagado());
						ventemp.setSaldo(vent.get(1).getSaldo());
						
						
						Double cero=0.0;
						int retval = Double.compare(vent.get(1).getSaldo(), cero);
						if(criterio.get(6).equals("2")==true) {
							System.out.println("menor");
						}
						if(criterio.get(6).equals("1")==true && retval>=0) {
						
							pagos.setVenta(ventemp);
							//System.out.println(ventemp.getId_largo_ticket());
						}
						if(criterio.get(6).equals("2")==true && retval<0) {
							
							pagos.setVenta(ventemp);
						}
						if(retval<0) {
							pagos.setVenta(ventaService.findOne(Venta) );
						}
						if(criterio.get(6).equals("1")==false && criterio.get(6).equals("2")==false) {
							pagos.setVenta(ventemp);
						}
						
					}else {
						pagos.setVenta( ventaService.findOne(Venta) );
					}
					
				}
				if(pagos.getVenta().getFactura()!=null) {
					nro_factura.add(pagos.getVenta().getFactura().getNro_Factura());
				}else {
					nro_factura.add("");
				}
				pago.add(pagos);
		        
		    }      
			
			data.close();
			
		}catch(Exception e) {
			
			System.out.println(e);
			
		}
		
		
		modelAndView.addObject("nro_factura",nro_factura);
		if(pago.size()==0) {
			request.getSession().setAttribute("ListConsultados",null);
		}else {
		modelAndView.addObject("ventas",pago);
		}
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.setViewName("tpv/factura/listaVentas");
		return modelAndView;
	}
	

	@RequestMapping(value = "/tpv/factura/gastos/buscar/{criterio}/ajax")
	@ResponseBody
	public ModelAndView listarGastosSegunBusqueda(HttpServletRequest request,  @PathVariable(value = "criterio") List<String> criterio) throws ParseException {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		if (idClientSel!=null ) {
			modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
		}else {
			modelAndView.addObject("clientesel",true);
		}
		
		List<Gastos> gasto = new ArrayList <Gastos>();
	
		Double sumBase=0.00;
		Double sumIva=0.00;
		try {
			Connection conexion;
			Statement consulta;            
			ResultSet data;
			String query="";
			query = "Select * from gastos where empresa_id="+idEmpresa+" and marca_id="+idMarca+"and sucursal_id="+idSucursal;
			
			
			if(criterio.get(0).equals("")==false) {
				query=query+" and fecha>='"+criterio.get(0)+"'";
			}
			if(criterio.get(1).equals("")==false) {
				query=query+" and fecha<='"+criterio.get(1)+"'";
			}
			if(criterio.get(2).equals("")==false) {
				
				query=query+" and proveedor='"+criterio.get(2)+"'";
			}
			
			if(criterio.get(3).equals("")==false) {
				
				query=query+" and tipo_gasto='"+criterio.get(3)+"'";
			}
			query=query+" order by fecha asc";
			request.getSession().setAttribute("ListConsultadosGastos",query);
			// Conectamos con la base de datos			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.print(query);
			conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
			    
			consulta = conexion.createStatement();
			
				
			
			data = consulta.executeQuery(query);
			
			while (data.next()) {
				Long marc_id;
				Long emp_id;
				Long suc_id;
				Long user_id;
				Gastos gastos = new Gastos();
				gastos.setId(data.getLong("id"));
				gastos.setNro_Factura(data.getString("nro_factura"));
				gastos.setFecha(data.getTimestamp("fecha"));
				gastos.setCIF(data.getString("cif"));
				gastos.setComentario(data.getString("comentario"));
				gastos.setConcepto(data.getString("concepto"));
				gastos.setFecha_pago(data.getTimestamp("fecha_pago"));
				gastos.setBase_imponible(data.getDouble("base_imponible"));
				gastos.setAnular(data.getBoolean("anular"));
				gastos.setPorc_deduccion(data.getDouble("porc_deduccion"));
				gastos.setIva(data.getDouble("iva"));
				gastos.setTipo_gasto(data.getString("tipo_gasto"));
				marc_id = data.getLong("marca_id");
				emp_id=data.getLong("empresa_id");
				suc_id=data.getLong("sucursal_id");
				user_id=data.getLong("usuario_id");
				Double base=data.getDouble("base_imponible");
				if(base!=null) {
					sumBase=sumBase+base;
				}
				Double iva=data.getDouble("iva");
				if(iva!=null) {
					sumIva=sumIva+iva;
				}
				Marca marca_gasto= marcaService.findOne(marc_id);
				if( !data.wasNull() ) {
					gastos.setMarca(marca_gasto);
				}
				
				Empresa empre_gasto= empresaService.findOne(emp_id);
				if( !data.wasNull() ) {
					gastos.setEmpresa(empre_gasto);
				}
				
				Sucursal sucur_gasto= sucursalService.findOne(suc_id);
				if( !data.wasNull() ) {
					gastos.setSucursal(sucur_gasto);
					
				}
			
				Usuario us= userService.findOne(user_id);
				if( !data.wasNull() ) {
					gastos.setEmpleado(us);
					
				}
				
				gasto.add(gastos);
		        
		    }
			
			data.close();
			
			
		}catch(Exception e) {
			
			System.out.println(e);
			
		}
		modelAndView.addObject("gastoslist",gasto);
		modelAndView.addObject("base",sumBase);
		modelAndView.addObject("iva",sumIva);
		
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.setViewName("tpv/factura/listaGastos");
		return modelAndView;
	}
	
	@GetMapping(value = "/tpv/factura/gastos/buscar/{criterio}/ajax")
	@ResponseBody
	public String listarEnGastosSegunBusqueda(HttpServletRequest request, @PathVariable(value = "criterio") String criterio, @RequestParam String busqueda)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		String jsons ="";
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		JSONObject objson = new JSONObject();
		List<Object[]> listadosjson = new ArrayList<Object[]>();
		if(criterio.equals("proveedor")) {
			
			List<Gastos> listados = gastosService.findByProveedorAndSucursal(busqueda, idSucursal, idEmpresa, idMarca); 
		
			for (int i=0; i< listados.size(); i++) {
				
				Object[] temp= new Object[1];
				temp[0]=listados.get(i).getProveedor();
				listadosjson.add(temp);
			}
			 jsons = objGson.toJson(listadosjson);
		}

		if(criterio.equals("tipogasto")) {
			List<Gastos> listados = gastosService.findByTipoGastoAndSucursal(busqueda, idSucursal, idEmpresa, idMarca); 
				
			
				for (int i=0; i< listados.size(); i++) {
					
					Object[] temp= new Object[1];
					temp[0]= listados.get(i).getTipo_gasto();
					listadosjson.add(temp);
				}
				 jsons = objGson.toJson(listadosjson);
			}
		return jsons;
	}
	
	@RequestMapping(value = "/tpv/factura/lotes/presentar/{valor}/{busqueda}",method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView presentarLote(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "valor") Long valor, @PathVariable(value = "busqueda") Date busqueda, RedirectAttributes flash,SessionStatus status) {
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		System.out.println("presentado "+busqueda);
			Lotes lote = loteService.findOne(valor);
			if(lote!=null) {
				lote.setFecha_presentado(busqueda);
				lote.setPresentado(true);
				loteService.saveLotes(lote);
			}
			
		if (rol!=null) {
			
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			
				modelAndView.addObject("user", user);

				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		        modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("ventas", loteService.LotesBySucursal(idSucursal));
				
			}
	
		
		modelAndView.setViewName("redirect:/tpv/factura");
		return modelAndView;
		
	}
	
	
	@RequestMapping(value = "/tpv/factura/lotes/buscar/{criterio}/ajax")
	@ResponseBody
	public ModelAndView FiltrarLotes(HttpServletRequest request,  @PathVariable(value = "criterio") List<String> criterio) throws ParseException {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());

		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		if (idClientSel!=null ) {
			modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
		}else {
			modelAndView.addObject("clientesel",true);
		}
		
		List<Lotes> lote = new ArrayList <Lotes>();
		List <String> agentes = new ArrayList <String>();
		try {
			Connection conexion;
			Statement consulta;            
			ResultSet data;
			
			String query="";
			query = "Select * from lotes where empresa_id="+idEmpresa+" and marca_id="+idMarca+"and sucursal_id="+idSucursal;
			
			
			if(criterio.get(0).equals("")==false) {
				query=query+" and fecha_creacion>='"+criterio.get(0)+" 00:00:00'";
			}
			if(criterio.get(1).equals("")==false) {
				query=query+" and fecha_creacion<='"+criterio.get(1)+" 12:59:59'";
			}
			
			if(criterio.get(2).equals("")==false) {
				query=query+" and fecha_envio='"+criterio.get(2)+"'";
			}
			if(criterio.get(3).equals("")==false) {
				
				query=query+" and fecha_envio<>''";
			}
			
			if(criterio.get(4).equals("")==false) {
				
				
				query=query+" and presentado=1";
			}
			query=query+" order by fecha_creacion asc";
			System.out.print(query);
			request.getSession().setAttribute("ListConsultadosLotes",query);
			// Conectamos con la base de datos			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		
			conexion=DriverManager.getConnection(urlBD, usuarioBD, passBD );
			    
			consulta = conexion.createStatement();
			
				
			
			data = consulta.executeQuery(query);
			
			while (data.next()) {
				Long marc_id;
				Long emp_id;
				Long suc_id;
				Long user_id;
				Lotes lotes = new Lotes();
				
				marc_id = data.getLong("marca_id");
				emp_id=data.getLong("empresa_id");
				suc_id=data.getLong("sucursal_id");
				user_id=data.getLong("usuario_id");
				lotes.setId(data.getLong("Id"));
				
				lotes.setFecha_creacion(data.getDate("fecha_creacion"));
				
				Set <Factura> facturas =loteService.findOne(data.getLong("Id")).getFactura();
			
				lotes.setFactura(facturas);
				lotes.setIvaDeducible(data.getDouble("iva_deducible"));
				lotes.setIvaDevegando(data.getDouble("iva_devegando"));
				lotes.setTotalGastos(data.getDouble("total_gastos"));
				lotes.setTotalFactura(data.getDouble("total_facturado"));
				lotes.setFecha_envio(data.getDate("fecha_envio"));
				lotes.setEmail_Receptor(data.getString("email_receptor"));
				lotes.setFecha_presentado(data.getDate("fecha_presentado"));
				lotes.setPresentado(data.getBoolean("presentado"));
				lotes.setAnular(data.getBoolean("anular"));
				Marca marca_gasto= marcaService.findOne(marc_id);
				if( !data.wasNull() ) {
					//Si el último valor leído no fue null, asigno el cliente
					lotes.setMarca(marca_gasto);
				}
				
				Empresa empre_gasto= empresaService.findOne(emp_id);
				if( !data.wasNull() ) {
					//Si el último valor leído no fue null, asigno el cliente
					lotes.setEmpresa(empre_gasto);
				}
				
				Sucursal sucur_gasto= sucursalService.findOne(suc_id);
				if( !data.wasNull() ) {
					lotes.setSucursal(sucur_gasto);
					
				}
			
				Usuario us= userService.findOne(user_id);
				if( !data.wasNull() ) {
					lotes.setEmpleado(us);
					agentes.add(us.getNombre()+" "+us.getApellido());
					System.out.println(agentes.get(0));
				}
				
				lote.add(lotes);
		        
		    }
			
			data.close();
		
			
		}catch(Exception e) {
			
			System.out.println(e);
			
		}
		modelAndView.addObject("ventas",lote);
		modelAndView.addObject("agentes",agentes);
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.setViewName("/tpv/factura/listaLotes");
		return modelAndView;
	}
	

}
