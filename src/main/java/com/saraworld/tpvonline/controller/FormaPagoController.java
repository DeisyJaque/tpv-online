package com.saraworld.tpvonline.controller;

import static java.lang.Math.toIntExact;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.FormaPago;
import com.saraworld.tpvonline.model.MarEmpSucUsuRol;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.model.Usuario;
import com.saraworld.tpvonline.service.MarEmpSucUsuRolService;
import com.saraworld.tpvonline.service.RolService;
import com.saraworld.tpvonline.service.FormaPagoService;
import com.saraworld.tpvonline.service.UsuarioService;
import com.saraworld.tpvonline.util.PageRender;

@Controller
public class FormaPagoController {
	@Autowired
	private UsuarioService userService;
	@Autowired
	private RolService rolService;
	@Autowired
	private FormaPagoService formaPagoService;
	@Autowired
	private MarEmpSucUsuRolService marEmpSucUsuRolService;
	
	public List<FormaPago> listarFormasPagoOfUser (Usuario user){
		
		List<MarEmpSucUsuRol> registros = marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
		List<FormaPago> formasPago = new ArrayList<FormaPago>();
		
		for (MarEmpSucUsuRol registro : registros) {
			formasPago.addAll(formaPagoService.findListFormasPagoByIdOfEmpresa(registro.getEmpresa().getId()));
		}
		
		//Para eliminar registros repetidos del listado de formas de pago
		Set<FormaPago> hashSetFormasPago = new HashSet<>();
		hashSetFormasPago.addAll(formasPago);
		formasPago.clear();
		formasPago.addAll(hashSetFormasPago);
		
		//Para ordenar los registros del listado de formas de pago
		formasPago.sort(Comparator.comparing(FormaPago::getNombre));
		
		return formasPago;
	}
	
	@RequestMapping(value = "/admin/empresas/FormasPago", method=RequestMethod.GET )
	public ModelAndView listar(@RequestParam(name = "page", defaultValue="0") int page) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		if (rolAdmin != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		
			if (modulosPermisos.contains("Admin - Formas de pago")) {
				
				List<FormaPago> listaFormasPagos = listarFormasPagoOfUser(user);
				Page<FormaPago> formasPago = convertListToPage(page, listaFormasPagos);
				PageRender<FormaPago> pageRender = new PageRender<>("/admin/empresas/FormasPago",formasPago);
				
				modelAndView.addObject("user", user );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("formasPago",formasPago);
				modelAndView.addObject("page", pageRender);
				modelAndView.addObject("empresa", marEmpSucUsuRolService.findListEmpresasByIdOfUsuario(user.getId()));
				modelAndView.setViewName("administracion/formasPago/listar");
				
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/empresas/formasPago/{id}", method=RequestMethod.GET)
	public ModelAndView ver(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		if (rolAdmin != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			
			//Solo permite consultar formas de pago a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("64")) {
				FormaPago formapago = formaPagoService.findOne(id);
				
				if (formapago == null) {
					flash.addFlashAttribute("error", "La Forma de Pago que desea ver no existe");
					modelAndView.setViewName("redirect:/empresas/FormasPago");
					return modelAndView;
				}

				//Datos del usuario logueado en el sistema
				modelAndView.addObject("user", user);
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				
				//Datos de la forma de pago consultada
				modelAndView.addObject("formapago", formapago);
				
				List<Empresa> empresas = formaPagoService.findListEmpresasByIdOfFormaPago(id);
				List<List<Sucursal>> listaSucursalesPorEmpresa = new ArrayList<List<Sucursal>> ();
				
				for(Empresa empresa : empresas ) {
					List<Sucursal> sucursalesDeEmpresa = marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(empresa.getId());
					List<Sucursal> sucursalesFormaPago = formaPagoService.findListSucursalesByIdOfFormaPago(id);
					List<Sucursal> sucursales = new ArrayList<Sucursal> ();
					
					for(Sucursal sucursal : sucursalesFormaPago ) {
						if( sucursalesDeEmpresa.contains(sucursal) ) {
							sucursales.add(sucursal);
						}
					}
					
					listaSucursalesPorEmpresa.add(sucursales);
				}
				
				modelAndView.addObject("empresas",empresas);
				modelAndView.addObject("listaSucursalesPorEmpresa", listaSucursalesPorEmpresa);

				modelAndView.setViewName("administracion/formasPago/ver");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/empresas/formasPago/crear", method=RequestMethod.GET )
	public ModelAndView formapagocrear() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		if (rolAdmin != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			
			//Solo permite crear tipos de IVA a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("65")) {
				modelAndView.addObject("user", user );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				
				modelAndView.addObject("formasPago", new FormaPago());
				modelAndView.addObject("marEmpSucUsuRol", marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId()));
				modelAndView.addObject("empresa", marEmpSucUsuRolService.findListEmpresasByIdOfUsuario(user.getId()));
				modelAndView.addObject("empre_sucur", marEmpSucUsuRolService.findSucursalAndEmpresaByIdOfUsuario(user.getId()));
				modelAndView.addObject("sucursales",marEmpSucUsuRolService.findListSucursalesByIdAndRolOfUsuario(user.getId(), rolAdmin.getRol().getId()));
				modelAndView.setViewName("administracion/formasPago/crear");
				
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;	
	}
	
	@RequestMapping(value="/admin/empresas/formasPago/crear", method = RequestMethod.POST)
	public ModelAndView crear(@ModelAttribute @Valid FormaPago formapago,  BindingResult result, @RequestParam Map<String, String> requestParams, ModelAndView modelAndView, RedirectAttributes flash ,SessionStatus status){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		modelAndView.addObject("user", user);
        modelAndView.addObject("modulosPermisos", modulosPermisos);
        modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
        Date date = new Date(Calendar.getInstance().getTime().getTime());
		if(result.hasErrors()) {
			modelAndView.setViewName("administracion/formasPago/crear");
	        modelAndView.addObject("formasPago", formapago);
	        return modelAndView;
		}
		
		String mensajeFlash = "Forma de Pago: '" + formapago.getNombre() + "' fue creado con éxito";
		
		formapago.setFecha_creacion(date);
		formaPagoService.saveFormaPago(formapago);
		
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		modelAndView.setViewName("redirect:/admin/empresas/FormasPago");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/empresas/formasPago/{id}/editar", method=RequestMethod.GET)
	public ModelAndView paginaEditar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		if (rolAdmin != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			
			//Solo permite editar empleados a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("66")) {
				
				FormaPago formapago = formaPagoService.findOne(id);
				
				if (formapago == null) {
					flash.addFlashAttribute("error", "La forma de pago que desea editar no existe");
					modelAndView.setViewName("redirect:/admin/empresas/FormasPago");
					return modelAndView;
				}
				
				//Datos que necesita el usuario que esta logueado en el sistema
				modelAndView.addObject("user", user );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				
				//Datos que se necesitan para el formulario de Editar FOrma de Pago
				modelAndView.addObject("formapago", formapago );
				modelAndView.addObject("empresa", marEmpSucUsuRolService.findListEmpresasByIdOfUsuario(user.getId()));
				modelAndView.addObject("marEmpSucUsuRol", marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId()));
				modelAndView.addObject("empre_sucur", marEmpSucUsuRolService.findSucursalAndEmpresaByIdOfUsuario(user.getId()));
				modelAndView.setViewName("administracion/formaspago/editar");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;	
	}
	
	@RequestMapping(value="/admin/empresas/formasPago/{id}/editar", method = RequestMethod.POST)
	public ModelAndView editar(@PathVariable(value = "id") Long id, @ModelAttribute @Valid FormaPago formapago,  BindingResult result, @RequestParam Map<String, String> requestParams, ModelAndView modelAndView, RedirectAttributes flash ,SessionStatus status){
		
		FormaPago formaPagoRegistrada = formaPagoService.findOne(id);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		modelAndView.addObject("user", user);
        modelAndView.addObject("modulosPermisos", modulosPermisos);
        modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
        Date date = new Date(Calendar.getInstance().getTime().getTime());
		if(result.hasErrors()) {
			modelAndView.setViewName("administracion/formasdepago/editar");
	        modelAndView.addObject("formapago", formapago);
			modelAndView.addObject("marEmpSucUsuRol", marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId()));
	        return modelAndView;
		}
		
		//Para que se mantengan al hacer la actualización los campos que no estan presentes en el formulario
		
		formapago.setBorrado(formaPagoRegistrada.getBorrado());
		formapago.setFecha_creacion(formaPagoRegistrada.getFecha_creacion());
		formapago.setFecha_modificacion(date);
		formapago.setFecha_espejo(formaPagoRegistrada.getFecha_espejo());
		formapago.setRowguid(formaPagoRegistrada.getRowguid());
		
		String mensajeFlash = "Forma de Pago: '" + formapago.getNombre() + "' fue editado con éxito";
		formaPagoService.saveFormaPago(formapago);
		
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		modelAndView.setViewName("redirect:/admin/empresas/FormasPago");
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/empresas/formasPago/{id}/eliminar")
	public ModelAndView eliminar(@PathVariable(value="id") Long id , RedirectAttributes flash) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		Calendar calendar = Calendar.getInstance();
		
		if (rolAdmin != null) {
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			
			//Solo permite eliminar formas de pago a los usuarios que tengan esa funcionalidad asignada a su rol
			if ( true == true ) {
				FormaPago formaPago = formaPagoService.findOne(id);
				
				if (formaPago == null) {
					flash.addFlashAttribute("error", "La Forma de Pago que desea eliminar no existe");
					modelAndView.setViewName("redirect:/admin/empresas/FormasPago");
					return modelAndView;
				}
				
				flash.addFlashAttribute("success", "La Forma de Pago '" + formaPago.getNombre() + "' fue eliminada con éxito");
				//formaPagoService.delete(id);
				formaPago.setBorrado(calendar.getTime());
				formaPagoService.saveFormaPago(formaPago);
				
				modelAndView.setViewName("redirect:/admin/empresas/FormasPago");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	//Método para verificar la forma de pago que se quiere eliminar tiene pagos asociados
	@RequestMapping(value="/admin/empresas/formasPago/{id}/eliminar/verificar")
	@ResponseBody
	public String verificarFormaPagoEliminadoAjax(@PathVariable(value="id") Long id , RedirectAttributes flash) {
		
		FormaPago formaPago = formaPagoService.findOne(id);
		
		if( formaPago.getPagos().size() > 0 ) {
			return "Si tiene pagos";
		}else {
			return "";
		}
	}
	
	private Page<FormaPago> convertListToPage(int page, List<FormaPago> listaFormasPago) {
		Pageable pageable = new PageRequest(page, 10);
		
		//Porción de código que permite convertir un objeto List a un objeto Page
		int inicio = pageable.getOffset();
		int fin = (inicio + pageable.getPageSize()) > listaFormasPago.size() ? listaFormasPago.size() : (inicio + pageable.getPageSize());
		Page<FormaPago> formaspago = new PageImpl<FormaPago>(listaFormasPago.subList(inicio, fin), pageable, listaFormasPago.size());
		
		return formaspago;
	}
	
	public boolean verificarSiExiste(Object[] formapago, List<Object[]> formaspago) {
		for(int i=0; i<formaspago.size(); i++) {
			if(formapago[0] == formaspago.get(i)[0]) {
				return true;
			}
		}
		return false;
	}
	
	@GetMapping(value = "/admin/empresas/formasPago/buscar/{nombre}")
	public @ResponseBody String buscarFormasPago(@PathVariable(value = "nombre") String nombre) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdAndRolOfUsuario(user.getId(), rolAdmin.getRol().getId());
		List<Object[]> formaspago = new ArrayList<Object[]>();
		
		for (Sucursal sucursal : sucursales) {
			List<Object[]> formaspagoTemp = formaPagoService.findListFormasDePagoByIdOfSucursalAndIdOrNombreOfForma(sucursal.getId(), nombre);
			
			for (int i=0; i< formaspagoTemp.size(); i++) {
				if(verificarSiExiste(formaspagoTemp.get(i),formaspago)==false) {
					formaspago.add(formaspagoTemp.get(i));
				}
			}
		}
		
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		String json = objGson.toJson(formaspago);
		return json;

	}
	
	@GetMapping(value = "/admin/empresas/formasdepago/ajax")
	@ResponseBody
	public ModelAndView listarFormasPago() {
		ModelAndView modelAndView = new ModelAndView();
		List<FormaPago> formapago = new ArrayList<FormaPago> ();
		
		
		//Para ordenar los registros del listado 
		Collections.sort(formapago, (t1, t2) -> toIntExact(t1.getId()) - toIntExact(t2.getId()));

		modelAndView.addObject("formasPago",formapago);
		modelAndView.setViewName("administracion/empresas/FormasPago");
		
		return modelAndView;
	}
}
