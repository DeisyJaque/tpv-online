package com.saraworld.tpvonline.controller;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saraworld.tpvonline.model.Ayuda;
import com.saraworld.tpvonline.model.Gastos;
import com.saraworld.tpvonline.model.GrupoAyuda;
import com.saraworld.tpvonline.model.Informes;
import com.saraworld.tpvonline.model.MarEmpSucUsuRol;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.model.Usuario;
import com.saraworld.tpvonline.service.EmpresaService;
import com.saraworld.tpvonline.service.MarEmpSucUsuRolService;
import com.saraworld.tpvonline.service.MarcaService;
import com.saraworld.tpvonline.service.RolService;
import com.saraworld.tpvonline.service.SucursalService;
import com.saraworld.tpvonline.service.UsuarioService;
import com.saraworld.tpvonline.service.AyudaService;
import com.saraworld.tpvonline.service.ClienteService;
import com.saraworld.tpvonline.service.GrupoAyudaService;
import com.saraworld.tpvonline.service.VentaService;

@Controller
public class AyudaController {
	@Autowired
	private UsuarioService userService;
	@Autowired
	private RolService rolService;
	@Autowired
	private MarEmpSucUsuRolService marEmpSucUsuRolService;
	@Autowired
	private SucursalService sucursalService;
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private AyudaService ayudaService;
	@Autowired
	private GrupoAyudaService grupoayudaService;
	@Autowired
	private VentaService ventaService;
	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value = "/tpv/ayuda", method=RequestMethod.GET )
	public ModelAndView listar(RedirectAttributes flash, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		boolean nuevoUsuario = marEmpSucUsuRolService.verifyIfIsNewUsuario(user.getId());
		
		if (rolAdmin != null && nuevoUsuario == false ) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			
			
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
			modelAndView.addObject("grupoayuda", grupoayudaService.ListGrupoByOrder());
			modelAndView.addObject("ayuda", ayudaService.findListAyudaByOrden());
			modelAndView.setViewName("tpv/ayuda/listar");
			return modelAndView;
			
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}


	@GetMapping(value="/tpv/listarayuda/ajax/{id}")
	@ResponseBody
	public  List<String> ListarFiltrosporGrupo(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "id") Long id, BindingResult result,  RedirectAttributes flash ,SessionStatus status) {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		List<String> informes= new ArrayList<>();
		 request.getSession().setAttribute("queryaux",null);
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.addObject("user", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("grupoayuda", grupoayudaService.ListGrupoByOrder());
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("agentes", marEmpSucUsuRolService.findListUsuariosByIdSucursalTPV(idSucursal));
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
		
		List<Ayuda> informessu=ayudaService.findListAyudaByIdOfGrupo(id);
		 for (int i = 0; i < informessu.size(); i++) { 
			 	informes.add(informessu.get(i).getId().toString());
			 	informes.add(informessu.get(i).getNombre());
		 }
		
		return informes;
	}
	
	@RequestMapping(value="/tpv/traerurl/ajax/{id}")
	@ResponseBody
	public ModelAndView ListarCheck(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "id") Long id,BindingResult result,  RedirectAttributes flash ,SessionStatus status) {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", empresaService.findOne(idEmpresa) );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfMarca(idMarca));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		
		modelAndView.addObject("ayudaSel", ayudaService.findOne(id).getURL());


		System.out.println("Controlador " + ayudaService.findOne(id).getURL());
		status.setComplete();
		
		modelAndView.setViewName("tpv/ayuda/cuadroUrl");
	
		
		return modelAndView;
	}
	
	
	@GetMapping(value = "/tpv/ayuda/buscar/{criterio}/ajax/{busqueda}")
	@ResponseBody
	public ModelAndView listarGastosSegunBusqueda(HttpServletRequest request, @PathVariable(value = "criterio") String criterio,  @PathVariable(value = "busqueda") String busqueda) throws ParseException {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		if (idClientSel!=null ) {
			modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
		}else {
			modelAndView.addObject("clientesel",true);
		}
		
		List<GrupoAyuda> ayuda = new ArrayList <GrupoAyuda>();
		
		if(busqueda.equals("todos-ayuda")) {
			ayuda  = grupoayudaService.ListGrupoByOrder();
			modelAndView.addObject("grupoayuda",ayuda);
			modelAndView.addObject("ayuda", ayudaService.findListAyudaByOrden());
		}else {
			
			if(criterio.equals("nombre")) {
			
					ayuda  = grupoayudaService.ListGrupoByNombre(busqueda);
					
					if(ayuda.size()==0) {
						List<GrupoAyuda> grupayudas = new ArrayList <GrupoAyuda>();
						List <Ayuda> ayudas = ayudaService.findListAyudaByOrdenPorNombre(busqueda);
						System.out.println(ayudas.size());
						for (int i=0; i< ayudas.size(); i++) {
							GrupoAyuda temp=grupoayudaService.findOne(ayudas.get(i).getGrupoAyuda().getId());
							grupayudas.add(temp);
						}
						Set<GrupoAyuda> hs = new HashSet<>();
						hs.addAll(grupayudas);
						grupayudas.clear();
						grupayudas.addAll(hs);
						Collections.sort(grupayudas, (x, y) -> x.getNombre().compareToIgnoreCase(y.getNombre()));
						modelAndView.addObject("grupoayuda", grupayudas);
						modelAndView.addObject("ayuda", ayudas);
					}else {
						modelAndView.addObject("grupoayuda",ayuda);
						modelAndView.addObject("ayuda", ayudaService.findListAyudaByOrden());
					}
			
			}
			
			
		}
		
	
		
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", user );
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		//modelAndView.addObject("ventas", ventaService.findByIdSucursaltoListTipo(idSucursal));
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.setViewName("tpv/ayuda/listaAyuda");
		return modelAndView;
	}
	
}
