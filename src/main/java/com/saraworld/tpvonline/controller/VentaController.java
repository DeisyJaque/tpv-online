package com.saraworld.tpvonline.controller;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.PrinterName;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.saraworld.tpvonline.model.Catalogo;
import com.saraworld.tpvonline.model.Cliente;
import com.saraworld.tpvonline.model.Coleccion;
import com.saraworld.tpvonline.model.Venta;
import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.MarEmpSucUsuRol;
import com.saraworld.tpvonline.model.Marca;
import com.saraworld.tpvonline.model.Pago;
import com.saraworld.tpvonline.model.Servicio;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.model.TipoIva;
import com.saraworld.tpvonline.model.Usuario;
import com.saraworld.tpvonline.model.Vale;
import com.saraworld.tpvonline.service.ClienteService;
import com.saraworld.tpvonline.service.ColeccionService;
import com.saraworld.tpvonline.service.ImpresoraService;
import com.saraworld.tpvonline.service.ArticuloService;
import com.saraworld.tpvonline.service.CatalogoService;
import com.saraworld.tpvonline.service.EmpresaService;
import com.saraworld.tpvonline.service.FamiliaService;
import com.saraworld.tpvonline.service.FormaPagoService;
import com.saraworld.tpvonline.service.MarEmpSucUsuRolService;
import com.saraworld.tpvonline.service.MarcaService;
import com.saraworld.tpvonline.service.PagoService;
import com.saraworld.tpvonline.service.RolService;
import com.saraworld.tpvonline.service.ServicioService;
import com.saraworld.tpvonline.service.SucursalService;
import com.saraworld.tpvonline.service.TipoDocumentoService;
import com.saraworld.tpvonline.service.TipoIvaService;
import com.saraworld.tpvonline.service.TipoNacionalidadService;
import com.saraworld.tpvonline.service.TipoSexoService;
import com.saraworld.tpvonline.service.UsuarioService;
import com.saraworld.tpvonline.service.PaisService;
import com.saraworld.tpvonline.service.VentaService;

import com.saraworld.tpvonline.service.PoblacionService;
import com.saraworld.tpvonline.service.ProvinciaService;
import com.saraworld.tpvonline.service.ValeService;

@Controller
public class VentaController {
	
	@Autowired
	private UsuarioService userService;
	@Autowired
	private RolService rolService;
	@Autowired
	private TipoSexoService tipoSexoService;
	@Autowired
	private TipoNacionalidadService tipoNacionalidadService;
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	@Autowired
	private MarEmpSucUsuRolService marEmpSucUsuRolService;
	@Autowired
	private SucursalService sucursalService;
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private PaisService paisService;
	@Autowired
	private ProvinciaService provinciaService;
	@Autowired
	private PoblacionService poblacionService;
	@Autowired
	private VentaService ventaService;
	@Autowired
	private CatalogoService catalogoService;
	@Autowired
	private ColeccionService coleccionService;
	@Autowired
	private FamiliaService familiaService;
	@Autowired
	private ArticuloService articuloService;
	@Autowired
	private TipoIvaService tipoIvaService;
	@Autowired
	private ServicioService servicioService;
	@Autowired
	private FormaPagoService formaPagoService;
	@Autowired
	private PagoService pagoService;
	@Autowired
	private ValeService valeService;
	@Autowired
	private JavaMailSender mailSender;
	
	private final static String ROOT_PATH_IMAGES_CLIENTES = "C:\\\\TPV_Online_Images\\\\clientes";
	private final static String ROOT_PATH_IMAGES_MARCAS = "C:\\\\TPV_Online_Images\\\\marcas";
	private final static String ROOT_PATH_PDF_FACTURAS = "C:\\\\TPV_Online_PDF\\\\";
	private static final String String = null;
	
	
	public double round( double cifra ) {
		return Math.round(cifra * 100.0) / 100.0;
	}
	
	public double calcularMontoIva(double precio, double porcentajeIva) {
		
		double monto_iva = ( (precio/(1 + (porcentajeIva / 100) ))*(porcentajeIva / 100) ) ;
		
		return round( monto_iva );
	}
	
	public double calcularMontoBase(double precio, double porcentajeIva) {
		
		double monto_base = (precio/(1 + (porcentajeIva / 100) ));
		
		return round( monto_base );
	}
	
	public double calcularMontoSegunPorcentaje(double precio, double porcentaje, int unidades) {
		
		double monto =  ( ( precio * unidades ) * porcentaje ) /100;
		
		return round( monto);
	}
	
	public double calcularTotalNetoDeVenta(Venta venta) {
		
		//Venta venta = ventaService.findOne(idVenta);
		List<Servicio> servicios = venta.getServicios();
		double totalNeto = 0.0;
		
		for(Servicio servicio : servicios ) {
			if( !servicio.getTipo_transaccion().equals("Anulacion") && servicio.getBorrado() == null) {
				totalNeto = totalNeto + servicio.getTotal_neto(); 
			}
		}
		
		return round(totalNeto);
	}
	
	public double calcularTotalIvaDeVenta(Venta venta) {
		
		//Venta venta = ventaService.findOne(idVenta);
		List<Servicio> servicios = venta.getServicios();
		double totalIva = 0.0;
		
		for(Servicio servicio : servicios ) {
			if( !servicio.getTipo_transaccion().equals("Anulacion") && servicio.getBorrado() == null) {
				totalIva = totalIva + servicio.getTotal_iva(); 
			}
		}
		
		return round(totalIva);
	}
	
	public double calcularTotalDescuentoDeVenta(Venta venta) {
		
		//Venta venta = ventaService.findOne(idVenta);
		List<Servicio> servicios = venta.getServicios();
		double totalDescuento = 0.0;
		
		for(Servicio servicio : servicios ) {
			
			if( !servicio.getTipo_transaccion().equals("Anulacion") && servicio.getBorrado() == null) {
				
				Double descuento_monto = 0.0;
				Double descuento_porcentaje = 0.0;
				
				if(servicio.getDescuento_monto() != null && servicio.getDescuento_monto() != 0.0 ) {
					descuento_monto = servicio.getDescuento_monto();
				}
				if(servicio.getDescuento_porcentaje() != null && servicio.getDescuento_porcentaje() != 0.0) {
					descuento_porcentaje = calcularMontoSegunPorcentaje(servicio.getPvp(), servicio.getDescuento_porcentaje() , 1);
					
				}
				
				totalDescuento = totalDescuento + descuento_monto + descuento_porcentaje;
			}
		}
		
		return round(totalDescuento);
	}
	
	public double calcularTotalDeVenta(Venta venta) {
		
		//Venta venta = ventaService.findOne(idVenta);
		double total = venta.getTotal_neto() + venta.getTotal_iva() ;
		
		return total;
	}
	
	public double calcularSaldoDeVenta(Venta venta) {
		
		//Venta venta = ventaService.findOne(idVenta);
		double totalSaldo = venta.getTotal() - venta.getPagado();
		
		return totalSaldo;
	}
	
	public  String  calcularTotalesDeLaVenta(Venta venta) {
		
		//Venta venta = ventaService.findOne(idVenta);
		
		venta.setTotal_neto( calcularTotalNetoDeVenta(venta) );
		venta.setTotal_iva( calcularTotalIvaDeVenta(venta) );
		venta.setTotal_descuento( calcularTotalDescuentoDeVenta(venta) );
		venta.setTotal( calcularTotalDeVenta(venta) );
		venta.setSaldo( calcularSaldoDeVenta(venta) );
		
		//ventaService.saveVenta(venta);
		
		return "OK";
	}
	    
	@RequestMapping(value = "/tpv/ventas", method=RequestMethod.GET )
	public ModelAndView listarClientes(HttpServletRequest request) throws UnknownHostException  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		 
				
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			//SelectQuery oSelectQuery = new SelectQuery { QueryString = "select * from win32_printer" };
			
			//Solo permite ingresar a la seccion de clientes a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("61")) {
				
				if (idClientSel!=null ) {
					
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				
				}else {
					modelAndView.addObject("clientesel",true);
				}
				
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
						modelAndView.addObject("ventashoyant", ventashoyant.get(0));
					}else {
						modelAndView.addObject("ventashoyant", null);
					}
				
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("clientes",clienteService.findAllInOrder(idEmpresa) );		
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				
			
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}else {
					modelAndView.addObject("ventaActiva", null);
				}
				
				modelAndView.setViewName("tpv/ventas/listar");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}

	@RequestMapping(value = "/tpv/ventas/reutilizar", method=RequestMethod.GET )
	public ModelAndView reutilizarticket(HttpServletRequest request)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		
		Calendar calendar = Calendar.getInstance();
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				
				if (idClientSel!=null ) {
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				}else {
					modelAndView.addObject("clientesel",true);
				}
				
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
					if(idUltimaVentaProcesada != null) {
						Venta ventaActivaModif = ventaService.findOne(idUltimaVentaProcesada);
						if(ventaActivaModif != null ) {
							ventaActivaModif.setEstado("En Espera");
							ventaActivaModif.setFecha_modificacion( calendar.getTime() );
							ventaService.saveVenta(ventaActivaModif);
						}
					}
					request.getSession().setAttribute("ultimaVentaProcesada",ventashoyant.get(0).getId());
					idUltimaVentaProcesada=ventashoyant.get(0).getId();
					ventashoyant.get(0).setEstado("Activa");
					ventashoyant.get(0).setFecha_modificacion( calendar.getTime() );
					ventaService.saveVenta(ventashoyant.get(0));
					
					if(idUltimaVentaProcesada != null) {
						Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
						
						if(ventaActiva != null ) {
							modelAndView.addObject("ventaActiva", ventaActiva);
						}
					}else {
						modelAndView.addObject("ventaActiva", null);
					}
				
				}
				
				List<Venta> ventashoyantact= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyantact.size()>0) {
					if(ventashoyantact.get(0).getServicios().size()==0) {
						modelAndView.addObject("ventashoyant", ventashoyantact.get(0));
						
					}else {
						modelAndView.addObject("ventashoyant", null);
					}
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("clientes",clienteService.findAllInOrder(idEmpresa) );		
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
		
				
				modelAndView.setViewName("tpv/ventas/listar");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/{id}/{operacion}", method=RequestMethod.GET )
	public ModelAndView ReabrirPendiente(HttpServletRequest request,@PathVariable(value = "id") Long id,@PathVariable(value = "operacion") Integer operacion)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		
		if (rol != null) {
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			//1 - Eliminar 2- Eliminar y reabrir
			if(operacion==1 || operacion==2) {
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						ventaActiva.setBorrado(date);
						ventaActiva.setEstado("Anulado");
						ventaService.saveVenta(ventaActiva);
					}
				}
			}
			//3 - Enviar a Pendiente 4- Enviar a pendiente y reabrir
			if(operacion==3 || operacion==4) {
				if(idUltimaVentaProcesada != null) {
					
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						ventaActiva.setEstado("En Espera");
						ventaActiva.setFecha_modificacion( date );
						ventaService.saveVenta(ventaActiva);
					}
				}
			}
			
			if(operacion==3 || operacion==1) {
				
				Venta venta = new Venta();
				
				//Se debe verificar si existen ventas en el día para saber que numero_venta asignarle a la venta en proceso
				Boolean ventasEnElDia = ventaService.verifyIfExistsVentaInTheDayByIdOfSucursal(date, idSucursal);
				if(ventasEnElDia == true) {
					Venta ultimaVenta = ventaService.findLastVentaByIdOfSucursal(date, idSucursal);
					venta.setNumero_venta(ultimaVenta.getNumero_venta()+1);
				}else {
					venta.setNumero_venta(1);
				}
			
				venta.setMarca(marcaService.findOne(idMarca));
				venta.setEmpresa(empresaService.findOne(idEmpresa));
				venta.setSucursal(sucursalService.findOne(idSucursal));
				venta.setEmpleado(user);
				venta.setAgente(user.getNombre() + " " +user.getApellido());
				venta.setTipo("T");
				venta.setEstado("Activa");
				venta.setObservaciones("");
				venta.setCaja(0);
				venta.setFecha(date);
				venta.setHora(date);
				venta.setTotal_neto(0.0);
				venta.setTotal_iva(0.0);
				venta.setTotal(0.0);
				venta.setPagado(0.0);
				venta.setSaldo(0.0);
				venta.setTotal_descuento(0.0);
				
				//Se establece el formato de los ids del ticket de la venta
				DateFormat dfy = new SimpleDateFormat("yy");
				DateFormat dfm = new SimpleDateFormat("MM");
				Calendar cal = Calendar.getInstance();
		
				String idVentaSucursal = String.format("%03d", Integer.valueOf(idSucursal.intValue()));
				String idVentaAno = dfy.format(Calendar.getInstance().getTime());
				String idVentaMes = dfm.format(Calendar.getInstance().getTime());
				String idVentaDia = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
				String idVentaNumero = String.format("%05d", venta.getNumero_venta());
				String idCortoTicket = idVentaAno + idVentaMes + idVentaDia + idVentaNumero;
				String idLargoTicket = 'S' + idVentaSucursal + venta.getTipo() + idVentaAno + idVentaMes + idVentaDia + idVentaNumero;
				
				venta.setId_corto_ticket(idCortoTicket);
				venta.setId_largo_ticket(idLargoTicket);
				//-------------------------------------------
			
				ventaService.saveVenta(venta);
				
				request.getSession().setAttribute("ultimaVentaProcesada",venta.getId());
				modelAndView.addObject("ventaActiva", venta);
			}
			//Si la operación es eliminar o enviar a pendientes y reabrir una venta
			if(operacion==4 || operacion==5) {
				
					Venta ventaActiva = ventaService.findOne(id);
					if(ventaActiva != null && (ventaActiva.getEstado().equals("En Espera") || ventaActiva.getEstado().equals("Activa"))) {
						ventaActiva.setEstado("Activa");
						ventaActiva.setFecha_modificacion( date );
						ventaService.saveVenta(ventaActiva);
						if(id!=idUltimaVentaProcesada && idUltimaVentaProcesada!=null) {
							Venta ventaActual = ventaService.findOne(idUltimaVentaProcesada);
							
							if(ventaActual.getEstado().equals("Activa")) {
								ventaActual.setEstado("En Espera");
								ventaActual.setFecha_modificacion( date );
								ventaService.saveVenta(ventaActual);
							}
						}
						
						request.getSession().setAttribute("ultimaVentaProcesada",id);
					}
				
			}
			if(operacion==6) {
				
				Venta ventaActiva = ventaService.findOne(id);
				
				request.getSession().setAttribute("ultimaVentaProcesada",id);
			}
			
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				
				if (idClientSel!=null ) {
					
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				
				}else {
					modelAndView.addObject("clientesel",true);
				}
				
				
				if(operacion==5 || operacion==6) {
					modelAndView.setViewName("redirect:/tpv/ventas/pagos");
				}else {
					modelAndView.setViewName("redirect:/tpv/ventas");
				}
				
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventasdev/{id}", method=RequestMethod.GET )
	public ModelAndView ReabrirVenta(HttpServletRequest request,@PathVariable(value = "id") String id)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
	
		Calendar calendar = Calendar.getInstance();
		
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		
		if (rol != null) {
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			Venta ventadev = ventaService.findByIdCortoTicketAndSucursal(id, idSucursal);
			Venta ventaActiva = ventaService.findOne(ventadev.getId());
			
			if(ventaActiva != null ) {
				ventaActiva.setEstado("Activa");
				ventaActiva.setFecha_modificacion( calendar.getTime() );
				ventaService.saveVenta(ventaActiva);
				
				request.getSession().setAttribute("ultimaVentaProcesada",ventadev.getId());
			}
				
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
							
			if (modulosPermisos.contains("TPV - Ventas")) {
				
				if (idClientSel!=null ) {
					
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				
				}else {
					modelAndView.addObject("clientesel",true);
				}
				
				modelAndView.setViewName("redirect:/tpv/ventas/pagos");
				
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/venta",method=RequestMethod.GET)
	public @ResponseBody ModelAndView crearVenta(HttpServletRequest request,RedirectAttributes flash)  {
		ModelAndView modelAndView = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
	
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("clientes",clienteService.findAllInOrder(idEmpresa) );	
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("familias",catalogoService.findListFamiliasByIdOfSucursal(idSucursal));
				
				if(idClientSel!=null ) {
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				}else {
					modelAndView.addObject("clientesel",true);
				}
				
				
				Venta venta = new Venta();
				
				Date date = new Date(Calendar.getInstance().getTime().getTime());
				
				//Se debe verificar si existen ventas en el día para saber que numero_venta asignarle a la venta en proceso
				Boolean ventasEnElDia = ventaService.verifyIfExistsVentaInTheDayByIdOfSucursal(date, idSucursal);
				if(ventasEnElDia == true) {
					Venta ultimaVenta = ventaService.findLastVentaByIdOfSucursal(date,idSucursal);
					
					venta.setNumero_venta(ultimaVenta.getNumero_venta()+1);
				}else {
					venta.setNumero_venta(1);
				}
				
				venta.setMarca(marcaService.findOne(idMarca));
				venta.setEmpresa(empresaService.findOne(idEmpresa));
				venta.setSucursal(sucursalService.findOne(idSucursal));
				venta.setEmpleado(user);
				venta.setAgente(user.getNombre() + " " +user.getApellido());
				venta.setTipo("T");
				venta.setEstado("Activa");
				venta.setObservaciones("");
				venta.setCaja(0);
				venta.setFecha(date);
				venta.setHora(date);
				venta.setTotal_neto(0.0);
				venta.setTotal_iva(0.0);
				venta.setTotal(0.0);
				venta.setPagado(0.0);
				venta.setSaldo(0.0);
				venta.setTotal_descuento(0.0);
				
				
				//Se establece el formato de los ids del ticket de la venta
				DateFormat dfy = new SimpleDateFormat("yy");
				DateFormat dfm = new SimpleDateFormat("MM");
				Calendar cal = Calendar.getInstance();
		
				String idVentaSucursal = String.format("%03d", Integer.valueOf(idSucursal.intValue()));
				String idVentaAno = dfy.format(Calendar.getInstance().getTime());
				String idVentaMes = dfm.format(Calendar.getInstance().getTime());
				String idVentaDia = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
				String idVentaNumero = String.format("%05d", venta.getNumero_venta());
				String idCortoTicket = idVentaAno + idVentaMes + idVentaDia + idVentaNumero;
				String idLargoTicket = 'S' + idVentaSucursal + venta.getTipo() + idVentaAno + idVentaMes + idVentaDia + idVentaNumero;
				
				venta.setId_corto_ticket(idCortoTicket);
				venta.setId_largo_ticket(idLargoTicket);
				//-------------------------------------------
			
				ventaService.saveVenta(venta);
				
				request.getSession().setAttribute("ultimaVentaProcesada",venta.getId());
				
				modelAndView.addObject("ventaActiva", venta);
				
				modelAndView.setViewName("/tpv/ventas/listar");
				return modelAndView;
			}
		}
		
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/consumoInterno",method=RequestMethod.GET)
	public @ResponseBody ModelAndView crearVentaConsumoInterno(HttpServletRequest request,RedirectAttributes flash)  {
		ModelAndView modelAndView = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
	
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}		
			if (modulosPermisos.contains("TPV - Ventas")) {
				
				Venta venta = new Venta();
				
				Date date = new Date(Calendar.getInstance().getTime().getTime());
				
				//Se debe verificar si existen ventas en el día para saber que numero_venta asignarle a la venta en proceso
				Boolean ventasEnElDia = ventaService.verifyIfExistsVentaInTheDayByIdOfSucursal(date, idSucursal);
				if(ventasEnElDia == true) {
					Venta ultimaVenta = ventaService.findLastVentaByIdOfSucursal(date,idSucursal);
					venta.setNumero_venta(ultimaVenta.getNumero_venta()+1);
				}else {
					venta.setNumero_venta(1);
				}
				
				venta.setMarca(marcaService.findOne(idMarca));
				venta.setEmpresa(empresaService.findOne(idEmpresa));
				venta.setSucursal(sucursalService.findOne(idSucursal));
				venta.setEmpleado(user);
				venta.setAgente(user.getNombre() + " " +user.getApellido());
				venta.setTipo("T");
				venta.setEstado("Activa");
				venta.setObservaciones("");
				venta.setCaja(0);
				venta.setFecha(date);
				venta.setHora(date);
				venta.setTotal_neto(0.0);
				venta.setTotal_iva(0.0);
				venta.setTotal(0.0);
				venta.setPagado(0.0);
				venta.setSaldo(0.0);
				venta.setTotal_descuento(0.0);
				
				//Se asigna el cliente de "Consumo Interno" que tiene la empresa
				Cliente cliente=clienteService.findClienteConsumoInternoByIdEmpresa(idEmpresa);
				venta.setCliente(cliente);
				
				//Se establece el formato de los ids del ticket de la venta
				DateFormat dfy = new SimpleDateFormat("yy");
				DateFormat dfm = new SimpleDateFormat("MM");
				Calendar cal = Calendar.getInstance();
		
				String idVentaSucursal = String.format("%03d", Integer.valueOf(idSucursal.intValue()));
				String idVentaAno = dfy.format(Calendar.getInstance().getTime());
				String idVentaMes = dfm.format(Calendar.getInstance().getTime());
				String idVentaDia = String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
				String idVentaNumero = String.format("%05d", venta.getNumero_venta());
				String idCortoTicket = idVentaAno + idVentaMes + idVentaDia + idVentaNumero;
				String idLargoTicket = 'S' + idVentaSucursal + venta.getTipo() + idVentaAno + idVentaMes + idVentaDia + idVentaNumero;
				
				venta.setId_corto_ticket(idCortoTicket);
				venta.setId_largo_ticket(idLargoTicket);
				//-------------------------------------------
			
				ventaService.saveVenta(venta);
				
				request.getSession().setAttribute("ultimaVentaProcesada",venta.getId());
				
				modelAndView.setViewName("redirect:/tpv/ventas/familia");
				return modelAndView;
			}
		}
		
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/familia", method=RequestMethod.GET )
	public ModelAndView listarFamilia(HttpServletRequest request,RedirectAttributes flash)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			
			if(idClientSel!=null) {
				if(clienteService.findOne(idClientSel)==null) {
					
					modelAndView.addObject("clientesel",true );
				}else{
					modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
					}
			}else {
				modelAndView.addObject("clientesel",true);
			}			
			
			//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
			if(idUltimaVentaProcesada != null) {
				Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
				if(ventaActiva != null ) {
					modelAndView.addObject("ventaActiva", ventaActiva);
				}
			}else {
				modelAndView.addObject("ventaActiva", null);
			}
			
			//Solo permite ingresar a la seccion de familias a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("62")) {
				
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("familias",catalogoService.findListFamiliasByIdOfSucursal(idSucursal));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));

				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}
				

				modelAndView.setViewName("tpv/ventas/familia");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/{idCliente}/cliente/{idTicket}", method=RequestMethod.GET)
	public ModelAndView ClienteFamilia(HttpServletRequest request, @PathVariable(value = "idCliente") Long idCliente,@PathVariable(value = "idTicket") Long idTicket)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		Calendar calendar = Calendar.getInstance();
		
		Cliente cliente=clienteService.findOne(idCliente);	
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("clientes",clienteService.findAllInOrder(idEmpresa) );
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));
				modelAndView.addObject("familias",catalogoService.findListFamiliasByIdOfSucursal(idSucursal));
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				
				
				if(idCliente==0) {
					idCliente=null;
					request.getSession().setAttribute("idClientSel",null);
					
				}else {
					request.getSession().setAttribute("idClientSel",idCliente);
				}
				
				if(idCliente==null ) {
					modelAndView.addObject("clientesel",true);
					//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				
						Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
						if(ventaActiva != null ) {
							ventaActiva.setCliente(null);
							ventaActiva.setFecha_modificacion( calendar.getTime() );
							ventaService.saveVenta(ventaActiva);
							modelAndView.addObject("ventaActiva", ventaActiva);
	
						}else {
						modelAndView.addObject("ventaActiva", null);
					}
					modelAndView.setViewName("tpv/ventas/familia");
					return modelAndView;
				}
				if(idCliente!=null ) {
					
						modelAndView.addObject("clientesel",clienteService.findOne(idCliente));
						//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
						if(idUltimaVentaProcesada != null) {
							Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
							if(ventaActiva != null ) {
								ventaActiva.setCliente(cliente);
								ventaActiva.setFecha_modificacion( calendar.getTime() );
								ventaService.saveVenta(ventaActiva);
								modelAndView.addObject("ventaActiva", ventaActiva);
		
							}
						}else {
							modelAndView.addObject("ventaActiva", null);
						}
						
						modelAndView.setViewName("tpv/ventas/familia");
						return modelAndView;
				}
				
					
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/pendientes", method=RequestMethod.GET )
	public ModelAndView ListarPendientes(HttpServletRequest request,RedirectAttributes flash)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");

		if (rol != null) {
			
			if(idUltimaVentaProcesada != null) {
				Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
				if(ventaActiva != null ) {
					modelAndView.addObject("ventaActiva", ventaActiva);
					
				}
			}
			
			if(idClientSel!=null) {
				if(clienteService.findOne(idClientSel)==null) {
					
					modelAndView.addObject("clientesel",true );
				}else{
					modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
					}
			}else {
				modelAndView.addObject("clientesel",true);
			}
			
			//Solo permite ir a la seccion de Pendientes a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("68")) {
				List<Venta> venta = ventaService.findLastPendientesByDateAndSucursal(date,idSucursal);
				List<Venta> ventasant = ventaService.findLastAnterioresByDateAndSucursal(date,idSucursal);
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("pendienteshoy", venta);
				modelAndView.addObject("pendientesant", ventasant);
				
			
				modelAndView.setViewName("tpv/ventas/pendientes");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	
	@RequestMapping(value = "/tpv/ventas/{id}/productos",method=RequestMethod.GET)
	public ModelAndView listarProductos(HttpServletRequest request, @PathVariable(value = "id") Long idFamilia)  {
		
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		request.getSession().setAttribute("ultimaFamConsultada",idFamilia);
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
                modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.addObject("catalogos",catalogoService.findListArticulosByIdOfSucursalAndFam(idSucursal, idFamilia));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				if(idClientSel!=null) {
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				}
				
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}
				
				modelAndView.setViewName("tpv/ventas/productos");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/productos",method=RequestMethod.GET)
	public ModelAndView listarProductosAnt(HttpServletRequest request)  {
		
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idFamilia=(Long)request.getSession().getAttribute("ultimaFamConsultada");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			//Solo permite ir a la seccion de Productos a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("63")) {
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
                modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
                
                if(idFamilia!=null) {
                	modelAndView.addObject("catalogos",catalogoService.findListArticulosByIdOfSucursalAndFam(idSucursal, idFamilia));
                }else {
                	modelAndView.addObject("catalogos",catalogoService.findListFavoritosByIdOfSucursal(idSucursal));
                }
				
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				if(idClientSel!=null) {
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						}
				}
				
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}
				
				modelAndView.setViewName("tpv/ventas/productos");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/favoritos",method=RequestMethod.GET)
	public ModelAndView listarProductosFavoritos(HttpServletRequest request)  {
		
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			//Solo permite ir a la seccion de Productos a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("63")) {
				
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
                modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
                modelAndView.addObject("catalogos",catalogoService.findListFavoritosByIdOfSucursal(idSucursal));
                
				
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				if(idClientSel!=null) {
					if(clienteService.findOne(idClientSel)==null) {
						
						modelAndView.addObject("clientesel",true );
					}else{
						modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
						};
				}
				
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}
				
				modelAndView.setViewName("tpv/ventas/productos");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	
	
	
	@RequestMapping(value = "/tpv/ventas/factura/{idLargoTicket}", produces = {"application/pdf" }, method=RequestMethod.GET )
	public String generarTicket(HttpServletRequest request, Model model)  {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
						model.addAttribute("ventashoyant", ventashoyant.get(0));
					}else {
						model.addAttribute("ventashoyant", null);
					}
				
				model.addAttribute("user", user );
				model.addAttribute("modulosPermisos", modulosPermisos);
				model.addAttribute("funcionalidadesPermisos", funcionalidadesPermisos);
				model.addAttribute("marca", marcaService.findOne(idMarca));
				model.addAttribute("empresa", empresaService.findOne(idEmpresa));
				model.addAttribute("empresaSeleccionada", empresaService.findOne(idEmpresa));
				model.addAttribute("clientes",clienteService.findAllInOrder(idEmpresa) );		
				model.addAttribute("sucursal", sucursalService.findOne(idSucursal));
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						model.addAttribute("ventaActiva", ventaActiva);
					}
				}
				
				return "tpv/ventas/factura";
			}
		}

		return "redirect:/acceso_denegado";
	}
	
	@RequestMapping(value="/tpv/ventas/vendedores", method = RequestMethod.GET)
	public ModelAndView ListarVendedores(HttpServletRequest request, RedirectAttributes flash){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(),idSucursal);
		
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
						
			if (modulosPermisos.contains("TPV - Ventas")) {
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
				
						modelAndView.addObject("ventashoyant", ventashoyant.get(0));
					}else {
						modelAndView.addObject("ventashoyant", null);
					
				}
				modelAndView.addObject("user", user );
				List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
				Integer sucursalsize=sucursales.size();
				modelAndView.addObject("sucursalsize",sucursalsize);
				if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
					modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
				}
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				if(clienteService.findOne(idClientSel)==null) {
					
					modelAndView.addObject("clientesel",true );
				}else{
					modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
					}
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				
				//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}
				
				modelAndView.setViewName("tpv/ventas/vendedor");
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");

		return modelAndView;
	}
	

	
	@RequestMapping(value = "/tpv/ventas/crear", method=RequestMethod.GET )
	public ModelAndView paginaCrearClientes(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		if (rol != null) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			//Solo permite crear clientes a los usuarios que tengan esa funcionalidad asignada a su rol
			if (funcionalidadesPermisos.contains("50")) {
				List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
				if( ventashoyant.size()>0) {
					
						modelAndView.addObject("ventashoyant", ventashoyant.get(0));
					}else {
						modelAndView.addObject("ventashoyant", null);
					
				}
				if(idUltimaVentaProcesada != null) {
					Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
					if(ventaActiva != null ) {
						modelAndView.addObject("ventaActiva", ventaActiva);
					}
				}else {
					modelAndView.addObject("ventaActiva", null);
				}
				modelAndView.addObject("user", user );
				modelAndView.addObject("modulosPermisos", modulosPermisos);
				modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
				modelAndView.addObject("marca", marcaService.findOne(idMarca));
				modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
				modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
				modelAndView.addObject("cliente", new Cliente() );
				modelAndView.addObject("paises", paisService.findAll());
				modelAndView.addObject("provincias", provinciaService.findListProvinciasByNombreOfPais("España"));
				modelAndView.addObject("poblaciones", poblacionService.findListPoblacionesByNombreOfPais("España"));
				modelAndView.addObject("documentos", tipoDocumentoService.findAll() );
				modelAndView.addObject("nacionalidades", tipoNacionalidadService.findAll() );
				modelAndView.addObject("sexos", tipoSexoService.findAll() );
				modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
				modelAndView.setViewName("tpv/ventas/crear");
				
				return modelAndView;
			}
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;	
	}
	
	@RequestMapping(value="/tpv/ventas/crear", method = RequestMethod.POST)
	public ModelAndView crearCliente(HttpServletRequest request, @ModelAttribute @Valid Cliente cliente, @RequestParam("file") MultipartFile imagen, BindingResult result, ModelAndView modelAndView, RedirectAttributes flash ,SessionStatus status){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
		Empresa empresa = empresaService.findOne(idEmpresa);
		Long idClientSel=(Long)request.getSession().getAttribute("idClientSel");
		boolean flagError=false;
		Path rutaCompleta;
		
		modelAndView.addObject("user", user);

        cliente.setEmpresa(empresa);
        
    	List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
		if( ventashoyant.size()>0) {
			
				modelAndView.addObject("ventashoyant", ventashoyant.get(0));
			}else {
				modelAndView.addObject("ventashoyant", null);
			
		}
		modelAndView.addObject("modulosPermisos", modulosPermisos);
        modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
        modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
        modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
        modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
        modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("documentos", tipoDocumentoService.findAll() );
		modelAndView.addObject("nacionalidades", tipoNacionalidadService.findAll() );
		modelAndView.addObject("sexos", tipoSexoService.findAll() );
		modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
		
		
		//Para chequear que el campo documento sea unico dentro de los clientes de la empresa
		if(cliente.getDocumento().equals("")==false && cliente.getDocumento().equals(null)==false){
	  		if( clienteService.findRegistroByDocumentoAndEmpresa(cliente.getDocumento(),idEmpresa) != null) {
				modelAndView.addObject("errorDocumento", "El Nro. Documento ya está registrado para un cliente de la empresa");
				flagError=true;
	  		}
		}
		
  		//Para chequear que el campo email sea unico dentro de los clientes de la empresa
		if(cliente.getEmail().equals("")==false && cliente.getEmail().equals(null)==false) {
	  		if( clienteService.findRegistroByEmailAndEmpresa(cliente.getEmail(),idEmpresa) != null) {
				modelAndView.addObject("errorEmail", "El email ya está registrado para un cliente de la empresa");
				flagError=true;
	  		}
		}
		
		//Para chequear que el campo teléfono sea unico dentro de los clientes de la empresa
		if(cliente.getTelefono().equals("")==false && cliente.getTelefono().equals(null)==false) {
	  		if(clienteService.findRegistroByTlfAndEmpresa(cliente.getTelefono(),idEmpresa) != null) {
				modelAndView.addObject("errorTelefono", "El teléfono ya está registrado para un cliente de la empresa");
				flagError=true;
	  		}
		}
				
		//Validando campos vaíos y guardando campos con valor null
		if(cliente.getDocumento().equals("")==true ) {
			cliente.setDocumento("");
  		}
  		
		if(cliente.getEmail().equals("")==true) {
			cliente.setEmail("");
  		}
		
		if(cliente.getTelefono().equals("")==true) {
			cliente.setTelefono("");
  		}
		//Cierre de validación de campos vacíos

				
  		if(cliente.getDescuento() != null && (cliente.getDescuento()>100 || cliente.getDescuento()<1)) {
			modelAndView.addObject("errorDescuento", "El valor debe estar comprendido entre 1 a 100");
			flagError=true;
  		}
  		
		if(result.hasErrors() || flagError==true) {
			modelAndView.addObject("cliente", cliente);
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			modelAndView.addObject("paises", paisService.findAll());
			modelAndView.addObject("provincias", provinciaService.findListProvinciasByNombreOfPais("España"));
			modelAndView.addObject("poblaciones", poblacionService.findListPoblacionesByNombreOfPais("España"));
			modelAndView.setViewName("tpv/ventas/crear");
			return modelAndView;
		}else{
			if (idClientSel!=null) {
				if(clienteService.findOne(idClientSel)==null) {
					
					modelAndView.addObject("clientesel",true );
				}else{
					modelAndView.addObject("clientesel",clienteService.findOne(idClientSel) );
					}
				
			}	else {
				modelAndView.addObject("clientesel",true);
			
			}
			
			//Si se recibe imagen de formulario
			if (!imagen.isEmpty()) {	
				//Para asignar un nombre unico a la imagen
				String uniqueFilename = UUID.randomUUID().toString() + "_" + imagen.getOriginalFilename();
				rutaCompleta = Paths.get(ROOT_PATH_IMAGES_CLIENTES + "\\" + uniqueFilename);	
				
				try {
					Files.copy(imagen.getInputStream(), rutaCompleta);
					cliente.setImagen(uniqueFilename);

				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}else {			
				cliente.setImagen("");
			}
			
			cliente.setSucursal(sucursalService.findOne(idSucursal));
			
			Integer nro_clientes_empresa = clienteService.getNroClientesByIdEmpresa(idEmpresa);
			cliente.setNro_cliente(String.valueOf(nro_clientes_empresa+1));
			
			String sucstring=sucursalService.findOne(idSucursal).getNro_sucursal();
			String clistring=Long.toString(nro_clientes_empresa + 1);
			cliente.setNro_socio(sucstring+"-"+clistring);
			
			clienteService.saveCliente(cliente);
			
			String mensajeFlash = "El cliente '" + cliente.getNombre() + "' fue creado con éxito";	
			status.setComplete();
			flash.addFlashAttribute("success",mensajeFlash);
			modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
			
			modelAndView.setViewName("redirect:/tpv/ventas");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/tpv/ventas/{id}/editar", method=RequestMethod.GET)
	
	
	
	@RequestMapping(value="/tpv/ventas/{id}/editar", method = RequestMethod.POST)
	public ModelAndView editarCliente(HttpServletRequest request, @PathVariable(value = "id") Long id, @ModelAttribute @Valid Cliente cliente, @RequestParam("file") MultipartFile imagen, BindingResult result, ModelAndView modelAndView, RedirectAttributes flash ,SessionStatus status){
		
		Cliente clienteRegistrado= clienteService.findOne(id);
		boolean flagError = false;
		Path rutaCompleta;
		Calendar calendar = Calendar.getInstance();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rol = marEmpSucUsuRolService.findRolOfModoUsuarioByIdOfUsuarioAndIdOfSucursal(user.getId(), idSucursal);
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rol.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rol.getRol().getId());
		modelAndView.addObject("user", user);
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
        modelAndView.addObject("modulosPermisos", modulosPermisos);
        modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
        
        //Para chequear que el campo documento sea unico dentro de los clientes de la empresa
  		if(cliente.getDocumento().equals("")==false && clienteService.findRegistroByDocumentoAndEmpresa(cliente.getDocumento(),idEmpresa) != null) {
  			if(cliente.getDocumento().equals(clienteRegistrado.getDocumento() ) == false ) {
				modelAndView.addObject("errorDocumento", "El Nro. Documento ya está registrado para un cliente de la empresa");
				flagError=true;
  			}
  		}
  		
  		//Para chequear que el campo email sea unico dentro de los clientes de la empresa
  		if(cliente.getEmail().equals("")==false && clienteService.findRegistroByEmailAndEmpresa(cliente.getEmail(),idEmpresa) != null) {
  			if(cliente.getEmail().equals(clienteRegistrado.getEmail() ) == false ) {
				modelAndView.addObject("errorEmail", "El email ya está registrado para un cliente de la empresa");
				flagError=true;
  			}
  		}
  		
  		//Para chequear que el campo telefono sea unico dentro de los clientes de la empresa
  		if(cliente.getTelefono().equals("")==false && clienteService.findRegistroByTlfAndEmpresa(cliente.getTelefono(),idEmpresa) != null) {
  			if(cliente.getTelefono().equals(clienteRegistrado.getTelefono() ) == false ) {
				modelAndView.addObject("errorTelefono", "El teléfono ya está registrado para un cliente de la empresa");
				flagError=true;
  			}
  		}
  		
  		//Validando campos vaíos y guardando campos con valor null
		if(cliente.getDocumento().equals("")==true ) {
			cliente.setDocumento("");
			
  		}
  		
		if(cliente.getEmail().equals("")==true) {
			cliente.setEmail("");
  		}
		
		if(cliente.getTelefono().equals("")==true) {
			cliente.setTelefono("");
  		}
		//Cierre de validación de campos vacíos

		
  		if(cliente.getDescuento() != null && (cliente.getDescuento()>100 || cliente.getDescuento()<1)) {
			modelAndView.addObject("errorDescuento", "El valor debe estar comprendido entre 1 a 100");
			flagError=true;
  		}
  		
		if(result.hasErrors() || flagError==true) {
			cliente.setImagen(clienteRegistrado.getImagen());
			
			modelAndView.setViewName("tpv/ventas/editar");
			List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
			if( ventashoyant.size()>0) {
				
					modelAndView.addObject("ventashoyant", ventashoyant.get(0));
				}else {
					modelAndView.addObject("ventashoyant", null);
				
			}
			modelAndView.addObject("documentos", tipoDocumentoService.findAll() );
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
	        modelAndView.addObject("marca", marcaService.findOne(idMarca));
	        modelAndView.addObject("cliente", cliente);
	        modelAndView.addObject("paises", paisService.findAll());
	        
	        if( cliente.getProvincia()!=null && cliente.getPoblacion()!=null ) {
	        	modelAndView.addObject("provincias", provinciaService.findListProvinciasByIdOfPais(cliente.getPais().getId()) );
				modelAndView.addObject("poblaciones", poblacionService.findListPoblacionesByIdOfProvincia(cliente.getProvincia().getId()) );
	        }
	        
			modelAndView.addObject("sexos", tipoSexoService.findAll() );
			modelAndView.addObject("empleados",marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
			
	        return modelAndView;
		}
		
		//Si se recibe imagen de formulario
		if (!imagen.isEmpty()) {

			//Se valida si Cliente ya tiene una imagen asisgnada
			//Esto es para casos donde se quiera cambiar la imagen asignada. Primero se debe eliminar la que ya tiene
			if(clienteRegistrado.getImagen()!=null && clienteRegistrado.getImagen().length() > 0) {
				
				rutaCompleta = Paths.get(ROOT_PATH_IMAGES_CLIENTES + "\\" + clienteRegistrado.getImagen());
				File archivo = rutaCompleta.toFile(); 
				if(archivo.exists() && archivo.canRead()) {
					archivo.delete();
				}
			}
			
			//Para asignar un nombre unico a la imagen
			String uniqueFilename = UUID.randomUUID().toString() + "_" + imagen.getOriginalFilename();
			rutaCompleta = Paths.get(ROOT_PATH_IMAGES_CLIENTES + "\\" + uniqueFilename);	
			
			try {
				Files.copy(imagen.getInputStream(), rutaCompleta);
				cliente.setImagen(uniqueFilename);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			if (clienteRegistrado.getImagen()!=null && clienteRegistrado.getImagen().length() > 0) {
				cliente.setImagen(clienteRegistrado.getImagen());
			}else {
				cliente.setImagen("");
			}
		}
		
		//Para que se mantengan al hacer la actualización los campos que no estan presentes en el formulario
		
		cliente.setEmpresa(clienteRegistrado.getEmpresa());
		cliente.setSucursal(clienteRegistrado.getSucursal());
		cliente.setNro_cliente(clienteRegistrado.getNro_cliente());
		cliente.setNro_socio(clienteRegistrado.getNro_socio());
		cliente.setBorrado(clienteRegistrado.getBorrado());
		cliente.setFecha_creacion(clienteRegistrado.getFecha_creacion());
		cliente.setFecha_modificacion( calendar.getTime() );
		cliente.setFecha_espejo(clienteRegistrado.getFecha_espejo());
		cliente.setRowguid(clienteRegistrado.getRowguid());
		
		modelAndView.addObject("clientesel",clienteService.findOne(id) );
		List<Venta> ventashoyant= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
		if( ventashoyant.size()>0) {
			if(ventashoyant.get(0).getServicios().size()==0) {
				modelAndView.addObject("ventashoyant", ventashoyant.get(0));
			}else {
				modelAndView.addObject("ventashoyant", null);
			}
		}
	
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
        modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
        modelAndView.addObject("marca", marcaService.findOne(idMarca));
    	modelAndView.addObject("documentos", tipoDocumentoService.findAll() );
        
		String mensajeFlash = "El cliente '" + cliente.getNombre() + "' fue editado con éxito";
		
		clienteService.saveCliente(cliente);
		
		//Método utilizado para actualizar el descuento de todos los servicios de la venta activa. 
		//Por ejemplo: Si al cliente se le asigna un descuento del 10%, le aplicara ese descuento a todos los servicios de la venta
		/*Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");
		if(idUltimaVentaProcesada != null) {
			Venta ventaActiva = ventaService.findOne(idUltimaVentaProcesada);
			if(ventaActiva != null ) {
				
				if(cliente.getDescuento()!=null && ventaActiva.getCliente()!=null && ventaActiva.getCliente().getId()==cliente.getId()) {
				
					if(ventaActiva.getServicios()!=null) {
						
						for (int i = 0; i < ventaActiva.getServicios().size(); i++) {
						
							Servicio servicio = ventaActiva.getServicios().get(i);
						
							if( servicio.getTipo_transaccion().equals("Venta") == true  ){
								servicio.setDescuento_monto(0.0);
								servicio.setDescuento_porcentaje( cliente.getDescuento() );
								
								Double descuento_monto = 0.0;
								Double descuento_porcentaje = 0.0;
								if(servicio.getDescuento_monto() != null) {
									descuento_monto = servicio.getDescuento_monto();
								}
								if(servicio.getDescuento_porcentaje() != null) {
									descuento_porcentaje = calcularMontoSegunPorcentaje( servicio.getPvp() , servicio.getDescuento_porcentaje(), servicio.getUnidades());
								}
								
								Double total_servicio = ( servicio.getPvp() * servicio.getUnidades() ) -  descuento_monto - descuento_porcentaje;
								servicio.setTotal(total_servicio);
								servicio.setTotal_neto( calcularMontoBase(total_servicio, servicio.getPorcentaje_iva() ) );
								servicio.setTotal_iva( calcularMontoIva(total_servicio, servicio.getPorcentaje_iva() ) );
								
								servicio.setFecha_modificacion( calendar.getTime() );
								servicioService.saveServicio(servicio);
							}
						}
						
						//Se debe actualizar los atributos de la venta que se esta procesando		
						calcularTotalesDeLaVenta(ventaActiva);
						
						ventaActiva.setFecha_modificacion( calendar.getTime() );
						ventaService.saveVenta(ventaActiva);
					}
				}
				ventaService.saveVenta(ventaActiva);
				modelAndView.addObject("ventaActiva", ventaActiva);

			}
		}else {
			modelAndView.addObject("ventaActiva", null);
		}*/
		
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		
		modelAndView.setViewName("redirect:/tpv/ventas");
		
		return modelAndView;
	}
	
	
	@RequestMapping(value="/tpv/ventas/pendienteshoy/ajax")
	@ResponseBody
	public String PendientesHoyAjax(HttpServletRequest request,SessionStatus status){		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		List<Venta> ventashoyantact= ventaService.findByIdSucursalAndAntiguaHoy(idSucursal);
		String respuesta="vacío";
		
		if( ventashoyantact.size()>0) {		
			respuesta = "lleno";
		}else {
			respuesta = "vacío";
		}
		
		status.setComplete();
		
		return respuesta;
	}

	
	@RequestMapping(value="/tpv/ventas/favoritos/ajax/{id}")
	@ResponseBody
	public ModelAndView agregarFavoritoAjax(HttpServletRequest request, @ModelAttribute @Valid Catalogo catalogo, @PathVariable(value = "id") Long id,BindingResult result, ModelAndView modelAndView, RedirectAttributes flash ,SessionStatus status){		
		
		Calendar calendar = Calendar.getInstance();
		
		//Long idUltimaVentaProcesada = (Long)request.getSession().getAttribute("ultimaVentaProcesada");

		//Se debe verificar si hay una venta activa para mandar los datos a la vista y asi actualizar el panel de la derecha del modulo de ventas
		Catalogo catalog= catalogoService.findOne(id);
		boolean esfav = catalogoService.findOne(id).getFavoritos();
		
		if (esfav==false) {
			catalog.setFavoritos(true);
		}else {
			catalog.setFavoritos(false);
		}
		
		catalogo.setFecha_modificacion( calendar.getTime() );
		catalogoService.saveCatalogo(catalog);
	
		status.setComplete();
		
		modelAndView.setViewName("redirect:/tpv/ventas/productos");
		
		return modelAndView;
	}
	
    
}
