package com.saraworld.tpvonline.controller;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.transform.Source;

import org.apache.commons.io.FileUtils;
import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.itextpdf.text.BaseColor;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.model.Cliente;
import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.Familia;
import com.saraworld.tpvonline.model.Articulo;
import com.saraworld.tpvonline.model.Catalogo;
import com.saraworld.tpvonline.model.Informes;
import com.saraworld.tpvonline.model.GrupoInformes;
import com.saraworld.tpvonline.model.MarEmpSucUsuRol;
import com.saraworld.tpvonline.model.Marca;
import com.saraworld.tpvonline.model.Pago;
import com.saraworld.tpvonline.model.Usuario;
import com.saraworld.tpvonline.model.Venta;
import com.saraworld.tpvonline.service.MarEmpSucUsuRolService;
import com.saraworld.tpvonline.service.RolService;
import com.saraworld.tpvonline.service.UsuarioService;
import com.saraworld.tpvonline.service.GrupoInformesService;
import com.saraworld.tpvonline.service.VentaService;
import com.saraworld.tpvonline.service.FamiliaService;
import com.saraworld.tpvonline.service.CatalogoService;
import com.saraworld.tpvonline.util.PageRender;
import com.saraworld.tpvonline.service.InformesService;
import com.saraworld.tpvonline.service.EmpresaService;
import com.saraworld.tpvonline.service.MarcaService;
import com.saraworld.tpvonline.service.SucursalService;
import com.saraworld.tpvonline.service.ClienteService;
import com.saraworld.tpvonline.service.ColeccionService;
import com.saraworld.tpvonline.service.ArticuloService;

@Controller
public class InformesController {
	
	@Autowired
	private UsuarioService userService;
	@Autowired
	private RolService rolService;
	@Autowired
	private MarEmpSucUsuRolService marEmpSucUsuRolService;
	@Autowired
	private SucursalService sucursalService;
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private InformesService informesService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private GrupoInformesService grupoinformesService;
	@Autowired
	private VentaService ventaService;
	@Autowired
	private CatalogoService catalogoService;
	@Autowired
	private ColeccionService coleccionService;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private ArticuloService articuloService;
	@Autowired
	private FamiliaService familiaService;
	
	
	@Value("${spring.datasource.url}")
	private String urlBD;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.password}")
	private String password;
	@Value("${spring.datasource.driverClassName}")
	private String urldriverServer;
	
	private final static String ROOT_PATH_PDF_FACTURAS = "C:\\\\TPV_Online_PDF\\\\";
	
	@RequestMapping(value = "/tpv/listados", method=RequestMethod.GET )
	public ModelAndView listar(RedirectAttributes flash, HttpServletRequest request,@RequestParam(name = "page", defaultValue="0") int page) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");	
		boolean nuevoUsuario = marEmpSucUsuRolService.verifyIfIsNewUsuario(user.getId());
		
		if (rolAdmin != null && nuevoUsuario == false ) {
			List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
			List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
			List<GrupoInformes> grupoinformes=grupoinformesService.findListGrupoInformesByIdOfSucursal(idSucursal);
			List<GrupoInformes> groupall= grupoinformesService.findListGrupoInformesByTodasLasSucursales();
			List<GrupoInformes> grouplist = new ArrayList<>();
			*/
			modelAndView.addObject("listafiltro", grupoinformesService.findAll());
			modelAndView.addObject("user", user );
			modelAndView.addObject("userinf", user );
			modelAndView.addObject("modulosPermisos", modulosPermisos);
			modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
			List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
			Integer sucursalsize=sucursales.size();
			modelAndView.addObject("sucursalsize",sucursalsize);
			if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
				modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
			}
			
			modelAndView.addObject("marca", marcaService.findOne(idMarca));
			modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
			modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
			modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
			modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));	
			modelAndView.setViewName("tpv/informaciones/listados");
			return modelAndView;
			
		}
		modelAndView.setViewName("redirect:/acceso_denegado");
		return modelAndView;
	}
	
	private Page<Object[]> convertListToPage(int page, ArrayList<Object[]> listado) {
		Pageable pageable = new PageRequest(page, 10);
		
		//Porción de código que permite convertir un objeto List a un objeto Page
		int inicio = pageable.getOffset();
		int fin = (inicio + pageable.getPageSize()) > listado.size() ? listado.size() : (inicio + pageable.getPageSize());
		Page<Object[]> informes = new PageImpl<Object[]>(listado.subList(inicio, fin), pageable, listado.size());
		
		return informes;
	}
	
	
	public List<String> Listacamposantes(String cadena){
		List<String> campos= new ArrayList<String>();
		String TextoBuscado="#";
		while (cadena.indexOf(TextoBuscado) > -1) {
			String[] split=cadena.split(TextoBuscado);
			String part=cadena.substring(split[0].lastIndexOf(" ")+1,cadena.indexOf(TextoBuscado));
			part=part.replaceAll(" ","");
			boolean noespeciales=false;
			while(noespeciales!=true) {
			//Condicional que verifica si hay mas caracteres que no tengan que ver con operaciones de comparación
			if(part.substring(part.length()-1, part.length()).equals("<")==false && part.substring(part.length()-1, part.length()).equals(">")==false && part.substring(part.length()-1, part.length()).equals("=")==false ) {
				part=part.substring(0,part.length()-1);
				
			
			}else {
				noespeciales=true;
			}
			
			}	
			
			//Condicional que verifica si antes hay espacios
			if(part.substring(0, 1).equals("<") || part.substring(0, 1).equals(">") || part.substring(0, 1).equals("=") || part.substring(0, 1).equals("!")) {
				boolean esta=false;
				Integer apariciones=1;
				split[0]=split[0].substring(0, split[0].indexOf(part.substring(0, 1)));
				
				while(esta!=true) {
					
					String verifspace=split[0].substring(split[0].length()-1, split[0].length());
					if(verifspace.equals(" ")) {
						split[0]=split[0].substring(0,split[0].length()-1);
						
						apariciones++;
					}else {
						String space=" ";String result=IntStream.range(1, apariciones).mapToObj(i -> space).collect(Collectors.joining(""));
						String campotant=split[0].substring(split[0].lastIndexOf(" "), split[0].length()).replaceAll(" ", "");
						String campocomplet=campotant+result+part;
						campos.add(campocomplet);
						esta=true;
					}
				}
			}else {
				campos.add(part);
			}
			String comilla =cadena.substring(cadena.indexOf(
			        TextoBuscado)-1,cadena.indexOf(
					        TextoBuscado));
			cadena = cadena.substring(cadena.indexOf(
		        TextoBuscado)+1,cadena.length());
			
		     	String rest=cadena.substring(0,cadena.indexOf(TextoBuscado));
		     	 if(campos.contains(rest)==false) {
			    	  if(rest.toUpperCase() != null) {
			    		 
			    		  rest=rest.substring(0, 1)+rest.substring(1, rest.length());
			    		  	if(comilla.equals("'")) {
			    		  		campos.add("'#"+rest+"#'");
			    		  	}else {
			    		  		campos.add("#"+rest+"#");
			    		  	}
			    				
			    	  }
			    	  cadena=cadena.substring(rest.length()+1,cadena.length());
			    	  
			      }else {
			    	  cadena=cadena.substring(rest.length()+1,cadena.length());
			      }
		     	
		}
		
		return campos;
		
	}
	
	public List<String> CamposCheckbox(String cadena,HttpServletRequest request) {
		String nombreqery = (String)request.getSession().getAttribute("Nombrequery");
		String query = (String)request.getSession().getAttribute("Nombrequery");
		
		char TextoBuscado='#'; 
		List<String> campos= new ArrayList<String>();
		while (cadena.indexOf(TextoBuscado) > -1) {
			
			cadena = cadena.substring(cadena.indexOf(
		        TextoBuscado)+1,cadena.length());
		     	String rest=cadena.substring(0,cadena.indexOf(TextoBuscado));
		      if(campos.contains(rest)==false) {
		    	  if(rest.toUpperCase() != null) {
		    		 
		    		  rest=rest.substring(0, 1)+rest.substring(1, rest.length()).toLowerCase();
		    		  if(rest.equals("Usuariolog")==false && rest.equals("Desde")==false &&  rest.equals("Hasta")==false && rest.equals("Sucursal")==false && rest.equals("Empresa")==false && rest.equals("Marca")==false) {
		    			  
		    				  campos.add(rest);
		    		  }
		    		  
		    	  }
		    	  cadena=cadena.substring(rest.length()+1,cadena.length());
		    	  
		      }else {
		    	  cadena=cadena.substring(rest.length()+1,cadena.length());
		      }
		      
		      
		}
		HashSet<String> hs = new HashSet<>();
		hs.addAll(campos);
		campos.clear();
		campos.addAll(hs);
		
		return campos;
	}
	
	public String limpiarCadena(String queCadena,String buscaEnCadena,String campo, boolean check, String valorFinal, Long SQLNumSucursal,HttpServletRequest request) {
		int inner = 0;
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		if(queCadena.contains(" AND ")) {
			queCadena=queCadena.replaceAll("AND", "and");
		}
		  if(queCadena.contains("#MARCA#") ){
			  queCadena = queCadena.replaceAll(campo, idMarca.toString());
		  }
		  
		  if(queCadena.contains("#EMPRESA#" )){
			  queCadena = queCadena.replaceAll(campo, idEmpresa.toString());
		  }
		  if(queCadena.contains("#SUCURSAL#") ){
			  queCadena = queCadena.replaceAll(campo, idSucursal.toString());
		  }
		if(queCadena.contains("#USUARIOLOG#")) {
			queCadena = queCadena.replaceAll("#USUARIOLOG#", user.getId().toString());
		}
		if(queCadena.indexOf("JOIN") > 0) {
			inner = 1;
			}
		
		String delante ="";
		String detras = "";

		if(check==true ) {
			
			queCadena = queCadena.replaceAll(campo, valorFinal);
		
			
		}
		if(queCadena.indexOf(campo) > 0)
		{
			if(check==true ) {
				
			}else
			{
				if(inner != 0) //Hay Join
					
				{
					
				
						
					
				} 
				String[] mitades=queCadena.split(buscaEnCadena+campo);
				if(mitades.length>1) {
					if(mitades[0].contains("#")) {
						if(mitades[0].contains("AND")){
							if(mitades[0].lastIndexOf("AND")>-1) {
								delante=mitades[0].substring(mitades[0].lastIndexOf("AND"), mitades[0].length());
							
								delante=delante.replaceAll(" ", "");
							}
							
						}
						if(mitades[0].contains("and")){
							if(mitades[0].lastIndexOf("and")>-1) {
								delante=mitades[0].substring(mitades[0].lastIndexOf("and"), mitades[0].length());
								
								delante=delante.replaceAll(" ", "");
							}
							
						}
					}
					
					
						if( mitades[1].contains("AND")) {
							if(mitades[1].indexOf("AND")>-1) {
								detras = mitades[1].substring(mitades[1].indexOf("AND"),mitades[1].indexOf("AND")+3);
								
								detras=detras.replaceAll(" ", "");
							
							}
							
						}
						 if( mitades[1].contains("and")) {
							if(mitades[1].indexOf("and")>-1) {
								detras = mitades[1].substring(mitades[1].indexOf("and"),mitades[1].indexOf("and")+3);
								detras=detras.replaceAll(" ", "");
							}
							
						}
					
					 
				} 
				
				
				
				if(delante.equals("AND") || delante.equals("and"))
				{
					
					if(delante.equals("AND")) {
						String sustituir = queCadena.substring(queCadena.lastIndexOf("AND"),queCadena.lastIndexOf("AND")+4);
						queCadena = queCadena.replaceAll(sustituir, "");
					}
					if(delante.equals("and")) {
						
						String sustituir = queCadena.substring(queCadena.lastIndexOf("and"),queCadena.lastIndexOf("and")+4);
						queCadena = queCadena.replaceAll(sustituir, "");
					}
					
				}
				if(delante.equals("AND")==false || delante.equals("and")==false)
				{
					if(detras.equals("AND") || detras.equals("and"))
					{
						
						if(detras.equals("AND") )
						{
							Integer longandatras=mitades[1].substring(0, mitades[1].indexOf("AND")+3).length();
							String sustituir = queCadena.substring(queCadena.indexOf(buscaEnCadena+campo) ,queCadena.indexOf(buscaEnCadena+campo)+(buscaEnCadena+campo).length()+longandatras);
							
							queCadena = queCadena.replaceAll(sustituir, "");
						}
						
						if(detras.equals("and") )
						{
							Integer longandatras=mitades[1].substring(0, mitades[1].indexOf("and")+3).length();
							String sustituir = queCadena.substring(queCadena.indexOf(buscaEnCadena+campo) ,queCadena.indexOf(buscaEnCadena+campo)+(buscaEnCadena+campo).length()+longandatras);
						
							queCadena = queCadena.replaceAll(sustituir, "");
						}
						
					}
					else
					{
						
						String where ="";
						if(queCadena.contains("where")){
							where=queCadena.substring(queCadena.lastIndexOf("where"),queCadena.lastIndexOf("where")+5);
						
							}
						if( queCadena.contains("WHERE")) {
							where=queCadena.substring(queCadena.lastIndexOf("WHERE"),queCadena.lastIndexOf("WHERE")+5);
							
						}
						if(where.equals("where") || where.equals("WHERE"))
						{
							String sustituir = queCadena.substring(queCadena.indexOf(where),queCadena.indexOf(buscaEnCadena+campo)+(buscaEnCadena+campo).length() );
							
							queCadena = queCadena.replaceAll(sustituir, "");
							
						}else {
							if(mitades.length==1) {
								if(queCadena.lastIndexOf("AND")>-1){
								
										delante=queCadena.substring(queCadena.lastIndexOf("AND"), queCadena.length());
									
								}
								if(queCadena.lastIndexOf("and")>-1){
										delante=queCadena.substring(queCadena.lastIndexOf("and"), queCadena.length());
									
								}
								queCadena = queCadena.replaceAll(delante, "");
							}
						}
						
					}
				}
			}
		}
		
		return queCadena;
	}
	
	
	@GetMapping(value="/tpv/grupoinformes/ajax/{id}")
	@ResponseBody
	public  List<String> ListarFiltrosporGrupo(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "id") Long id, BindingResult result,  RedirectAttributes flash ,SessionStatus status) {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		List<String> informes= new ArrayList<>();
		 request.getSession().setAttribute("queryaux",null);
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		
		modelAndView.addObject("user", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("listafiltro", grupoinformesService.findListGrupoInformesByIdOfSucursal(idSucursal));
		modelAndView.addObject("listafiltrogrupo", informesService.findListInformesByIdOfSucursalAndGrupo(idSucursal, id));
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("agentes", marEmpSucUsuRolService.findListUsuariosByIdSucursalTPV(idSucursal));
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		
		List<Informes> informessu=informesService.findListInformesByIdOfGrupo(id);
		 for (int i = 0; i < informessu.size(); i++) { 
			 	informes.add(informessu.get(i).getId().toString());
			 	informes.add(informessu.get(i).getNombre());
		 }
		
		return informes;
	}
	
	@GetMapping(value="/tpv/listainformespdf/{doc}")
	@ResponseBody
	public  String ListarPDF(ModelAndView modelAndView,HttpServletRequest request, BindingResult result, @PathVariable(value = "doc") String doc,  RedirectAttributes flash ,SessionStatus status) {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		String informes= (String)request.getSession().getAttribute("listadoquery");
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		request.getSession().setAttribute("tipodoc",doc);
		modelAndView.addObject("user", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("listafiltro", grupoinformesService.findListGrupoInformesByIdOfSucursal(idSucursal));
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("agentes", marEmpSucUsuRolService.findListUsuariosByIdSucursalTPV(idSucursal));
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		
		
		return informes;
	}
	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping(value="/tpv/enviaremail", method=RequestMethod.POST)
	public  String EnviarFile(ModelAndView modelAndView,HttpServletRequest request, BindingResult result, @RequestParam Map<String, String> requestParams, RedirectAttributes flash ,SessionStatus status) throws FileNotFoundException, DocumentException {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		String tipodoc = (String)request.getSession().getAttribute("tipodoc");
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		
		modelAndView.addObject("user", user );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("listafiltro", grupoinformesService.findListGrupoInformesByIdOfSucursal(idSucursal));
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		ArrayList<Object[]> listadom=new ArrayList<>();
		listadom = (ArrayList<Object[]>)request.getSession().getAttribute("listado");
		
		Object[] cabecera= (Object[])request.getSession().getAttribute("cabecera");
		
		if(tipodoc.equals("pdf")) {
			 Document document = new Document();
			  PdfPTable table = new PdfPTable(listadom.size()); 
			  table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			  for(int c=0;c<cabecera.length;c++){
				  table.addCell(cabecera[c].toString());
			  }
			  table.setHeaderRows(1);
			  PdfPCell[] cells = table.getRow(0).getCells(); 
			  Color custom = new Color(255,255,255);
			  Color border = new Color(0,0,0);
			  for (int j=0;j<cells.length;j++){
			     cells[j].setBackgroundColor(custom);
			     cells[j].setBorderColor(border);
			  }
		          for (int i=0;i<listadom.size();i++){
		        	  Object[] object=listadom.get(i);
		        	  for(int j=0;j<object.length;j++) {
		        		  table.addCell(object[j].toString()); 
		        	  }
		          }
			  PdfWriter.getInstance(document, new FileOutputStream(new File(ROOT_PATH_PDF_FACTURAS + "Informe.pdf")));
			  document.open();
		          document.add(table);
			  document.close();
			
			MimeMessage message = mailSender.createMimeMessage();
			 String emailCliente= requestParams.get("email-informe");
		        
		        String [] correos = emailCliente.split(";");
			try {
				MimeMessageHelper helper = new MimeMessageHelper(message, true);
				
				helper.setFrom("swtpvonline@gmail.com"); 
				helper.setTo(correos);
				helper.setSubject("Informe" );
				helper.setText("Estimado cliente, se adjunta el documento del informe solicitado en formato PDF.\n\n");
			
				FileSystemResource fileinf = new FileSystemResource(ROOT_PATH_PDF_FACTURAS + "Informe.pdf");
				helper.addAttachment(fileinf.getFilename(), fileinf);
				
				
				
			} catch (MessagingException e) {
				throw new MailParseException(e);
			
			}
			
			mailSender.send(message);
			
			File fileinf = new File(ROOT_PATH_PDF_FACTURAS + "Informe.pdf");
	         
			fileinf.delete();
		}else {
			 try {
				 	
					
					String jsonm = (String)request.getSession().getAttribute("listadoquery");
					jsonm="{\"infile\":"+jsonm+"}";
					JSONObject output = new JSONObject(jsonm);
		            File file=new File(ROOT_PATH_PDF_FACTURAS +"Informe.csv");
		           String csv =CDL.toString(new JSONArray(output.get("infile").toString()));
		           FileUtils.writeStringToFile(file, csv);
		           MimeMessage message = mailSender.createMimeMessage();
					 String emailCliente= requestParams.get("email-informe");
				        
				        String [] correos = emailCliente.split(";");
					try {
						MimeMessageHelper helper = new MimeMessageHelper(message, true);
						
						helper.setFrom("swtpvonline@gmail.com");
						helper.setTo(correos);
						helper.setSubject("Informe" );
						helper.setText("Estimado cliente, se adjunta el documento del informe solicitado en formato CSV.\n\n");
					
						FileSystemResource fileinf = new FileSystemResource(ROOT_PATH_PDF_FACTURAS + "Informe.csv");
						helper.addAttachment(fileinf.getFilename(), fileinf);
						
						
						
					} catch (MessagingException e) {
						throw new MailParseException(e);
					
					}
					
					mailSender.send(message);
					
					File fileinf = new File(ROOT_PATH_PDF_FACTURAS + "Informe.csv");
			         
					fileinf.delete();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }  
				
		}
		 
	
		String referer = request.getHeader("Referer");
		
		if( referer.contains("tpv/listados") ) {
			return "redirect:/tpv/listados";
		}else {
			return "redirect:"+referer;
		}
		
	}
	
	@RequestMapping(value="/tpv/check/ajax/{id}")
	@ResponseBody
	public ModelAndView ListarCheck(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "id") Long id,BindingResult result,  RedirectAttributes flash ,SessionStatus status) {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", empresaService.findOne(idEmpresa) );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("listafiltro", informesService.findAll());
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfMarca(idMarca));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		modelAndView.addObject("agentes", marEmpSucUsuRolService.findListUsuariosByIdSucursal(idSucursal));
		modelAndView.addObject("clientes", clienteService.findAllInOrder(idEmpresa));
		modelAndView.addObject("familia", catalogoService.findListFamiliasByIdOfSucursal(idSucursal));
		modelAndView.addObject("tpago", ventaService.findByIdSucursaltoListTipo(idSucursal));
		modelAndView.addObject("colecciones",coleccionService.findListColeccionesByIdOfSucursal(idSucursal));
		modelAndView.addObject("productos",catalogoService.findListCatalogosByIdOfSucursal(idSucursal));
		Informes informe= informesService.findOne(id);
		 request.getSession().setAttribute("Query",id);
		String queryInfo=informe.getQuery();
		if(queryInfo.contains("#DESDE#") || queryInfo.contains("#HASTA#")) {
			
			modelAndView.addObject("fechainf",true);
			
			request.getSession().setAttribute("fecha",true);
		}else {
			
			modelAndView.addObject("fechainf",false );
			
			request.getSession().setAttribute("fecha",false);
		}
		if(informe.getNombre().equals("Clientes")) {
			queryInfo=queryInfo.replaceAll("#EMPRESA#", idEmpresa.toString());
		}
		List<String> camposcheck=CamposCheckbox(queryInfo,request);
		 if(camposcheck!=null) {
	    	   modelAndView.addObject("checkbox", camposcheck);	
	    	 
	       }
		
		 
		
		status.setComplete();
		
		modelAndView.setViewName("tpv/informaciones/listaCheckbox");
	
		
		return modelAndView;
	}

	
	@RequestMapping(value="/tpv/checkinformes/ajax/{valores}/{criterios}")
	@ResponseBody
	public ModelAndView ListarInformesCheck(ModelAndView modelAndView,HttpServletRequest request, @PathVariable(value = "valores") List<String> valores,  @PathVariable(value = "criterios") List<String> criterios,BindingResult result,  RedirectAttributes flash ,SessionStatus status) throws ParseException {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");	
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		Long idQuery =(Long)request.getSession().getAttribute("Query");
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		List<String> modulosPermisos = rolService.fingByIdMyListModulos(rolAdmin.getRol().getId());
		List<String> funcionalidadesPermisos = rolService.fingByIdMyListFuncionalidades(rolAdmin.getRol().getId());
		List<Sucursal> sucursales = marEmpSucUsuRolService.findListSucursalesByIdUsuarioAndByIdEmpresaForTPV(user.getId(), idEmpresa);
		Integer sucursalsize=sucursales.size();
		modelAndView.addObject("sucursalsize",sucursalsize);
		if(ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).size()>=1) {
			modelAndView.addObject("ventavacia", ventaService.findByIdSucursalAndAntiguaHoy(idSucursal).get(0));
		}
		modelAndView.addObject("user", user );
		modelAndView.addObject("userinf", empresaService.findOne(idEmpresa) );
		modelAndView.addObject("modulosPermisos", modulosPermisos);
		modelAndView.addObject("funcionalidadesPermisos", funcionalidadesPermisos);
		modelAndView.addObject("listafiltro", informesService.findAll());
		modelAndView.addObject("marca", marcaService.findOne(idMarca));
		modelAndView.addObject("empresa", empresaService.findOne(idEmpresa));
		modelAndView.addObject("sucursal", sucursalService.findOne(idSucursal));
		modelAndView.addObject("sucursales", marEmpSucUsuRolService.findListSucursalesByIdOfEmpresa(idEmpresa));	
		modelAndView.addObject("empresaSeleccionada", empresaService.findOne(idEmpresa));
		Informes informe= informesService.findOne(idQuery);
		String queryInfo=informe.getQuery();
		queryInfo=queryInfo.replace("\"","'");
	
		List<String> camposcheck=CamposCheckbox(queryInfo,request);
		 request.getSession().setAttribute("camposcheck",camposcheck);
		 if(camposcheck!=null) {
	    	   modelAndView.addObject("checkbox", camposcheck);	
	    	   for (int i = 0; i < camposcheck.size(); i++) { 

	  			 if(camposcheck.get(i).equals("Marca")) {
	  			
	  				List<MarEmpSucUsuRol> marcas = marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
	  				 if(marcas!=null) {
	  					 
	  					 modelAndView.addObject("marcas", marcas); 
	  				 }
	  				 
	  			 }
	  			 if(camposcheck.get(i).equals("Empresa")) {
		  			
	  				List<MarEmpSucUsuRol> listaEmpresas = marEmpSucUsuRolService.findMarcaAndEmpresaByIdOfUsuario(user.getId());
	  				 if(listaEmpresas!=null) {
	  					 
	  					 modelAndView.addObject("empresa", listaEmpresas); 
	  				 }
	  				 
	  			 }
	  			 if(camposcheck.get(i).equals("Cliente")) {
			  			
		  				List<Cliente> clientes = clienteService.findAllInOrder(idEmpresa);
		  				 if(clientes!=null) {
		  					 
		  					 modelAndView.addObject("clientes",clientes); 
		  				 }
		  				 
		  			 }
	                      
	            } 
	       }
			
		 String id="";
		 String criterio="";
		 Integer count=0;
		 
		 
		 while (count<valores.size()) {	
			
			 id=valores.get(count);
			 String fechavalida="\\d{4}-\\d{2}-\\d{2}";
			 criterio=criterios.get(count).toUpperCase();
			 String campo2="#"+criterio+"#";
	    	  if( queryInfo.contains("fecha_creacion") && Pattern.matches(fechavalida,id)) {
	    		  if(campo2.equals("#DESDE#")) {
	    			  id=id+" 00:00:00";
	    		  }
	    		  if(campo2.equals("#HASTA#")) {
	    			  id=id+" 12:59:59";
	    		  }
	    	  }
			
			 queryInfo= queryInfo.replaceAll(campo2, id);
			
			 count++;
		 }
	
		 List<String> camposant=Listacamposantes(queryInfo);
	
		 Integer countcamps=0;
		 
		 while(countcamps<camposant.size()) {
			
			 queryInfo= limpiarCadena(queryInfo,camposant.get(countcamps),camposant.get(countcamps+1),false,"",(long)0,request);
			 countcamps+=2;
			
		 }
		 System.out.println(queryInfo);
		 if(queryInfo!="") {
				try {
				      Connection conexion;
				      Statement consulta;            
				      ResultSet data;
				            
				      String usuario = username;
				      String pass = password;
				            
				      // Conectamos con la base de datos

				      Class.forName(urldriverServer);

				      conexion=DriverManager.getConnection(
				    		  urlBD, usuario, pass );
				        
				      // Obtenemos los nombres de los campos de 
				     

				  
				      String comandoCampos = queryInfo;
				      consulta = conexion.createStatement();
				      data = consulta.executeQuery(comandoCampos);
				            
				     
				      ResultSetMetaData rsm =data.getMetaData(); 
				  
				   	Gson objGson = new GsonBuilder().setPrettyPrinting().create();
					
				      int columnCount = rsm.getColumnCount();
				      ArrayList<Object[]> listado=new ArrayList<>();
				      List<Map<String,Object> > listjson=new ArrayList<>();
				      Object[] cabecera=new Object[columnCount] ; 
				      String fechavalida="\\d{4}-\\d{2}-\\d{2}";
				      for (int i = 1; i <= columnCount; i++ ) {
				    	  String columna=rsm.getColumnName(i).substring(0, 1).toUpperCase() + rsm.getColumnName(i).substring(1); 
				    		if(columna.contains("_")) {
				    			columna.replaceAll("_", " ");
				    		}

							
				    		cabecera[i-1]=columna ;
				    	}
				     
				      while(data.next())
				      {
				    	  Object[] filas=new Object[rsm.getColumnCount()]; 
				    	 
				            for (int i = 0; i < filas.length; i++) { 
						    		if(data.getObject(i+1)!=null && Pattern.matches(fechavalida,data.getObject(i+1).toString())) {
						    			
						    			SimpleDateFormat mdyFormat = new SimpleDateFormat("dd-MM-yyyy");
						    			String mdy = mdyFormat.format(data.getObject(i+1));
						    			 mdy=mdy.replaceAll("-", "/");
						    			filas[i]=mdy.toString();
						    		}else {
				                filas[i]=data.getObject(i+1); 
				                
						    		}   
				            } 
				            listado.add(filas); 
				            request.getSession().setAttribute("listado",listado);
				            Map<String,Object> columnMap = new LinkedHashMap<String,Object>();
			                for(int columnIndex=1;columnIndex<=rsm.getColumnCount();columnIndex++)
			                {
			                    if(data.getString(rsm.getColumnName(columnIndex))!=null) {
			                    		if( data.getString(rsm.getColumnName(columnIndex))!=null && Pattern.matches(fechavalida, data.getString(rsm.getColumnName(columnIndex)).toString())) {
						    			
						    			SimpleDateFormat mdyFormat = new SimpleDateFormat("dd-MM-yyyy");
						    			String mdy = mdyFormat.format(data.getObject(rsm.getColumnName(columnIndex)));
						    			 mdy=mdy.replaceAll("-", "/");
						    			 columnMap.put(rsm.getColumnLabel(columnIndex),  mdy   );
						    		}else {
						    			 columnMap.put(rsm.getColumnLabel(columnIndex),     data.getString(rsm.getColumnName(columnIndex)));
				                
						    		}   
			                       
			                }else {
			                	columnMap.put(rsm.getColumnLabel(columnIndex), "");
			                }
			                        
			                }
			               
			                listjson.add(columnMap);
			              
				    	
				      }
				   
				     String jsonm = objGson.toJson(listjson);
				       consulta.close();
				       //Cierre de consulta
				       request.getSession().setAttribute("listadoquery",jsonm);
				      
						if(listado.size()!=0) {
							request.getSession().setAttribute("cabecera",cabecera);
							request.getSession().setAttribute("listadoObject",listado);
						       modelAndView.addObject("cabecera", cabecera);
						       modelAndView.addObject("listado",listado);
						}else {
							request.getSession().setAttribute("cabecera","");
						       modelAndView.addObject("cabecera", null);
						       modelAndView.addObject("listado",null);
						}
						
						
				       if(camposcheck!=null) {
				    	   modelAndView.addObject("checkbox", camposcheck);	
				       }
				    
					} catch (ClassNotFoundException ex) {
				         System.err.println("Errorriver");
				         ex.printStackTrace();
				      } catch (SQLException ex) {
				         System.err.println("Errorcceso a base de datos");
				         ex.printStackTrace();
				      }
				
				}
		
		status.setComplete();
		
		modelAndView.setViewName("tpv/informaciones/listaInformes");
	
		
		return modelAndView;
	}
	
	@GetMapping(value = "/tpv/litados/buscar/{criterio}/ajax")
	@ResponseBody
	public String listarIhformesSegunBusqueda(HttpServletRequest request, @PathVariable(value = "criterio") String criterio, @RequestParam String busqueda)  {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = userService.findByEmail(auth.getName());
		MarEmpSucUsuRol rolAdmin = marEmpSucUsuRolService.findRolOfPanelAdministracionByIdOfUsuario(user.getId());
		Long idEmpresa = (Long)request.getSession().getAttribute("empresa");
		Long idSucursal = (Long)request.getSession().getAttribute("sucursal");
		Long idMarca =(Long)request.getSession().getAttribute("marca");
		String jsons ="";
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		JSONObject objson = new JSONObject();
		List<Object[]> listadosjson = new ArrayList<Object[]>();
		
		if(criterio.equals("cliente")) {
			List<Cliente> listados = clienteService.findByNombreOrApellidoAndEmpresaList(busqueda, idEmpresa);
			
		
			for (int i=0; i< listados.size(); i++) {
				
				Object[] temp= new Object[3];
				temp[0]= listados.get(i).getId();
				temp[1]=listados.get(i).getNombre();
				temp[2]= listados.get(i).getApellido();
				listadosjson.add(temp);
			}
			 jsons = objGson.toJson(listadosjson);
		}

		if(criterio.equals("agente")) {
				List<Usuario> listados = marEmpSucUsuRolService.findListUsuariosByEmpresaSucursalMarca(busqueda, idEmpresa,idSucursal,idMarca);
				
			
				for (int i=0; i< listados.size(); i++) {
					
					Object[] temp= new Object[3];
					temp[0]= listados.get(i).getId();
					temp[1]=listados.get(i).getNombre();
					temp[2]= listados.get(i).getApellido();
					listadosjson.add(temp);
				}
				 jsons = objGson.toJson(listadosjson);
			}
		if(criterio.equals("producto")) {
			List<Articulo> articulos = articuloService.findListArticulosByIdOfMarcaAndNombreOrCodBarraOrCodigoOfArticulo(idMarca, busqueda);
			 jsons = objGson.toJson(articulos);
		}
		
		if(criterio.equals("familia")) {
			List<Familia> familia = familiaService.findListFamiliasByIdOfMarcaAndIdOrNombreOfFamilia(idMarca, busqueda);
			 jsons = objGson.toJson(familia);
		}
		
		
		
		
		
		return jsons;
	}
	
	
}
