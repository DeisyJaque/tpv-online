package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Gastos;
import com.saraworld.tpvonline.model.Lotes;
import com.saraworld.tpvonline.repository.LotesRepository;

@Service("loteService")
public class LoteService {
	private LotesRepository loteRepository;

	@Autowired
	public LoteService (LotesRepository LotesRepository) {
		this.loteRepository = LotesRepository;
	}
	
	@Transactional
	public void saveLotes(Lotes Lotes) {
		loteRepository.save(Lotes);
	}
	
	
	@Transactional(readOnly = true)
	public List<Lotes> findAll(){
		return (List<Lotes>)loteRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Lotes findOne(Long id) {
		// TODO Auto-generated method stub
		return loteRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<Lotes> LotesBySucursal(Long idSucursal){
		return (List<Lotes>)loteRepository.LotesBySucursal(idSucursal);
	}
	
	
}

