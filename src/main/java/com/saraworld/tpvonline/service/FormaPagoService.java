package com.saraworld.tpvonline.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.FormaPago;
import com.saraworld.tpvonline.model.Sucursal;
import com.saraworld.tpvonline.repository.FormaPagoRepository;

@Service("formaPagoService")
public class FormaPagoService {
	private FormaPagoRepository formaPagoRepository;

	@Autowired
	public FormaPagoService(FormaPagoRepository formaPagoRepository) {
		this.formaPagoRepository = formaPagoRepository;
	}
	
	@Transactional
	public void saveFamilia(FormaPago formaPago) {
		formaPagoRepository.save(formaPago);
	}
	
	@Transactional(readOnly = true)
	public FormaPago findOne(Long id) {
		// TODO Auto-generated method stub
		return formaPagoRepository.findOne(id);
	}

	@Transactional
	public void delete(Long id) {
		formaPagoRepository.delete(id);
	}
	
	@Transactional(readOnly = true)
	public List<FormaPago> findAll(){
		return (List<FormaPago>)formaPagoRepository.findAll();
	}
	
	@Transactional
	public void saveFormaPago(FormaPago formapago) {
		formaPagoRepository.save(formapago);
	}
	
	@Transactional(readOnly = true)
	public FormaPago findByNombre(String nombre){
		return (FormaPago)formaPagoRepository.findByNombre(nombre);
	}
	
	@Transactional(readOnly = true)
	public List <FormaPago> findByNombreList(String nombre){
		return ( List <FormaPago>)formaPagoRepository.findByNombreList(nombre);
	}
	
	@Transactional(readOnly = true)
	public List <FormaPago> findFormasPagosByIdSucursal(Long id){
		return ( List <FormaPago>)formaPagoRepository.findFormasPagosByIdSucursal(id);
	}
	
	@Transactional(readOnly = true)
	public List<Sucursal> findListSucursalesByIdOfFormaPago(Long id){
		return (List<Sucursal>)formaPagoRepository.findListSucursalesByIdOfFormaPago(id);
	}
	
	@Transactional(readOnly = true)
	public List<Empresa> findListEmpresasByIdOfFormaPago(Long id){
		return (List<Empresa>)formaPagoRepository.findListEmpresasByIdOfFormaPago(id);
	}
	
	@Transactional(readOnly = true)
	public List<Object[]> findListFormasDePagoByIdOfSucursalAndIdOrNombreOfForma(Long idSucursal, String criterio){
		return (List<Object[]>)formaPagoRepository.findListFormasDePagoByIdOfSucursalAndIdOrNombreOfForma(idSucursal, criterio);
	}
	
	@Transactional(readOnly = true)
	public List<FormaPago> findListFormasPagoByIdOfEmpresa(Long id){
		return (List<FormaPago>)formaPagoRepository.findListFormasPagoByIdOfEmpresa(id);
	}
	
	@Transactional(readOnly = true)
	public FormaPago findByFormaAndUser(String idforma,Long iduser){
		return (FormaPago)formaPagoRepository.findByFormaAndUser(idforma,iduser);
	}
	
	@Transactional(readOnly = true)
	public List<FormaPago> findByUserAndEmpresa(Long id){
		return (List<FormaPago>)formaPagoRepository.findByUserAndEmpresa(id);
	}
	
	
}
