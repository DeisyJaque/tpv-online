package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.saraworld.tpvonline.model.Ayuda;
import com.saraworld.tpvonline.model.GrupoAyuda;
import com.saraworld.tpvonline.model.Informes;
import com.saraworld.tpvonline.repository.AyudaRepository;

@Service("ayudaService")
public class AyudaService {
	private AyudaRepository ayudaRepository;

	@Autowired
	public AyudaService(AyudaRepository AyudaRepository) {
		this.ayudaRepository = AyudaRepository;
	}
	
	@Transactional
	public void Ayuda(Ayuda Ayuda) {
		ayudaRepository.save(Ayuda);
	}
	
	
	@Transactional(readOnly = true)
	public List<Ayuda> findAll(){
		return (List<Ayuda>)ayudaRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Ayuda findOne(Long id) {
		// TODO Auto-generated method stub
		return ayudaRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<Ayuda> findListAyudaByIdOfGrupo(Long grupo_id){
		return (List<Ayuda>)ayudaRepository.findListAyudaByIdOfGrupo(grupo_id);
	}
	
	@Transactional(readOnly = true)
	public List<Ayuda> findListAyudaByOrden(){
		return (List<Ayuda>)ayudaRepository.findListAyudaByOrden();
	}
	
	@Transactional(readOnly = true)
	public List<Ayuda> findListAyudaByOrdenPorNombre(String busqueda){
		Pageable pageable = new PageRequest(0, 100);
		return (List<Ayuda>)ayudaRepository.findListAyudaByOrdenPorNombre(busqueda, pageable);
	}
}

