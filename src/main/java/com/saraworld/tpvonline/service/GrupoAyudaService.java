package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import com.saraworld.tpvonline.model.GrupoAyuda;
import com.saraworld.tpvonline.repository.GrupoAyudaRepository;

@Service("grupoayudaService")
public class GrupoAyudaService {
	private GrupoAyudaRepository grupoayudaRepository;

	@Autowired
	public GrupoAyudaService(GrupoAyudaRepository GrupoAyudaRepository) {
		this.grupoayudaRepository = GrupoAyudaRepository;
	}
	
	@Transactional
	public void GrupoAyuda(GrupoAyuda GrupoAyuda) {
		grupoayudaRepository.save(GrupoAyuda);
	}
	
	
	@Transactional(readOnly = true)
	public List<GrupoAyuda> findAll(){
		return (List<GrupoAyuda>)grupoayudaRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public GrupoAyuda findOne(Long id) {
		// TODO Auto-generated method stub
		return grupoayudaRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<GrupoAyuda> ListGrupoByOrder(){
		return (List<GrupoAyuda>)grupoayudaRepository.ListGrupoByOrder();
	}
	
	@Transactional(readOnly = true)
	public List<GrupoAyuda> ListGrupoByNombre(String busqueda){
		Pageable pageable = new PageRequest(0, 100);
		return (List<GrupoAyuda>)grupoayudaRepository.ListGrupoByNombre(busqueda, pageable);
	}
	
	
}

