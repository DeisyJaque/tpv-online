package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.GrupoInformes;
import com.saraworld.tpvonline.model.Informes;
import com.saraworld.tpvonline.repository.InformesRepository;

@Service("informeService")
public class InformesService {
	private InformesRepository informesRepository;

	@Autowired
	public InformesService(InformesRepository InformesRepository) {
		this.informesRepository = InformesRepository;
	}
	
	@Transactional
	public void Informes(Informes Informes) {
		informesRepository.save(Informes);
	}
	
	
	@Transactional(readOnly = true)
	public List<Informes> findAll(){
		return (List<Informes>)informesRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Informes findOne(Long id) {
		// TODO Auto-generated method stub
		return informesRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<Informes> findListInformesByIdOfSucursalAndGrupo(Long idSucursal, Long grupo_id){
		return (List<Informes>)informesRepository.findListInformesByIdOfSucursalAndGrupo(idSucursal,grupo_id);
	}
	
	@Transactional(readOnly = true)
	public List<Informes> findListInformesByIdOfGrupo( Long grupo_id){
		return (List<Informes>)informesRepository.findListInformesByIdOfGrupo(grupo_id);
	}
	
	
}

