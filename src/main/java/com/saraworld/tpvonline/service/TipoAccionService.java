package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.saraworld.tpvonline.model.TipoAccion;

import com.saraworld.tpvonline.repository.TipoAccionRepository;

@Service("tipoaccionService")
public class TipoAccionService{
	private TipoAccionRepository tipoaccionRepository;

	@Autowired
	public TipoAccionService(TipoAccionRepository bitacoraRepository) {
		this.tipoaccionRepository = bitacoraRepository;
	}
	
	@Transactional(readOnly = true)
	public TipoAccion findById(Long id) {
		return tipoaccionRepository.findById(id);
	}
	
	@Transactional
	public void saveTipoAccion(TipoAccion tipoaccion) {
		tipoaccionRepository.save(tipoaccion);
	}
	
	@Transactional(readOnly = true)
	public List<TipoAccion> findAll(){
		return (List<TipoAccion>)tipoaccionRepository.findAll();
	}
	
	
	
	
}
