package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Familia;
import com.saraworld.tpvonline.model.GrupoInformes;
import com.saraworld.tpvonline.repository.GrupoInformesRepository;

@Service("grupoinformesService")
public class GrupoInformesService {
	private GrupoInformesRepository grupoinformesRepository;

	@Autowired
	public GrupoInformesService(GrupoInformesRepository GrupoInformesRepository) {
		this.grupoinformesRepository = GrupoInformesRepository;
	}
	
	@Transactional
	public void GrupoInformes(GrupoInformes GrupoInformes) {
		grupoinformesRepository.save(GrupoInformes);
	}
	

	@Transactional(readOnly = true)
	public List<GrupoInformes> findAll(){
		return (List<GrupoInformes>)grupoinformesRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public GrupoInformes findOne(Long id) {
		// TODO Auto-generated method stub
		return grupoinformesRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<GrupoInformes> findListGrupoInformesByIdOfSucursal(Long id){
		return (List<GrupoInformes>)grupoinformesRepository.findListGrupoInformesByIdOfSucursal(id);
	}
	
	@Transactional(readOnly = true)
	public List<GrupoInformes> findListGrupoInformesByTodasLasSucursales(){
		return (List<GrupoInformes>)grupoinformesRepository.findListGrupoInformesByTodasLasSucursales();
	}
	
}

