package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Bitacora;
import com.saraworld.tpvonline.repository.BitacoraRepository;

@Service("bitacoraService")
public class BitacoraService {
	private BitacoraRepository bitacoraRepository;

	@Autowired
	public BitacoraService(BitacoraRepository bitacoraRepository) {
		this.bitacoraRepository = bitacoraRepository;
	}
	
	@Transactional
	public void saveBitacora(Bitacora bitacora) {
		bitacoraRepository.save(bitacora);
	}
	
	@Transactional(readOnly = true)
	public List<Bitacora> findListBitacoraByIdBitacoraInicio(Long idBitacora) {
		//List<Bitacora> bitacoraInicio = new ArrayList<Bitacora>();
		
		
		//bitacoraInicio = bitacoraRepository.findListBitacoraByIdBitacoraInicio(idBitacora);
		
		return (List<Bitacora>)bitacoraRepository.findListBitacoraByIdBitacoraInicio(idBitacora);
	}
	@Transactional(readOnly = true)
	public Long findBitacoraByIdUsuario(Long idBitaUser) {
	
		
		return bitacoraRepository.findBitacoraByIdUsuario(idBitaUser);
	}
	
	@Transactional(readOnly = true)
	public List<Bitacora> findAll(){
		return (List<Bitacora>)bitacoraRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Bitacora findOne(Long id) {
		// TODO Auto-generated method stub
		return bitacoraRepository.findOne(id);
	}
	@Transactional
	public void delete(Long id) {
		bitacoraRepository.delete(id);
		
	}
	@Transactional(readOnly = true)
	public List<Bitacora> findListBitacoraByUserId(Long id){
		return (List<Bitacora>)bitacoraRepository.findListBitacoraByUserId(id);
	}
	
}
