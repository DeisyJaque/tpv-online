package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Impresora;
import com.saraworld.tpvonline.repository.ImpresoraRepository;

@Service("impresoraService")
public class ImpresoraService {
	private ImpresoraRepository impresoraRepository;

	@Autowired
	public ImpresoraService(ImpresoraRepository ImpresoraRepository) {
		this.impresoraRepository = ImpresoraRepository;
	}
	
	@Transactional
	public void saveImpresora(Impresora Impresora) {
		impresoraRepository.save(Impresora);
	}
	
	
	@Transactional(readOnly = true)
	public List<Impresora> findAll(){
		return (List<Impresora>)impresoraRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Impresora findOne(Long id) {
		// TODO Auto-generated method stub
		return impresoraRepository.findOne(id);
	}
	
	
	
}
