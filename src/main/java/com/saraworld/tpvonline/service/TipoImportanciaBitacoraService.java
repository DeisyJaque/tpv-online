package com.saraworld.tpvonline.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.TipoImportanciaBitacora;
import com.saraworld.tpvonline.repository.TipoImportanciaBitacoraRepository;

@Service("tipoimportanciabitacoraService")
public class TipoImportanciaBitacoraService{
	private TipoImportanciaBitacoraRepository tipoimportanciabitacoraRepository;

	@Autowired
	public TipoImportanciaBitacoraService(TipoImportanciaBitacoraRepository tipoimportanciabitacoraRepository) {
		this.tipoimportanciabitacoraRepository = tipoimportanciabitacoraRepository;
	}
	
	@Transactional(readOnly = true)
	public TipoImportanciaBitacora findById(Long id) {
		return tipoimportanciabitacoraRepository.findById(id);
	}
	
	@Transactional
	public void savetipoimportanciabitacoraRepository(TipoImportanciaBitacora tipoimportanciabitacora) {
		tipoimportanciabitacoraRepository.save(tipoimportanciabitacora);
	}
	
	
}
