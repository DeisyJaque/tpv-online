package com.saraworld.tpvonline.service;



import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.saraworld.tpvonline.model.Gastos;
import com.saraworld.tpvonline.repository.GastosRepository;

@Service("gastosService")
public class GastosService {
	private GastosRepository gastosRepository;

	@Autowired
	public GastosService (GastosRepository GastosRepository) {
		this.gastosRepository = GastosRepository;
	}
	
	@Transactional
	public void saveGastos(Gastos Gastos) {
		gastosRepository.save(Gastos);
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> findAll(){
		return (List<Gastos>)gastosRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> GastosBySucursal(Long idSucursal, Long idEmpresa,Long idMarca){
		return (List<Gastos>)gastosRepository.GastosBySucursal(idSucursal,idEmpresa,idMarca);
	}
	
	
	@Transactional(readOnly = true)
	public Gastos findOne(Long id) {
		// TODO Auto-generated method stub
		return gastosRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> findByFechaAndSucursal(Long idSucursal, Long idEmpresa,Long idMarca,Date fechaDesde, Date fechaHasta){
		Pageable pageable = new PageRequest(0, 100);
		return (List<Gastos>)gastosRepository.findByFechaAndSucursal(idSucursal,idEmpresa,idMarca, fechaDesde, fechaHasta, pageable);
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> findByTipoGastoAndSucursal(String busqueda,Long idSucursal,Long idEmpresa,Long idMarca){
		return (List<Gastos>)gastosRepository.findByTipoGastoAndSucursal(busqueda,idSucursal,idEmpresa,idMarca);
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> findByTipoGastoAndSucursalAnteriores(String busqueda,Long idSucursal,Long idEmpresa,Long idMarca){
		Pageable pageable = new PageRequest(0, 100);
		return (List<Gastos>)gastosRepository.findByTipoGastoAndSucursalAnteriores(busqueda,idSucursal,idEmpresa,idMarca, pageable);
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> findByProveedorAndSucursal(String busqueda,Long idSucursal,Long idEmpresa,Long idMarca){
		return (List<Gastos>)gastosRepository.findByProveedorAndSucursal(busqueda,idSucursal,idEmpresa,idMarca);
	}
	
	@Transactional(readOnly = true)
	public List<Gastos> findByProveedorAndSucursalAnteriores(String busqueda,Long idSucursal,Long idEmpresa,Long idMarca){
		Pageable pageable = new PageRequest(0, 100);
		return (List<Gastos>)gastosRepository.findByProveedorAndSucursalAnteriores(busqueda,idSucursal,idEmpresa,idMarca,pageable);
	}
}

