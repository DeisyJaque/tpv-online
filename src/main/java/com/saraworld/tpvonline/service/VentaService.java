package com.saraworld.tpvonline.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Venta;
import com.saraworld.tpvonline.repository.VentaRepository;

@Service("ventaService")
public class VentaService {
	private VentaRepository ventaRepository;

	@Autowired
	public VentaService(VentaRepository ventaRepository) {
		this.ventaRepository = ventaRepository;
	}
	
	@Transactional
	public void saveVenta(Venta venta) {
		ventaRepository.save(venta);
	}
	
	
	@Transactional(readOnly = true)
	public Venta findOne(Long id) {
		// TODO Auto-generated method stub
		return ventaRepository.findOne(id);
	}

	@Transactional
	public void delete(Long id) {
		ventaRepository.delete(id);
		
	}
	
	@Transactional(readOnly = true)
	public List<Venta> findAll(){
		return (List<Venta>)ventaRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public List<Venta> findVentaByClienteAndSucursal(Long idClient,Long idSucu){
		return (List<Venta>)ventaRepository.findVentaByClienteAndSucursal(idClient,idSucu);
	}
	
	@Transactional(readOnly = true)
	public boolean verifyIfExistsVentaInTheDayByIdOfSucursal(Date date, Long idSucursal) {
		List <Venta> ventaDelDia = ventaRepository.verifyIfExistsVentaInTheDayByIdOfSucursal(date, idSucursal);
		if(ventaDelDia.isEmpty()) {
			return false;
		}else{
			return true;
		}
	}
	
	@Transactional(readOnly = true)
	public boolean verifyIfExistsDevolucionInTheDayByIdOfSucursal(Date date, Long idSucursal) {
		List <Venta> devolucionDelDia = ventaRepository.verifyIfExistsDevolucionInTheDayByIdOfSucursal(date, idSucursal);
		if(devolucionDelDia.isEmpty()) {
			return false;
		}else{
			return true;
		}
	}
	
	@Transactional(readOnly = true)
	public boolean verifyIfVentaTieneDevolucionByIdLargoTicketIdOfSucursal(String idLargoTicket, Long idSucursal) {
		Venta devolucion = ventaRepository.verifyIfVentaTieneDevolucionByIdLargoTicketIdOfSucursal(idLargoTicket, idSucursal);
		if(devolucion == null) {
			return false;
		}else{
			return true;
		}
	}
	
	@Transactional(readOnly = true)
	public Venta findLastVentaByIdOfSucursal(Date date, Long idSucursal) {
		List <Venta> ventas = ventaRepository.findLastVentaByIdOfSucursal(date, idSucursal);
		return ventas.get(0);
	}
	
	@Transactional(readOnly = true)
	public Venta findLastDevolucionByIdOfSucursal(Date date, Long idSucursal) {
		List <Venta> ventas = ventaRepository.findLastDevolucionByIdOfSucursal(date, idSucursal);
		return ventas.get(0);
	}
	
	@Transactional(readOnly = true)
	public List <Venta> findLastPendientesByDateAndSucursal(Date date,Long idSucursal) {
		return(List <Venta> )ventaRepository.findLastPendientesByDateAndSucursal(date,idSucursal);
	}
	
	@Transactional(readOnly = true)
	public List <Venta> findLastAnterioresByDateAndSucursal(Date date,Long idSucursal) {
		return(List <Venta> )ventaRepository.findLastAnterioresByDateAndSucursal(date,idSucursal);
	}
	
	@Transactional(readOnly = true)
	public List<Venta> findByIdSucursalAndAntiguaHoy(Long idSucursal) {
		return(List<Venta>)ventaRepository.findByIdSucursalAndAntiguaHoy(idSucursal);
	}
	
	@Transactional(readOnly = true)
	public List<Venta> findByIdTicketAndSucursal(String busqueda, Long idSucursal){
		List <Venta> ventas = ventaRepository.findByIdTicketAndSucursal(busqueda, idSucursal);
		
		if(ventas.size()>10) {
			return ventas.subList(0, 10);
		}else {
			return ventas;
		}
	}
	
	@Transactional(readOnly = true)
	public Venta findByIdCortoTicketAndSucursal(String idCortoTicket, Long idSucursal) {
		return ventaRepository.findByIdCortoTicketAndSucursal(idCortoTicket, idSucursal);
	}
	
	
	@Transactional(readOnly = true)
	public List<Venta> findByIdSucursaltoListTipo(Long idSucursal) {
		return ventaRepository.findByIdSucursaltoListTipo(idSucursal);
	}
	
	@Transactional(readOnly = true)
	public Venta findDevolucionByIdCortoTicketAndSucursal(String idCortoTicket, Long idSucursal) {
		return ventaRepository.findDevolucionByIdCortoTicketAndSucursal(idCortoTicket, idSucursal);
	}
	
	@Transactional(readOnly = true)
	public List<Venta> findDevolucionByIdCortoTicketAndSucursalVenta(String idCortoTicket, Long idSucursal) {
		return ventaRepository.findDevolucionByIdCortoTicketAndSucursalVenta(idCortoTicket, idSucursal);
	}
	
	@Transactional(readOnly = true)
	public List<Venta> findVentasAJAXByIdSucursal(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente, Long idMarca, Long idEmpresa, String anuladas){
		
		if(idAgente == 0) {
			return ventaRepository.findVentasAJAXByIdSucursalAndFechas(idSucursal, fechaDesde, fechaHasta, idMarca, idEmpresa, anuladas);
		}else {
			return ventaRepository.findVentasAJAXByIdSucursalAndFechasAndAgente(idSucursal, fechaDesde, fechaHasta, idAgente, idMarca, idEmpresa, anuladas);
		}
		
	}
	
	@Transactional(readOnly = true)
	public Double findTotalNetoVentasByIdSucursal(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente){
		
		if(idAgente == 0) {
			return ventaRepository.findTotalNetoVentasByIdSucursalAndFechas(idSucursal, fechaDesde, fechaHasta);
		}else {
			return ventaRepository.findTotalNetoVentasByIdSucursalAndFechasAndAgente(idSucursal, fechaDesde, fechaHasta, idAgente);
		}
		
	}
	
	@Transactional(readOnly = true)
	public Double findTotalIvaVentasByIdSucursal(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente){
		
		if(idAgente == 0) {
			return ventaRepository.findTotalIvaVentasByIdSucursalAndFechas(idSucursal, fechaDesde, fechaHasta);
		}else {
			return ventaRepository.findTotalIvaVentasByIdSucursalAndFechasAndAgente(idSucursal, fechaDesde, fechaHasta, idAgente);
		}
	}
	
	@Transactional(readOnly = true)
	public Double findTotalDescuentoVentasByIdSucursal(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente, Long idMarca, Long idEmpresa){
		
		if(idAgente == 0) {
			return ventaRepository.findTotalDescuentoVentasByIdSucursalAndFechas(idSucursal, fechaDesde, fechaHasta, idMarca, idEmpresa);
		}else {
			return ventaRepository.findTotalDescuentoVentasByIdSucursalAndFechasAndAgente(idSucursal, fechaDesde, fechaHasta, idAgente, idMarca, idEmpresa);
		}
	}
	
	
}
