package com.saraworld.tpvonline.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Marca;
import com.saraworld.tpvonline.model.Familia;
import com.saraworld.tpvonline.repository.FamiliaRepository;

@Service("familiaService")
public class FamiliaService {
	private FamiliaRepository familiaRepository;

	@Autowired
	public FamiliaService(FamiliaRepository familiaRepository) {
		this.familiaRepository = familiaRepository;
	}
	
	@Transactional
	public void saveFamilia(Familia familia) {
		familiaRepository.save(familia);
	}
	
	
	@Transactional(readOnly = true)
	public Familia findOne(Long id) {
		// TODO Auto-generated method stub
		return familiaRepository.findOne(id);
	}

	@Transactional
	public void delete(Long id) {
		familiaRepository.delete(id);
		
	}
	
	@Transactional(readOnly = true)
	public List<Familia> findAll(){
		return (List<Familia>)familiaRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public List<Familia> findListFamiliasByIdOfMarca(Long id){
		return (List<Familia>)familiaRepository.findListFamiliasByIdOfMarca(id);
	}
	
	@Transactional(readOnly = true)
	public List<Marca> findListMarcasByIdOfFamilia(Long id){
		return (List<Marca>)familiaRepository.findListMarcasByIdOfFamilia(id);
	}
	
	@Transactional(readOnly = true)
	public List<Familia> findListFamiliasByIdOfMarcaAndIdOrNombreOfFamilia(Long idMarca, String criterio){
		return (List<Familia>)familiaRepository.findListFamiliasByIdOfMarcaAndIdOrNombreOfFamilia(idMarca, criterio);
	}
}
