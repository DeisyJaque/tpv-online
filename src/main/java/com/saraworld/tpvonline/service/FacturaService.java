package com.saraworld.tpvonline.service;



import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Factura;
import com.saraworld.tpvonline.repository.FacturaRepository;

@Service("facturaService")
public class FacturaService {
	private FacturaRepository facturaRepository;

	@Autowired
	public FacturaService(FacturaRepository FacturaRepository) {
		this.facturaRepository = FacturaRepository;
	}
	
	@Transactional
	public void SaveFactura(Factura Factura) {
		facturaRepository.save(Factura);
	}
	
	
	@Transactional(readOnly = true)
	public List<Factura> findAll(){
		return (List<Factura>)facturaRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public List<Factura> FacturaByLotes(Long idSucursal, Long idEmpresa, Long idMarca, Long Lote){
		return (List<Factura>)facturaRepository.FacturaByLotes(idSucursal, idEmpresa, idMarca,Lote);
	}
	
	@Transactional(readOnly = true)
	public Factura findOne(Long id) {
		// TODO Auto-generated method stub
		return facturaRepository.findOne(id);
	}
	
	
	
}

