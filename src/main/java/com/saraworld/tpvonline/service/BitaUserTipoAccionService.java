package com.saraworld.tpvonline.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.saraworld.tpvonline.model.BitaUserTipoAccion;
import com.saraworld.tpvonline.repository.BitaUserTipoAccionRepository;

@Service("bitausertipoaccionService")
public class BitaUserTipoAccionService {
	private BitaUserTipoAccionRepository bitausertipoaccionRepository;

	@Autowired
	public BitaUserTipoAccionService(BitaUserTipoAccionRepository bitausertipoaccionRepository) {
		this.bitausertipoaccionRepository = bitausertipoaccionRepository;
	}
	
	@Transactional
	public void saveBitaAccion(BitaUserTipoAccion bitausertipoaccion) {
		bitausertipoaccionRepository.save(bitausertipoaccion);
	}
	
	@Transactional(readOnly = true)
	public BitaUserTipoAccion findOne(Long id) {
		// TODO Auto-generated method stub
		return bitausertipoaccionRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public List<BitaUserTipoAccion> findAll(){
		return (List<BitaUserTipoAccion>)bitausertipoaccionRepository.findAll();
	}

	
	@Transactional(readOnly = true)
	public boolean verifyUserInTipoAccion(Long id) {
		List<BitaUserTipoAccion> bitauser = bitausertipoaccionRepository.verifyUserInTipoAccion(id);
		if(bitauser.size() > 0) {
			return true;
		}else{
			return false;
		}
	}
	
	@Transactional(readOnly = true)
	public Long IdUserBitaAccion(Long id) {
		return bitausertipoaccionRepository.IdUserBitaAccion(id);
	}
	
	@Transactional(readOnly = true)
	public BitaUserTipoAccion findUserById(Long id) {
		BitaUserTipoAccion bitauser = bitausertipoaccionRepository.verifyUserById(id);
		return (BitaUserTipoAccion) bitauser;
	}
}
