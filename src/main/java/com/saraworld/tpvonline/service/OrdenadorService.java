package com.saraworld.tpvonline.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saraworld.tpvonline.model.Ordenador;
import com.saraworld.tpvonline.repository.OrdenadorRepository;

@Service("ordenadorService")
public class OrdenadorService {
	private OrdenadorRepository ordenadorRepository;

	@Autowired
	public OrdenadorService(OrdenadorRepository ordenadorRepository) {
		this.ordenadorRepository = ordenadorRepository;
	}
	
	@Transactional
	public void saveOrdenador(Ordenador Ordenador) {
		ordenadorRepository.save(Ordenador);
	}
	
	
	@Transactional(readOnly = true)
	public List<Ordenador> findAll(){
		return (List<Ordenador>)ordenadorRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Ordenador findOne(Long id) {
		// TODO Auto-generated method stub
		return ordenadorRepository.findOne(id);
	}
	
	@Transactional(readOnly = true)
	public Ordenador findOrdenadorByMac(String idMac) {
	
		
		return ordenadorRepository.findOrdenadorByMac(idMac);
	}
	
}
