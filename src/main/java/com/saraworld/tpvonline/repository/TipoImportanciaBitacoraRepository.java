package com.saraworld.tpvonline.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.TipoImportanciaBitacora;

@Repository("tipoimportanciabitacoraRepository")
public interface TipoImportanciaBitacoraRepository extends CrudRepository<TipoImportanciaBitacora, Long> {

	TipoImportanciaBitacora findById(Long id);

}
