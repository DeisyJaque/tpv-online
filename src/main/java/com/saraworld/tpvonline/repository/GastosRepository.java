package com.saraworld.tpvonline.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Gastos;


@Repository("gastosRepository")
public interface GastosRepository extends CrudRepository<Gastos, Long> {
	@Query("select g from Gastos g join fetch g.sucursal s join fetch g.empresa e join fetch g.marca m  where s.id=?1 and e.id=?2 and m.id=?3 order by g.fecha desc")
	List<Gastos> GastosBySucursal(Long idSucursal,Long idEmpresa,Long idMarca);
	
	@Query("Select g from Gastos g join fetch g.sucursal s join fetch g.empresa e join fetch g.marca m  where s.id=?1 and e.id=?2 and m.id=?3 and g.fecha>=?4 and g.fecha <=?5 order by g.fecha desc")
	public List<Gastos> findByFechaAndSucursal(Long idSucursal,Long idEmpresa,Long idMarca,Date fechaDesde, Date fechaHasta, Pageable pageable);
	
	@Query("Select g from Gastos g join fetch g.sucursal s join fetch g.empresa e join fetch g.marca m where g.tipo_gasto like ?1% and s.id=?2 and e.id=?3 and m.id=?4 order by g.fecha desc")
	public List<Gastos> findByTipoGastoAndSucursal(String busqueda, Long idSucursal,Long idEmpresa,Long idMarca);
	
	@Query("Select g from Gastos g join fetch g.sucursal s join fetch g.empresa e join fetch g.marca m where g.tipo_gasto like %?1% and s.id=?2 and e.id=?3 and m.id=?4 order by g.fecha desc")
	public List<Gastos> findByTipoGastoAndSucursalAnteriores(String busqueda, Long idSucursal,Long idEmpresa,Long idMarca, Pageable pageable);
	
	@Query("Select g from Gastos g join fetch g.sucursal s join fetch g.empresa e join fetch g.marca m  where g.proveedor like ?1% and s.id=?2 and e.id=?3 and m.id=?4  order by g.fecha desc")
	public List<Gastos> findByProveedorAndSucursal(String busqueda, Long idSucursal,Long idEmpresa,Long idMarca);
	
	@Query("Select g from Gastos g join fetch g.sucursal s join fetch g.empresa e join fetch g.marca m  where g.proveedor like %?1% and s.id=?2 and e.id=?3 and m.id=?4 order by g.fecha desc")
	public List<Gastos> findByProveedorAndSucursalAnteriores(String busqueda, Long idSucursal,Long idEmpresa,Long idMarca, Pageable pageable);
	
}
