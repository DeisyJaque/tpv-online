package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Informes;


@Repository("informesRepository")
public interface InformesRepository extends CrudRepository<Informes, Long> {

	@Query("select i from Informes i join fetch i.sucursales s where s.id=?1 and i.grupo_id.id=?2 and i.borrado is null and s.borrado is null order by i.id asc ")
	List<Informes> findListInformesByIdOfSucursalAndGrupo(Long idSucursal, Long grupo_id);

	@Query("select i from Informes i where i.grupo_id.id=?1 and i.borrado is null order by i.id asc ")
	List<Informes> findListInformesByIdOfGrupo(Long grupo_id);

}

