package com.saraworld.tpvonline.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Factura;


@Repository("facturaRepository")
public interface FacturaRepository extends CrudRepository<Factura, Long> {

	@Query("select f from Factura f join fetch f.sucursal s join fetch f.empresa e join fetch f.marca m  where s.id=?1 and e.id=?3 and m.id=?3 ")
	List<Factura> FacturaBySucursal(Long idSucursal, Long idEmpresa, Long idMarca);
	
	@Query("select f from Factura f join fetch f.sucursal s join fetch f.empresa e join fetch f.marca m  where s.id=?1 and e.id=?3 and m.id=?3 and f.lotes=?4 ")
	List<Factura> FacturaByLotes(Long idSucursal, Long idEmpresa, Long idMarca, Long Lote);
	

}

