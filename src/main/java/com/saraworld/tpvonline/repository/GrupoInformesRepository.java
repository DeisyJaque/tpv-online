package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.saraworld.tpvonline.model.GrupoInformes;
import com.saraworld.tpvonline.model.Informes;


@Repository("grupoinformesRepository")
public interface GrupoInformesRepository extends CrudRepository<GrupoInformes, Long> {

	@Query("select g from GrupoInformes g join fetch g.sucursales s where s.id=?1 and g.borrado is null and s.borrado is null ")
	List<GrupoInformes> findListGrupoInformesByIdOfSucursal(Long id);

	@Query("select g from GrupoInformes g join fetch g.sucursales s where s.id=0 and g.borrado is null and s.borrado is null ")
	List<GrupoInformes> findListGrupoInformesByTodasLasSucursales();

}

