package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Ordenador;


@Repository("ordenadorRepository")
public interface OrdenadorRepository extends CrudRepository<Ordenador, Long> {

	@Query("select ord from Ordenador ord where ord.mac=?1")
	Ordenador findOrdenadorByMac(String idMac);

}
