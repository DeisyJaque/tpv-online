package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Marca;
import com.saraworld.tpvonline.model.Familia;

@Repository("familiaRepository")
public interface FamiliaRepository extends CrudRepository<Familia, Long> {

	@Query("select f from Familia f join fetch f.marcas m where m.id=?1 and f.predefinido=false and f.borrado is null and m.borrado is null")
	List<Familia> findListFamiliasByIdOfMarca(Long id);
	
	@Query("select m from Marca m join fetch m.familias f where f.id=?1 and f.borrado is null and m.borrado is null")
	List<Marca> findListMarcasByIdOfFamilia(Long id);
	
	@Query("select f.id, f.nombre from Familia f join f.marcas m where m.id=?1 and f.predefinido=false and f.borrado is null and m.borrado is null and (CAST(f.id as string) like %?2% or f.nombre like %?2%)")
	List<Familia> findListFamiliasByIdOfMarcaAndIdOrNombreOfFamilia(Long idMarca, String criterio);
}
