package com.saraworld.tpvonline.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Factura;
import com.saraworld.tpvonline.model.Lotes;


@Repository("loteRepository")
public interface LotesRepository extends CrudRepository<Lotes, Long> {

	@Query("select l from Lotes l join fetch l.sucursal s  where s.id=?1 order by l.fecha_creacion desc")
	List<Lotes> LotesBySucursal(Long idSucursal);
	
	
}
