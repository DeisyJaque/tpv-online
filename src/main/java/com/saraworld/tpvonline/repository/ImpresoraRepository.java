package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Impresora;


@Repository("impresoraRepository")
public interface ImpresoraRepository extends CrudRepository<Impresora, Long> {

	

}
