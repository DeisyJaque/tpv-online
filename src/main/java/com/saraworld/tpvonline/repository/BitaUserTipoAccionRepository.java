package com.saraworld.tpvonline.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.BitaUserTipoAccion;



@Repository("bitausertipoaccionRepository")
public interface BitaUserTipoAccionRepository extends CrudRepository<BitaUserTipoAccion, Long> {

	@Query("select bita from BitaUserTipoAccion bita where bita.usuario.id=?1")
	List<BitaUserTipoAccion> verifyUserInTipoAccion(Long id);
	
	@Query("select bita.id from BitaUserTipoAccion bita where bita.usuario.id=?1")
	Long IdUserBitaAccion(Long id);
	
	@Query("select bita from BitaUserTipoAccion bita where bita.usuario.id=?1")
	BitaUserTipoAccion verifyUserById(Long id);
	

}


