package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.saraworld.tpvonline.model.GrupoAyuda;

@Repository("grupoayudaRepository")
public interface GrupoAyudaRepository extends CrudRepository<GrupoAyuda, Long> {


	@Query("select a from GrupoAyuda a where a.borrado is null order by a.nombre asc ")
	List<GrupoAyuda> ListGrupoByOrder();
	
	@Query("select a from GrupoAyuda a where a.nombre like %?1%  and a.borrado is null order by a.nombre asc ")
	List<GrupoAyuda> ListGrupoByNombre(String busqueda,Pageable pageable);
	
	
	

}
