package com.saraworld.tpvonline.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Venta;

@Repository("ventaRepository")
public interface VentaRepository extends CrudRepository<Venta, Long> {
	@Query("select v from Venta v join fetch v.cliente c join fetch v.sucursal s where v.tipo='T' and c.id=?1 and s.id=?2")
	List<Venta> findVentaByClienteAndSucursal(Long idClient,Long idSucursal);
	
	@Query("select v from Venta v where v.tipo='T' and v.fecha=?1 and v.sucursal.id=?2")
	List<Venta> verifyIfExistsVentaInTheDayByIdOfSucursal(Date date, Long idSucursal);
	
	@Query("select v from Venta v where v.tipo='D' and v.estado='Devolucion' and v.fecha=?1 and v.sucursal.id=?2")
	List<Venta> verifyIfExistsDevolucionInTheDayByIdOfSucursal(Date date, Long idSucursal);
	
	@Query("select v from Venta v where v.tipo='D' and v.estado='Devolucion' and v.id_largo_relacion=?1 and v.sucursal.id=?2")
	Venta verifyIfVentaTieneDevolucionByIdLargoTicketIdOfSucursal(String idLargoTicketVenta, Long idSucursal);
	
	@Query("select v from Venta v where v.tipo='T' and v.fecha=?1 and v.sucursal.id=?2 order by v.fecha, v.hora desc")
	List<Venta> findLastVentaByIdOfSucursal(Date date,Long idSucursal);
	
	@Query("select v from Venta v where v.tipo='D' and v.estado='Devolucion' and v.fecha=?1 and v.sucursal.id=?2 order by v.fecha, v.hora desc")
	List<Venta> findLastDevolucionByIdOfSucursal(Date date,Long idSucursal);
	
	//@Query("select v from Venta v where v.fecha=?1 and v.sucursal.id=?2 and (v.total - v.pagado=0 or v.total - v.pagado!=0) and v.estado='En Espera' or v.estado='Activa' order by v.fecha, v.hora desc")
	//@Query("select v from Venta v where v.fecha=?1 and v.sucursal.id=?2 and ((v.total - v.pagado=0 and size(v.servicios) >= 0) or v.total - v.pagado!=0 ) order by v.hora desc")
	//@Query("select v from Venta v where v.fecha=?1 and v.tipo='T' and v.sucursal.id=?2 and ((v.total - v.pagado=0 and size(v.servicios) >= 0 and size(v.pagos) = 0) or v.total - v.pagado != 0 ) order by v.hora desc")
	@Query("select v from Venta v where v.fecha=?1 and v.tipo='T' and v.sucursal.id=?2  and ((v.total - v.pagado=0 and (select count(*) from Servicio s where s.venta.id = v.id and s.tipo_transaccion = 'Venta' and s.borrado = null ) >= 0 and (select count(*) from Pago p where p.venta.id = v.id and p.estado = 'Procesado' and p.borrado=null ) = 0) or v.total - v.pagado != 0 ) "
			+ " order by v.hora desc")
	List<Venta> findLastPendientesByDateAndSucursal(Date date,Long idSucursal);
	
	//@Query("select v from Venta v where v.fecha<?1 and v.sucursal.id=?2 and v.total - v.pagado!=0 and v.estado='En Espera' order by v.fecha, v.hora desc")
	@Query("select v from Venta v where v.fecha<?1 and v.tipo='T' and v.sucursal.id=?2 and v.total - v.pagado != 0 order by v.fecha desc, v.hora desc")
	List<Venta> findLastAnterioresByDateAndSucursal(Date date,Long idSucursal);
	
	@Query("select v.id_corto_ticket from Venta v where v.sucursal.id=?2 and v.tipo='T' and v.id_corto_ticket like %?1% order by v.fecha, v.hora desc")
	List<Venta> findByIdTicketAndSucursal(String busqueda, Long idSucursal);
	
	@Query("select v from Venta v join fetch v.sucursal s where v.tipo='T' and v.id_corto_ticket=?1 and s.id=?2")
	Venta findByIdCortoTicketAndSucursal(String idCortoTicket, Long idSucursal);
	
	@Query("select v from Venta v join fetch v.sucursal s where v.tipo='D' and v.estado='Devolucion' and v.id_corto_relacion=?1 and s.id=?2")
	Venta findDevolucionByIdCortoTicketAndSucursal(String idCortoTicket, Long idSucursal);
	
	@Query("select v from Venta v join fetch v.sucursal s where   v.id_corto_relacion=?1 and s.id=?2")
	List<Venta> findDevolucionByIdCortoTicketAndSucursalVenta(String idCortoTicket, Long idSucursal);
	
	@Query("select v from Venta v join fetch v.sucursal s  where v.tipo='T' and CONVERT (date, SYSDATETIME())=v.fecha and size(v.servicios) = 0 and s.id=?1 order by v.fecha, v.hora asc")
	List<Venta> findByIdSucursalAndAntiguaHoy(Long idSucursal);
	
	@Query("select v from Venta v join fetch v.sucursal s  where  size(v.servicios) = 0 and v.total=0.00 and v.saldo=0.00 and s.id=?1 ")
	List<Venta> findByIdSucursaltoListTipo(Long idSucursal);
	
	
	//@Query("select v from Venta v where v.tipo='T' and v.sucursal.id=?1")
	//List<Venta> findVentaByIdSucursal(Long idSucursal);
	
	@Query(nativeQuery = true, value ="SELECT * FROM Venta v WHERE v.tipo='T' AND v.sucursal_id=?1 AND v.fecha>=?2 AND v.fecha<=?3 AND v.marca_id=?4 AND v.empresa_id=?5 AND v.anular=?6")
	List<Venta> findVentasAJAXByIdSucursalAndFechas(Long idSucursal, String fechaDesde, String fechaHasta, Long idMarca, Long idEmpresa, String anuladas);
	
	@Query(nativeQuery = true, value ="SELECT * FROM Venta v WHERE v.tipo='T' AND v.sucursal_id=?1 AND v.fecha>=?2 AND v.fecha<=?3 AND v.usuario_id=?4 AND v.marca_id=?5 AND v.empresa_id=?6 AND v.anular=?7")
	List<Venta> findVentasAJAXByIdSucursalAndFechasAndAgente(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente, Long idMarca, Long idEmpresa, String anuladas);
	
	@Query(nativeQuery = true, value ="SELECT SUM (v.total_neto) FROM Venta v WHERE v.sucursal_id=?1 AND v.tipo='T' AND v.saldo=0.00 AND v.fecha>=?2 AND v.fecha<=?3")
	Double findTotalNetoVentasByIdSucursalAndFechas(Long idSucursal, String fechaDesde, String fechaHasta);
	
	@Query(nativeQuery = true, value ="SELECT SUM (v.total_neto) FROM Venta v WHERE v.sucursal_id=?1 AND v.tipo='T' AND v.saldo=0.00 AND v.fecha>=?2 AND v.fecha<=?3 AND v.usuario_id=?4")
	Double findTotalNetoVentasByIdSucursalAndFechasAndAgente(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente);
	
	@Query(nativeQuery = true, value ="SELECT SUM (v.total_iva) FROM Venta v WHERE v.sucursal_id=?1 AND v.tipo='T' AND v.saldo=0.00 AND v.fecha>=?2 AND v.fecha<=?3")
	Double findTotalIvaVentasByIdSucursalAndFechas(Long idSucursal, String fechaDesde, String fechaHasta);
	
	@Query(nativeQuery = true, value ="SELECT SUM (v.total_iva) FROM Venta v WHERE v.sucursal_id=?1 AND v.tipo='T' AND v.saldo=0.00 AND v.fecha>=?2 AND v.fecha<=?3 AND v.usuario_id=?4")
	Double findTotalIvaVentasByIdSucursalAndFechasAndAgente(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente);
	
	@Query(nativeQuery = true, value ="SELECT SUM (v.total_descuento) FROM Venta v WHERE v.sucursal_id=?1 AND v.tipo='T' AND v.saldo=0.00 AND v.fecha>=?2 AND v.fecha<=?3 AND v.marca_id=?4 AND v.empresa_id=?5")
	Double findTotalDescuentoVentasByIdSucursalAndFechas(Long idSucursal, String fechaDesde, String fechaHasta, Long idMarca, Long idEmpresa);
	
	@Query(nativeQuery = true, value ="SELECT SUM (v.total_descuento) FROM Venta v WHERE v.sucursal_id=?1 AND v.tipo='T' AND v.saldo=0.00 AND v.fecha>=?2 AND v.fecha<=?3 AND v.usuario_id=?4 AND v.marca_id=?5 AND v.empresa_id=?6")
	Double findTotalDescuentoVentasByIdSucursalAndFechasAndAgente(Long idSucursal, String fechaDesde, String fechaHasta, Long idAgente, Long idMarca, Long idEmpresa);


}
