package com.saraworld.tpvonline.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.saraworld.tpvonline.model.TipoAccion;

@Repository("tipoaccionRepository")
public interface TipoAccionRepository extends CrudRepository<TipoAccion, Long> {

	TipoAccion findById(Long id);
	
	

}
