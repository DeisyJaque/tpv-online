package com.saraworld.tpvonline.repository;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Empresa;
import com.saraworld.tpvonline.model.FormaPago;
import com.saraworld.tpvonline.model.Sucursal;
import java.util.List;

@Repository("formaPagoRepository")
public interface FormaPagoRepository extends CrudRepository<FormaPago, Long> {
	@Query("Select f from FormaPago f where f.nombre like %?1% and f.borrado is null")
	public FormaPago findByNombre(String nombre);
	
	@Query("Select f from FormaPago f where f.nombre like %?1% and f.borrado is null")
	public List<FormaPago> findByNombreList(String nombre);
	
	@Query("Select DISTINCT f from FormaPago f join fetch f.empresa emp join fetch emp.mar_emp_suc_usu_rol mesu join fetch mesu.usuario us join fetch mesu.empresa emps  where us.id=?1 and emp.id=emps.id")
	public List<FormaPago> findByUserAndEmpresa(Long id);
	
	@Query("Select DISTINCT f from FormaPago f join fetch f.empresa emp join fetch emp.mar_emp_suc_usu_rol mesu join fetch mesu.usuario us join fetch mesu.empresa emps  where  f.nombre =?1 and us.id=?2 and emp.id=emps.id")
	public FormaPago findByFormaAndUser(String idforma,Long iduser);
	
	@Query("Select f from FormaPago f join fetch f.sucursales s where s.id=?1 and f.borrado is null and s.borrado is null")
	public List<FormaPago> findFormasPagosByIdSucursal(Long id );
	
	@Query("select s from Sucursal s join fetch s.formapago f where f.id=?1 and s.borrado is null and f.borrado is null")
	List<Sucursal> findListSucursalesByIdOfFormaPago(Long id);
	
	@Query("select e from Empresa e join fetch e.formapago f where f.id=?1 and e.borrado is null and f.borrado is null")
	List<Empresa> findListEmpresasByIdOfFormaPago(Long id);
	
	@Query("select f.id, f.nombre from FormaPago f join f.sucursales s where s.id=?1 and (CAST(f.id as string) like %?2% or f.nombre like %?2%) and f.borrado is null and s.borrado is null")
	List<Object[]> findListFormasDePagoByIdOfSucursalAndIdOrNombreOfForma(Long id, String nombre);
	
	@Query("select f from FormaPago f join fetch f.empresa e where e.id=?1  and f.borrado is null and e.borrado is null ORDER BY f.nombre")
	List<FormaPago> findListFormasPagoByIdOfEmpresa(Long id);
	
}
