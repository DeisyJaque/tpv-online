package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Ayuda;

@Repository("ayudaRepository")
public interface AyudaRepository extends CrudRepository<Ayuda, Long> {

	@Query("select a from Ayuda a where a.grupo_ayuda_id.id=?1 and a.borrado is null order by a.id desc ")
	List<Ayuda> findListAyudaByIdOfGrupo(Long grupo_id);

	@Query("select a from Ayuda a where a.borrado is null order by a.id asc ")
	List<Ayuda> findListAyudaByOrden();
	
	@Query("select a from Ayuda a where a.nombre like %?1% and a.borrado is null order by a.id asc ")
	List<Ayuda> findListAyudaByOrdenPorNombre(String busqueda,Pageable pageable);
	


}
