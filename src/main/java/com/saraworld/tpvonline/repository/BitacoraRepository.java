package com.saraworld.tpvonline.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saraworld.tpvonline.model.Bitacora;


@Repository("bitacoraRepository")
public interface BitacoraRepository extends CrudRepository<Bitacora, Long> {

	@Query("select bita from Bitacora bita where bita.tipo_accion.id=?1")
	List<Bitacora> findListBitacoraByIdBitacoraInicio(Long idBitacora);
	
	@Query("select bita.usuario.id from Bitacora bita where bita.id=?1")
	Long findBitacoraByIdUsuario(Long idBitaUser);
	
	@Query("select bita from Bitacora bita join fetch bita.usuario user where user.id=?1")
	List<Bitacora> findListBitacoraByUserId(Long id);
}
