package com.saraworld.tpvonline.model;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "bitacora")
public class Bitacora {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@ManyToOne( fetch=FetchType.LAZY)
	private Usuario usuario;
	
	@ManyToOne( fetch=FetchType.LAZY)
	private TipoAccion tipo_accion;
	
	@ManyToOne( fetch=FetchType.LAZY)
	private TipoImportanciaBitacora tipo_importancia_bitacora;
	
	
	@Column(name = "fecha_hora")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_hora;
	
	@Column(name = "descripcion", length = 256)
	@Size(min=10)
	private String descripcion;
	
	/*El tamaño mínimo de una IP en IPV4 es 7.
	 * Y el máximo sería 15.
	 * Si se van a incluir los puertos puede cambiar el valor.
	 * También puede cambiar el valor si se van a almacenar direcciones en IPV6
	 * IPv4 : XXX.XXX.XXX.XXX
	 * */
	
	@Column(name = "ip_origen", length = 15)
	@Size(min=7)
	private String ip_origen;
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@Column(name = "borradolog")
	private Boolean borradolog;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoAccion getTipo_accion() {
		return tipo_accion;
	}

	public void setTipo_accion(TipoAccion tipo_accion) {
		this.tipo_accion = tipo_accion;
	}

	public TipoImportanciaBitacora getTipo_importancia_bitacora() {
		return tipo_importancia_bitacora;
	}

	public void setTipo_importancia_bitacora(TipoImportanciaBitacora tipo_importancia_bitacora) {
		this.tipo_importancia_bitacora = tipo_importancia_bitacora;
	}

	public Date getFecha_hora() {
		return fecha_hora;
	}

	public void setFecha_hora(Date fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIp_origen() {
		return ip_origen;
	}

	public void setIp_origen(String ip_origen) {
		this.ip_origen = ip_origen;
	}

	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Boolean getBorradolog() {
		return borradolog;
	}

	public void setBorradolog(Boolean borradolog) {
		this.borradolog = borradolog;
	}
}
