package com.saraworld.tpvonline.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "informes")
public class Informes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "sucursales_excluidos", length = 200)
	private String sucursales_excluidos;
	
	@Column(name = "sucursales_incluidos")
	private Integer sucursales_incluidos;
	
	@Column(name = "nivel")
	private Integer nivel;
	
	@Column(name = "nombre", length = 50)
	private String nombre;
	
	
	@Column(name = "query_tmp", length = 500)
	private String query_tmp;
	
	@Column(name = "tipo", length = 50)
	private String tipo;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "grupo_id")
	private GrupoInformes grupo_id;
	
	@ManyToMany(cascade = CascadeType.ALL)
  	@JoinTable(name = "informes_sucursal", 
	joinColumns = { @JoinColumn(name = "id", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "sucursal_id") })
	private Set<Sucursal> sucursales = new HashSet<>();
	
	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuery() {
		return query_tmp;
	}

	public void setQuery(String query_tmp) {
		this.query_tmp = query_tmp;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Integer getSucursalesSi() {
		return sucursales_incluidos;
	}

	public void setSucursalesSi(Integer sucursales_incluidos) {
		this.sucursales_incluidos = sucursales_incluidos;
	}
	
	public String getSucursalesNo() {
		return sucursales_excluidos;
	}

	public void setSucursalesNo(String sucursales_excluidos) {
		this.sucursales_excluidos = sucursales_excluidos;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void settipo(String tipo) {
		this.tipo = tipo;
	}

	
	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}
	
	
	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}
	
	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}
	
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public GrupoInformes getGrupoInformes() {
		return grupo_id;
	}

	public void setGrupoInformes(GrupoInformes grupo_id) {
		this.grupo_id = grupo_id;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
}
