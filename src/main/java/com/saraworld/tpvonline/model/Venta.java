package com.saraworld.tpvonline.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "venta")
public class Venta {

	@Id
	//@GeneratedValue(generator = "uuid")
	//@GenericGenerator(name = "uuid", strategy = "uuid2")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_corto_ticket", nullable = false )
	private String id_corto_ticket;
	
	@Column(name = "id_largo_ticket", nullable = false, unique = true)
	private String id_largo_ticket;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "marca_id")
	private Marca marca;
	
	@Column(name = "anular")
	private boolean anular;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "factura_id")
	private Factura factura;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "lote_id")
	private Lotes lotes;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "sucursal_id")
	private Sucursal sucursal;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "cliente_id")
	private Cliente cliente;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "usuario_id", nullable = false)
	private Usuario empleado;
	
	@Column(name = "tipo", length = 5, nullable = false)
	private String tipo;
	
	@Column(name = "numero_venta", nullable = false)
	private Integer numero_venta;
	
	@Column(name = "total_neto", columnDefinition="Decimal(8,2)")
    private Double total_neto;
	
	@Column(name = "total_iva", columnDefinition="Decimal(8,2)")
    private Double total_iva;
	
	@Column(name = "total_descuento", columnDefinition="Decimal(8,2)")
    private Double total_descuento;
	
	@Column(name = "total", columnDefinition="Decimal(8,2)")
    private Double total;
	
	@Column(name = "pagado", columnDefinition="Decimal(8,2)")
    private Double pagado;
	
	@Column(name = "saldo", columnDefinition="Decimal(8,2)")
    private Double saldo;
	
	@Column(name = "caja", nullable = false)
	private Integer caja;
	
	@Column(name = "agente", length = 50, nullable = false)
	private String agente;
	
	@Column(name = "fecha", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date fecha;
	
	@Column(name = "hora", nullable = false)
	@Basic
	@Temporal(TemporalType.TIME)
	private Date hora;
	
	@Column(name = "estado", length = 10, nullable = false)
	private String estado;
	
	@Column(name = "observaciones", length = 400)
	private String observaciones;
	
	@Column(name = "id_corto_relacion")
	private String id_corto_relacion;
	
	@Column(name = "id_largo_relacion")
	private String id_largo_relacion;
	
	@OneToMany(mappedBy = "venta", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Servicio> servicios = new ArrayList<>();
	
	@OneToMany(mappedBy = "venta", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Pago> pagos = new ArrayList<>();
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getId_corto_ticket() {
		return id_corto_ticket;
	}

	public void setId_corto_ticket(String id_corto_ticket) {
		this.id_corto_ticket = id_corto_ticket;
	}

	public String getId_largo_ticket() {
		return id_largo_ticket;
	}

	public void setId_largo_ticket(String id_largo_ticket) {
		this.id_largo_ticket = id_largo_ticket;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Factura getFactura() {
		return factura;
	}
	
	public void setFactura(Factura factura) {
		this.factura = factura;
	}
	
	
	public Lotes getLote() {
		return lotes;
	}
	
	public void setLote(Lotes lotes) {
		this.lotes = lotes;
	}

	
	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Usuario getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Usuario empleado) {
		this.empleado = empleado;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getNumero_venta() {
		return numero_venta;
	}

	public void setNumero_venta(Integer numero_venta) {
		this.numero_venta = numero_venta;
	}

	public Double getTotal_neto() {
		return total_neto;
	}

	public void setTotal_neto(Double total_neto) {
		this.total_neto = total_neto;
	}

	public Double getTotal_iva() {
		return total_iva;
	}

	public void setTotal_iva(Double total_iva) {
		this.total_iva = total_iva;
	}

	public Double getTotal_descuento() {
		return total_descuento;
	}

	public void setTotal_descuento(Double total_descuento) {
		this.total_descuento = total_descuento;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getPagado() {
		return pagado;
	}

	public void setPagado(Double pagado) {
		this.pagado = pagado;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Integer getCaja() {
		return caja;
	}

	public void setCaja(Integer caja) {
		this.caja = caja;
	}

	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getId_corto_relacion() {
		return id_corto_relacion;
	}

	public void setId_corto_relacion(String id_corto_relacion) {
		this.id_corto_relacion = id_corto_relacion;
	}

	public String getId_largo_relacion() {
		return id_largo_relacion;
	}

	public void setId_largo_relacion(String id_largo_relacion) {
		this.id_largo_relacion = id_largo_relacion;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public List<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pago> pagos) {
		this.pagos = pagos;
	}

	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}
	
	public boolean getAnular() {
		return anular;
	}

	public void setAnular(boolean anular) {
		this.anular = anular;
	}
	
}
