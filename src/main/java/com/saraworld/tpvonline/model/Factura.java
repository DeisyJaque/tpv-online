package com.saraworld.tpvonline.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import javax.persistence.OneToMany;

import javax.persistence.PrePersist;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "factura")
public class Factura {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "tipo")
	private String tipo;
	
	
	@Column(name = "nro_factura")
	private String nro_factura;
	
	@Column(name = "fecha", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date fecha;
	
	@Column(name = "hora", nullable = false)
	@Basic
	@Temporal(TemporalType.TIME)
	private Date hora;
	
	@Column(name = "concepto")
	private String concepto;
	
	@Column(name = "id_ticket", nullable = false )
	private String id_ticket;
	
	@ManyToMany( mappedBy = "factura")
	private Set<Lotes> lote = new HashSet<>();
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "id_cliente")
	private Cliente cliente;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "venta_id")
	private Venta ventas;
	
	@Column(name = "importe", columnDefinition="Decimal(8,2)")
    private Double importe;
	
	@Column(name = "iva", columnDefinition="Decimal(8,2)")
    private Double iva;
	
	@Column(name = "pvp", columnDefinition="Decimal(8,2)")
    private Double pvp;
	
	@Column(name = "estado")
	private String estado;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "lote")
	private Lotes lotes;
	
	@Column(name = "presentado")
	private boolean presentado;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "usuario_id", nullable = false)
	private Usuario empleado;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "marca_id")
	private Marca marca;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "sucursal_id")
	private Sucursal sucursal;

	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;

	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNro_Factura() {
		return nro_factura;
	}

	public void setNro_Factura(String nro_factura) {
		this.nro_factura = nro_factura;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}
	
	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	public boolean getPresentado() {
		return presentado;
	}

	public void setPresentado(boolean presentado) {
		this.presentado = presentado;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getId_ticket() {
		return id_ticket;
	}

	public void setId_ticket(String id_ticket) {
		this.id_ticket = id_ticket;
	}
	
	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Venta getVenta() {
		return ventas;
	}

	public void setVenta(Venta ventas) {
		this.ventas = ventas;
	}


	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	

	public Usuario getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Usuario empleado) {
		this.empleado = empleado;
	}
	
	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}
	
	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}
	
	
	public Double getPVP() {
		return pvp;
	}

	public void setPVP(Double pvp) {
		this.pvp = pvp;
	}
	
	public Lotes getLote() {
		return lotes;
	}

	public void setLote(Lotes lotes) {
		this.lotes = lotes;
	}
	
	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}

	public Set<Lotes> getLotes() {
		return lote;
	}

	public void setLotes(Set<Lotes> lote) {
		this.lote = lote;
	}
}
