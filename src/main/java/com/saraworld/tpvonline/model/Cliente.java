package com.saraworld.tpvonline.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "cliente")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nro_cliente", length = 30)
	private String nro_cliente;
	
	@Column(name = "nombre", length = 50)
	@NotEmpty(message = "Por favor provee el nombre")
	@Size(max=50)
	private String nombre;
	
	@Column(name = "apellido", length = 50)
	@NotEmpty(message = "Por favor provee el apellido")
	@Size(max=50)
	private String apellido;
	
	@Column(name = "nro_socio", length = 200)
	@Size(max=200)
	private String nro_socio;
	
    @OneToOne( fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private TipoSexo tipo_sexo;
    
    @Column(name = "menor_edad")
	private boolean menor_edad;
    
    @OneToOne( fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private TipoNacionalidad tipo_nacionalidad;
	
	@OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "poblacion_id")
	private Poblacion poblacion;
	
	@OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "provincia_id")
	private Provincia provincia;
	
	@OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "pais_id")
	private Pais pais;
	
	@Column(name = "cpostal", length = 10)
	@Size(max=10)
	private String cpostal;
	
	@OneToOne( fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	private TipoDocumento tipo_documento;
	
	@Column(name = "documento", length = 20)
	@Size(max=20)
	private String documento;
	
	@Column(name = "fecha_nacimiento")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date fecha_nacimiento;
	
	@Column(name = "direccion", length = 256)
	@Size(max=256)
	private String direccion;
	
	@Column(name = "telefono", length = 15)
	@Size(max=15)
	private String telefono;
	
	@Column(name = "descuento")
	private Double descuento;
	
	@Column(name = "telefono2", length = 15)
	@Size(max=15)
	private String telefono2;
	
	@Column(name = "email", length = 100)
	@Email(message = "Por favor, introduzca un correo electrónico válido")
	@Size(max=100)
	private String email;
	
	@Column(name = "observaciones")
	private String observaciones;
	
	@Column(name = "actualizar_datos")
	private boolean actualizar_datos;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "sucursal_id")
	private Sucursal sucursal;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Venta> ventas = new ArrayList<>();
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Vale> vales = new ArrayList<>();
	
	@Column(name = "imagen")
	private String imagen;

	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNro_cliente() {
		return nro_cliente;
	}

	public void setNro_cliente(String nro_cliente) {
		this.nro_cliente = nro_cliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public String getNro_socio() {
		return nro_socio;
	}

	public void setNro_socio(String nro_socio) {
		this.nro_socio = nro_socio;
	}
		
	public TipoSexo getTipo_sexo() {
		return tipo_sexo;
	}

	public void setTipo_sexo(TipoSexo tipo_sexo) {
		this.tipo_sexo = tipo_sexo;
	}

	public boolean isMenor_edad() {
		return menor_edad;
	}

	public void setMenor_edad(boolean menor_edad) {
		this.menor_edad = menor_edad;
	}

	public TipoNacionalidad getTipo_nacionalidad() {
		return tipo_nacionalidad;
	}

	public void setTipo_nacionalidad(TipoNacionalidad tipo_nacionalidad) {
		this.tipo_nacionalidad = tipo_nacionalidad;
	}

	public TipoDocumento getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(TipoDocumento tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	
	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public boolean isActualizar_datos() {
		return actualizar_datos;
	}

	public void setActualizar_datos(boolean actualizar_datos) {
		this.actualizar_datos = actualizar_datos;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	public List<Venta> getVentas() {
		return ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}

	public List<Vale> getVales() {
		return vales;
	}

	public void setVales(List<Vale> vales) {
		this.vales = vales;
	}

	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}
	
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public Poblacion getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(Poblacion poblacion) {
		this.poblacion = poblacion;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public String getCpostal() {
		return cpostal;
	}

	public void setCpostal(String cpostal) {
		this.cpostal = cpostal;
	}


}
