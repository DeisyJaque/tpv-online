package com.saraworld.tpvonline.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "grupo_informes")
public class GrupoInformes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nombre", length = 50)
	private String nombre;
	
	@Column(name = "sucursales_excluidos", length = 200)
	private String sucursales_excluidos;
	
	@Column(name = "sucursales_incluidos")
	private Integer sucursales_incluidos;
	
	@Column(name = "nivel")
	private Integer nivel;
	
	@Column(name = "tipo", length = 50)
	private String tipo;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@OneToMany(mappedBy = "grupo_id",cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Informes> informes = new ArrayList<>();
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@ManyToMany(cascade = CascadeType.ALL)
  	@JoinTable(name = "grupo_informes_sucursal", 
	joinColumns = { @JoinColumn(name = "id", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "sucursal_id") })
	private Set<Sucursal> sucursales = new HashSet<>();

	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void settipo(String tipo) {
		this.tipo = tipo;
	}

	
	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}
	
	
	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}
	
	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}
	
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public List<Informes> getInformes() {
		return informes;
	}

	public void setInformes(List<Informes> informes) {
		this.informes = informes;
	}
	
	public Integer getSucursalesSi() {
		return sucursales_incluidos;
	}

	public void setSucursalesSi(Integer sucursales_incluidos) {
		this.sucursales_incluidos = sucursales_incluidos;
	}
	
	public String getSucursalesNo() {
		return sucursales_excluidos;
	}

	public void setSucursalesNo(String sucursales_excluidos) {
		this.sucursales_excluidos = sucursales_excluidos;
	}
	
	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
}
