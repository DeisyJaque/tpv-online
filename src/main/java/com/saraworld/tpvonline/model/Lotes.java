package com.saraworld.tpvonline.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "lotes")
public class Lotes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_lote")
	public String id_lote;
	
	@Column(name = "email_receptor")
	public String email_receptor;
	
	@Column(name = "total_facturado", columnDefinition="Decimal(8,2)")
    public Double total_facturado;
	
	@Column(name = "total_gastos", columnDefinition="Decimal(8,2)")
    public Double total_gastos;
	
	@Column(name = "iva_devegando", columnDefinition="Decimal(8,2)")
    public Double iva_devegando;
	
	@Column(name = "iva_deducible", columnDefinition="Decimal(8,2)")
    public Double iva_deducible;
	
	@Column(name = "presentado")
	private boolean presentado;
	
	@Column(name = "anular")
	public boolean anular;
	
	@Column(name = "fecha_envio")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_envio;
	
	@Column(name = "fecha_presentado")
	@Temporal(TemporalType.TIMESTAMP)
	public Date fecha_presentado;
	
	@Column(name = "fecha_anular")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_anular;
	
	@ManyToMany(cascade = CascadeType.ALL)
  	@JoinTable(name = "lotes_factura", 
	joinColumns = {@JoinColumn(name = "id_factura", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "id_lote") })
	private Set<Factura> factura = new HashSet<>();
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "usuario_id", nullable = false)
	private Usuario empleado;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "marca_id")
	private Marca marca;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "sucursal_id")
	private Sucursal sucursal;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getId_Lote() {
		return id_lote;
	}

	public void setId_Lote(String id_lote) {
		this.id_lote = id_lote;
	}
	
	public String getEmail_Receptor() {
		return email_receptor;
	}

	public void setEmail_Receptor(String email_receptor) {
		this.email_receptor = email_receptor;
	}
	
	
	public boolean getPresentado() {
		return presentado;
	}

	public void setPresentado(boolean presentado) {
		this.presentado = presentado;
	}
	
	public boolean getAnular() {
		return anular;
	}

	public void setAnular(boolean anular) {
		this.anular = anular;
	}
	
	
	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Usuario getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Usuario empleado) {
		this.empleado = empleado;
	}

	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}


	public Date getFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(Date fecha_envio) {
		this.fecha_envio = fecha_envio;
	}
	
	public Date Fecha_presentado() {
		return fecha_presentado;
	}

	public void setFecha_presentado(Date fecha_presentado) {
		this.fecha_presentado = fecha_presentado;
	}

	public Date getFecha_anular() {
		return fecha_anular;
	}

	public void setFecha_anular(Date fecha_anular) {
		this.fecha_anular = fecha_anular;
	}
	
	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}
	
	public Set<Factura> getFactura() {
		return factura;
	}

	public void setFactura(Set<Factura> factura) {
		this.factura = factura;
	}
	
	public Double getTotalFactura() {
		return total_facturado;
	}

	public void setTotalFactura(Double total_facturado) {
		this.total_facturado = total_facturado;
	}
	
	public Double getTotalGastos() {
		return total_gastos;
	}

	public void setTotalGastos(Double total_gastos) {
		this.total_gastos = total_gastos;
	}
	public Double getIvaDevegando() {
		return iva_devegando;
	}

	public void setIvaDevegando(Double iva_devegando) {
		this.iva_devegando = iva_devegando;
	}
	
	public Double getIvaDeducible() {
		return iva_deducible;
	}

	public void setIvaDeducible(Double iva_deducible) {
		this.iva_deducible = iva_deducible;
	}
}
