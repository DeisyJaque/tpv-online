package com.saraworld.tpvonline.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "impresora")
public class Impresora {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nombre", length = 50)
	private String nombre;
	
	@Column(name = "corte", length = 50)
	private String corte;
	
	@Column(name = "cancelado", length = 50)
	private boolean cancelado;
	
	@Column(name = "margen_izq", length = 50)
	private Float margen_izq;
	
	@Column(name = "inferior", length = 50)
	private Float inferior;

	@Column(name = "superior", length = 50)
	private Float superior;
	
	@Column(name = "predeterminado", length = 50)
	private boolean predeterminado;
	
	@Column(name = "tipo_impresora", length = 50)
	private String tipo_impresora;
	
	@Column(name = "margen_der", length = 50)
	private Float margen_der;
	
	@Column(name = "alto", length = 50)
	private Float alto;
	
	@Column(name = "ancho", length = 50)
	private Float ancho;
	
	@Column(name = "borrado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrado;
	
	@Column(name = "fecha_creacion",updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_creacion;
	
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_modificacion;
	
	@Column(name = "fecha_espejo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_espejo;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@ManyToOne( fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "sucursal_id")
	private Sucursal sucursal;
	
	@ManyToMany(cascade = CascadeType.ALL)
  	@JoinTable(name = "impresora_ordenador", 
	joinColumns = { @JoinColumn(name = "id", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "ordenador_id") })
	private Set<Ordenador> ordenador = new HashSet<>();
	
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "rowguid" , columnDefinition="uniqueidentifier")
	private String rowguid;

	@PrePersist
	public void prePersist() {
		this.fecha_creacion = new Date();
		this.fecha_modificacion = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorte() {
		return corte;
	}

	public void setCorte(String corte) {
		this.corte = corte;
	}

	public boolean getCancelado() {
		return cancelado;
	}

	public void setCancelado(boolean cancelado) {
		this.cancelado = cancelado;
	}
	
	public boolean getPredeterminado() {
		return predeterminado;
	}

	public void setPredeterminado(boolean predeterminado) {
		this.predeterminado = predeterminado;
	}
	
	public String getTipo_Impresora() {
		return tipo_impresora;
	}

	public void setTipo_Impresora(String tipo_impresora) {
		this.tipo_impresora = tipo_impresora;
	}

	public Float getMargen_izq() {
		return margen_izq;
	}

	public void setMargen_izq(Float margen_izq) {
		this.margen_izq = margen_izq;
	}
	
	public Float getMargen_der() {
		return margen_der;
	}

	public void setMargen_der(Float margen_der) {
		this.margen_der = margen_der;
	}
	
	public Float getAlto() {
		return alto;
	}

	public void setAlto(Float alto) {
		this.alto = alto;
	}
	
	public Float getAncho() {
		return ancho;
	}

	public void setSuperior(Float superior) {
		this.superior = superior;
	}
	
	public Float getSuperior() {
		return superior;
	}

	public void setInferior(Float inferior) {
		this.inferior = inferior;
	}
	
	public Float getInferior() {
		return inferior;
	}
	
	public void setAncho(Float ancho) {
		this.ancho = ancho;
	}
	
	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Date getFecha_espejo() {
		return fecha_espejo;
	}

	public void setFecha_espejo(Date fecha_espejo) {
		this.fecha_espejo = fecha_espejo;
	}
	
	public Date getBorrado() {
		return borrado;
	}

	public void setBorrado(Date borrado) {
		this.borrado = borrado;
	}

	public String getRowguid() {
		return rowguid;
	}

	public void setRowguid(String rowguid) {
		this.rowguid = rowguid;
	}
	
	public String getTipo_impresora() {
		return tipo_impresora;
	}

	public void setTipo_impresora(String tipo_impresora) {
		this.tipo_impresora = tipo_impresora;
	}

	public Set<Ordenador> getOrdenador() {
		return ordenador;
	}

	public void setOrdenador(Set<Ordenador> ordenador) {
		this.ordenador = ordenador;
	}
	
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

}
