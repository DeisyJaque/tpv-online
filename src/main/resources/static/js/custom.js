/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
//Notificacion
setTimeout(function() {
    $(".alert-tpv").fadeOut(1500);
},3000);

var value_sel="";
var ticket_reutilizar="";
//Sombrear cliente seleccionado en la vista "Clientes" del modulo de "Ventas"
$( window ).load(function() {
    var value= $( "input#myForm" ).val() ;
    sessionStorage.setItem("idDirUp","");
    sessionStorage.setItem("idDirDown","");
    var ticket_finalizado=$(".ticket_finalizado").attr("id");
    value_sel=value;
        $( "#"+value+".item-sel" ).css( "background-color","#cfe8e4" );  
        nvaventa= sessionStorage.getItem("NuevaVenta");
        if(nvaventa=="Hay venta"){
        	sessionStorage.setItem("NuevaVenta","");
        }
        /*if(ticket_finalizado=="Finalizada"){
			
        	 $( ".panel-derecho").attr("id","nullcli");
 		 	$( ".panel-derecho" ).empty();
			
		}*/
});

function printPDF(htmlPage)
{
    var w = window.open("about:blank");
    w.document.write(htmlPage);
    if (navigator.appName == 'Microsoft Internet Explorer') window.print();
    else w.print();
};



function CrearGastos(){
	flagError = false;
	regexFecha = /^(0[1-9]|1[0-9]|2[0-9]|3[01])\/(0[1-9]|1[012])\/[0-9]{4}$/;
	regexHora = /^([0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
	
	//Obtener los datos del modal
	var nro_factura = $('#nro_factura').val();
	var fecha_pago = 	$('#fechaPagoGasto').val();
	var cif = $('#cif').val();
	var proveedor = 	$('#proveedor').val();
	var tipo_gasto = $('#tipo_gasto').val();
	var concepto = $('#concepto').val();
	var base_imponible = $('#base_imponible').val();
	var iva = $('#iva').val();
	var porc_deduccion = $('#porc_deduccion').val();
	var comentario = $('#comentarios').val();
	var empleado = $('#empleado').val();
	
	if(fecha_pago == null || fecha_pago == ''){
		$('#fechaPagoGasto').addClass("alert-danger");
		$('#fechaPagoGastoCitaError').text("Completa este campo");
		flagError =true;
		
	}else{		  
		if( regexFecha.test(String(fecha_pago).toLowerCase()) == false){
			$('#fechaPagoGasto').addClass("alert-danger");
			$('#fechaPagoGastoError').text("La Fecha de Pago no tiene el formato correcto");
			flagError =true;
		}
	}

		
		$.ajax({
		    type : "GET",
		    url : "/tpv/factura/gastos/crear",
		    data : {nro_factura:nro_factura,
		    	fecha_pago:fecha_pago,
		    	cif:cif,
		    	proveedor:proveedor,
		    	tipo_gasto:tipo_gasto,
		    	concepto:concepto,
		    	base_imponible:base_imponible,
		    	iva:iva,
		    	porc_deduccion:porc_deduccion,
		    	comentario:comentario,
		    	empleado:empleado},

		    success : function(data) {
		    	
		    	location.href = '/tpv/factura/gastos' ;
		    },
		    error : function(e) {
		    	
		        console.log("ERROR: ", e);
		    }
		});
	

}

//Consulta de Factura-Ventas
function FiltrarFacturas(){
	
	 var  arr = $.map
		
     (
   		  
        $('.menuFiltros').find('.selectfact option:selected'), function(n)

         {
        	 if(n.value!=0){
        		 return n.value;
        	 }
             
				
          }

      );
	 var  arrall = $.map
		
     (
   		  
        $('.menuFiltros').find('.selectfact option:selected'), function(n)

         {
        	
        		 return n.value;
        	
          }

      );
	 var arrval = new Array();
	 var arrvall = new Array();
	 $('.camptext').each(function(){
		 if($(this).val()!=""){
			 arrval.push($(this).val());
		 }
		 arrvall.push($(this).val());
		});

	 
	 var arrdate = new Array();
	 var arrdateall = new Array();
	 
	 if( $("#filtroFechaDesde").val()!="" && $("#filtroFechaHasta").val()!=""){
		 var inputDesde = $("#filtroFechaDesde").datepicker("getDate");
		 var inputHasta = $("#filtroFechaHasta").datepicker("getDate");
		 var fechaDesde = $.datepicker.formatDate("yy-mm-dd", inputDesde);
		 var fechaHasta = $.datepicker.formatDate("yy-mm-dd", inputHasta);
		 arrdateall.push(fechaDesde);
		 arrdateall.push(fechaHasta);
		 
	 }else{
		 arrdateall.push("");
		 arrdateall.push("");
	 }
	 
	 $('.datepicker').each(function(){
		if($(this).val()!=""){
	
		 arrdate.push($(this).val());
		}
		});
	
	 if(arr.length==0 && arrval.length==0 && arrdate.length<2 ){
			$('#ModalNoCriterios').modal('show');
	 }else{
		 var criterios=arrdateall.concat(arrvall).concat(arrall);
		 console.log(criterios);
		  $.ajax({
				        type: "POST",
				        url: "/tpv/factura/buscar/"+criterios+"/ajax",
				        success: function(data)
				        {
				        	 $(".facturas-ajax").html( data );
				        	$.datepicker.regional['es'] = {
				        			closeText: 'Cerrar',
				        			prevText: '< Ant',
				        			nextText: 'Sig >',
				        			currentText: 'Hoy',
				        			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				        			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
				        			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				        			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
				        			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
				        			weekHeader: 'Sm',
				        			dateFormat: 'dd/mm/yy',
				        			firstDay: 1,
				        			isRTL: false,
				        			showMonthAfterYear: false,
				        			yearSuffix: ''
				        		};
				        		$.datepicker.setDefaults($.datepicker.regional['es']);
				        		
				        		$('.datepicker').datepicker({
				        		    format: 'dd/mm/yy',
				        		    changeYear: true,
				        		    changeMonth: true,
				        		    autoclose: true
				        		});
				        			 
				        }
				      });
		
	 }
};
function GuardarCambiosGastos(){
	var value = sessionStorage.getItem("idGastos");
	flagError = false;
	regexFecha = /^(0[1-9]|1[0-9]|2[0-9]|3[01])\/(0[1-9]|1[012])\/[0-9]{4}$/;
	regexHora = /^([0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
	
	//Obtener los datos del modal
	var nro_factura = $('#nro_factura').val();
	var fecha_pago = 	$('#fechaPagoGasto').val();
	var cif = $('#cif').val();
	var proveedor = 	$('#proveedor').val();
	var tipo_gasto = $('#tipo_gasto').val();
	var concepto = $('#concepto').val();
	var base_imponible = $('#base_imponible').val();
	var iva = $('#iva').val();
	var porc_deduccion = $('#porc_deduccion').val();
	var comentario = $('#comentarios').val();
	var empleado = $('#empleado').val();

	if(fecha_pago == null || fecha_pago == ''){
		$('#fechaPagoGasto').addClass("alert-danger");
		$('#fechaPagoGastoCitaError').text("Completa este campo");
		flagError =true;
		
	}else{		  
		if( regexFecha.test(String(fecha_pago).toLowerCase()) == false){
			$('#fechaPagoGasto').addClass("alert-danger");
			$('#fechaPagoGastoError').text("La Fecha de Pago no tiene el formato correcto");
			flagError =true;
		}
	}

		$.ajax({
		    type : "GET",
		    url : "/tpv/factura/gastos/guardar",
		    data : {id:value,
		    	nro_factura:nro_factura,
		    	fecha_pago:fecha_pago,
		    	cif:cif,
		    	proveedor:proveedor,
		    	tipo_gasto:tipo_gasto,
		    	concepto:concepto,
		    	base_imponible:base_imponible,
		    	iva:iva,
		    	porc_deduccion:porc_deduccion,
		    	comentario:comentario,
		    	empleado:empleado},

		    success : function(data) {
		    	
		    	location.href = '/tpv/factura/gastos' ;
		    },
		    error : function(e) {
		    	
		        console.log("ERROR: ", e);
		    }
		});

}

//Para que solo se pueda seleccionar un solo check box
//con la clase selectme

$('.selectme').click(function() {
    $('.selectme').not(this).prop('checked', false);
});


//Obtiene y retornar el valor del radio button seleccionado
//Si no se ha seleccionado ninguno, retorna -1

function radioValue(){

	var value = -1;
	if(document.querySelector('input[name="radioTable"]:checked')){
		value = document.querySelector('input[name="radioTable"]:checked').value;
	}
	
	console.log(value);
	return value;
	
};
var ticket_activo="";
var cliente="";
var fam="";

//Seleccionar familia para enviar el dato a función de productos
function seleccionarFamilia(idFamilia){
	var dato=idFamilia;
	 if (typeof(Storage) !== "undefined") {
		    // Store
		    sessionStorage.setItem("FamSel", dato);
		
		   
		} else {
		    document.getElementById("result").innerHTML = "Lo sentimos, su browser no soporta web storage";
		}
	 if(dato != -1 ){
			location.href = '/tpv/ventas/' + dato + '/productos';
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
		
	}
		
}

//InformesCompartir
function Compartir(idcomp){
	sessionStorage.setItem("idCompartir",idcomp);

	$('#ModalCompartirInformeMetodo').modal('show');	      
}
function CompartirDoc(comp){
    var idcomp=sessionStorage.getItem("idCompartir");
	       if(idcomp=="compartir-pdf"){
	    	   var pdf='pdf';
	        		$.ajax({
    		        type: "GET",
    		        url: '/tpv/listainformespdf/'+pdf,
    		        contentType: "application/json",
    		        success: function(data)
    		        {
    		        	var json = JSON.parse(data);
    		        	var idcomp=sessionStorage.getItem("idCompartir");
    		        
	        		if(comp=="imprimir"){
	        			
	        			var arr=[];
	            		for (var i in json[0]) {
	                		   arr.push(i);
	                		   // do some more stuff with obj[key]
	                		}
	            		
	            	
	            	printJS({printable: json, properties: arr, type: 'json',documentTitle:'Informe'});	
	        		
	        		}
	        		if(comp=="exportar-salvar"){
	        			var headers=[];
	        			var columns = [];
	        			var cont=0;
	        			for (var i in json[0]) {
	        				columns.push(  {title: $("th").eq(cont).html(), dataKey: i} );
	        				
	        				cont++;
	        				
	                		}
	        			
	        			var rows=data;
	          	
	          		var doc = new jsPDF('p', 'pt');
	          		doc.autoTable(columns, json, {
	          		    margin: {top: 60},
	          		    addPageContent: function(data) {
	          		    	doc.text("Informe", 40, 30);
	          		    }
	          		});
	          		doc.save('Informe.pdf');
	        		}
    		        }
    		        });
	        	}
	        	
	        	if(idcomp=="compartir-csv"){
	        		var csv='csv'; 
	        		$.ajax({
		     		        type: "GET",
		     		        url: '/tpv/listainformespdf/'+csv,
		     		        contentType: "application/json",
		     		        success: function(data)
		     		        {
	     		        	var json = JSON.parse(data);
	     		        	var idcomp=sessionStorage.getItem("idCompartir");
	        		if(comp=="imprimir"){
	        			var arr=[];
	            		for (var i in json[0]) {
	                		   arr.push(i);
	                		}
	            		
	            	
	            	printJS({printable: json, properties: arr, type: 'json',documentTitle:'InformeCSV'});	
	    	}
	        		if(comp=="exportar-salvar"){	      
	        		        if(json == '')
	        		            return;
	        		        
	        		        JSONToCSVConvertor(json, true);
	        		  

	        		function JSONToCSVConvertor(JSONData,ShowLabel) {
	        		    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
	        		    
	        		    var CSV = '';    
	        		    
	        		    if (ShowLabel) {
	        		        var row = "";
	        		        
	        		        for (var index in arrData[0]) {
	        		            
	        		            row += index + ',';
	        		        }

	        		        row = row.slice(0, -1);
	        		        
	        		        CSV += row + '\r\n';
	        		    }
	        		    
	        		    for (var i = 0; i < arrData.length; i++) {
	        		        var row = "";
	        		        
	        		        for (var index in arrData[i]) {
	        		            row += '"' + arrData[i][index] + '",';
	        		        }

	        		        row.slice(0, row.length - 1);
	        		        
	        		       
	        		        CSV += row + '\r\n';
	        		    }

	        		    if (CSV == '') {        
	        		        alert("Invalid data");
	        		        return;
	        		    }   
	        		    
	        		   
	        		    var fileName = "Informe_";
	        		 
	        		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
	        		   
	        		    var link = document.createElement("a");    
	        		    link.href = uri;
	        		    
	        		 
	        		    link.style = "visibility:hidden";
	        		    link.download = fileName + ".csv";
	        		    
	        		    document.body.appendChild(link);
	        		    link.click();
	        		    document.body.removeChild(link);
	        		}
	        		}
	        	}
	        	});
	        }
	            
}

  function EnviarInforme(){
	  			var arr="";
	    		var email=$("#email-informe").val();
	    		console.log(email);
	    		  $.ajax({
	    	         
	    	          type: 'POST',
	    	          url: '/tpv/enviaremail/'+email.split(),
	    	          success: function(data){
	    	        	  console.log(data);
	    	          }
	    	      });
	
}

//Fin informes compartir

function ReloginModal(){ 
	$('#modalRelogin').modal('show');

};



//Modal de no venta creada
function modalSinSeleccionVenta(){

	$('#modalSinSeleccionVenta').modal('show');
		
};


function ordenar(){
	 $("#tablaListarClientes").tablesorter({
		cssAsc: 'headerSortUp',
		cssDesc: 'headerSortDown'
	});
};

function marcarFavorito(idFavorito){
	var fav="";
	fav=idFavorito;
	fav.includes("false");
	var valor="";
	if(fav.includes("false")){
		$("#"+idFavorito).attr("id","true"+fav.substring(5, fav.length));
		valor=fav.substring(5, fav.length);
		$( "#no"+fav.substring(5, fav.length)).removeClass( "star-position" ).addClass( "star-fav" );
		$("#no"+fav.substring(5, fav.length)).attr("id","yes"+fav.substring(5, fav.length));
		
	}else{
		$("#"+idFavorito).attr("id","false"+fav.substring(4, fav.length));
		valor=fav.substring(4, fav.length);
		$( "#yes"+fav.substring(4, fav.length)).removeClass( "star-fav" ).addClass( "star-position" );
		
		$("#yes"+fav.substring(4, fav.length)).attr("id","no"+fav.substring(4, fav.length));
		
	}
	  $.ajax({
	         type: "POST",
	         url: "/tpv/ventas/favoritos/ajax/"+valor,
	         success: function(data)
	         {
	      	   console.log("Lo hiciste");
	         }
	       });

};


function url_imp(print) {
	 
	 return print;
	};



//Aviso emergente con efecto mouseover sobre un campo (se coloca para trabajar como aviso en campos de ordenamiento)
$(document).ready(function(){
	
			$('[data-toggle="tooltip"]').tooltip();
   
});
$(".ord-cli").click(function () {
    $('[data-toggle="tooltip"]').tooltip({
    	  disabled: true
    });
    $('[data-toggle="tooltip"]').tooltip({
  	  disabled: false
  });
    
   
  });
function onClick() {
    clicks += 1;
    document.getElementById("clicks").innerHTML = clicks;
};

//Seleccionar cliente para mostrar en panel de datos
var nombre="";
var a = false;
var timeout;

function seleccionarClienteTpv(idCliente){
	
	 timeout = setTimeout(function () {
	        if (a) {
	            a = false
	            if(value_sel==idCliente){
	         		cliente=$(".panel-derecho").attr("id");
	         	if(cliente=="nullcli"){
	         		
	         		value_sel="";
	         	 	$( "span" ).remove(".selector");
	         	 	
	         		$( "tr" ).css("background-color","");
	         		nombre="";
	         		sessionStorage.setItem("Cliente", "");
	         		
	         		$( "#observaciones_cliente" ).html( "" );
	         		
	         		}else{
	         			value_sel="";
	         		 	$( "span" ).remove(".selector");
	         			$( "tr" ).css("background-color","");
	         			nombre="";
	         			sessionStorage.setItem("Cliente", "");
	         			
	        	 		if(cliente==idCliente){ 
	        	 		 	 $( ".panel-derecho").attr("id","nullcli");
	        	 		 	$( ".panel-derecho" ).empty();
	        	 		
	        	 			}
	         		}
	         	}else{
	         		value_sel=idCliente;
	         		nombre=$(".nombre_cli"+value_sel).attr("id")+" "+$(".apellido_cli"+value_sel).attr("id");
	         		var dni=$(".documento_cli"+value_sel).attr("id");
	         		if( dni == undefined) dni = "";
	         		var telefono=$(".telefono_cli"+value_sel).attr("id");
	         		if( telefono == undefined) telefono = "";
	         		var observaciones=$(".observaciones_cli"+value_sel).attr("id"); 
	         		if( observaciones == undefined) observaciones = "";
	         		$( "span" ).remove(".selector");
	         		$( "#nombre_cliente" ).append( '<span style="font-size: 0.9em;" class="selector">'+nombre+'</span>' );
	         		$( "#dni_cliente" ).append( '<span style="font-size: 0.9em;" class="selector">'+dni+'</span>' );
	         		$( "#telefono_cliente" ).append( '<span style="font-size: 0.9em;" class="selector">'+telefono+'</span>' );

	         		$( "#observaciones_cliente" ).html( '<p class="text-observaciones-clientes-ventas selector" >'+observaciones+'</p><img id="img-observaciones-cliente" onclick="abrirModalObservacionesCliente()" alt="observaciones cliente" src="/images/iconos/oscuros/lapiz.png" width="16" height="18" style="cursor:pointer;margin-left: 4px;">' );
	         		$("#id-cliente-observaciones").val(idCliente);
	         		$("#form-observaciones-cliente").val(observaciones);
	         		
	         		$( "tr" ).css("background-color","");
	         	 	$( "#"+value_sel+".item-sel" ).css( "background-color","#cfe8e4" );
	         	 	sessionStorage.setItem("Cliente", nombre);	
	         	 
	         	}
	        } else {
	            a = true
	            if(value_sel==idCliente){
	         		cliente=$(".panel-derecho").attr("id");
	         	if(cliente=="nullcli"){ 
	         		
	         		value_sel="";
	         	 	$( "span" ).remove(".selector");
	         	 	
	         		$( "tr" ).css("background-color","");
	         		nombre="";
	         		sessionStorage.setItem("Cliente", "");
	         		
	         		$( "#observaciones_cliente" ).html( "" );
	         		
	         		}else{
	         			value_sel="";
	         		 	$( "span" ).remove(".selector");
	         			$( "tr" ).css("background-color","");
	         			nombre="";
	         			sessionStorage.setItem("Cliente", "");
	        	 		if(cliente==idCliente){ 
	        	 		 	 $( ".panel-derecho").attr("id","nullcli");
	        	 		 	$( ".panel-derecho" ).empty();
	        	 		
	        	 			}
	         		}
	         	}else{
	         		value_sel=idCliente;
	         		nombre=$(".nombre_cli"+value_sel).attr("id")+" "+$(".apellido_cli"+value_sel).attr("id");
	         		var dni=$(".documento_cli"+value_sel).attr("id");
	         		if( dni == undefined) dni = "";
	         		var telefono=$(".telefono_cli"+value_sel).attr("id");
	         		if( telefono == undefined) telefono = "";
	         		var observaciones=$(".observaciones_cli"+value_sel).attr("id"); 
	         		if( observaciones == undefined) observaciones = "";
	         	  	$( "span" ).remove(".selector");
	         		$( "#nombre_cliente" ).append( '<span style="font-size: 0.9em;" class="selector">'+nombre+'</span>' );
	         		$( "#dni_cliente" ).append( '<span style="font-size: 0.9em;" class="selector">'+dni+'</span>' );
	         		$( "#telefono_cliente" ).append( '<span style="font-size: 0.9em;" class="selector">'+telefono+'</span>' );
	         		
	         		$( "#observaciones_cliente" ).html( '<p class="text-observaciones-clientes-ventas selector" >'+observaciones+'</p><img id="img-observaciones-cliente" onclick="abrirModalObservacionesCliente()" alt="observaciones cliente" src="/images/iconos/oscuros/lapiz.png" width="16" height="18" style="cursor:pointer;margin-left: 4px;">' );
	         		$("#id-cliente-observaciones").val(idCliente);
	         		$("#form-observaciones-cliente").val(observaciones);
	         		
	         		$( "tr" ).css("background-color","");
	         	 	$( "#"+value_sel+".item-sel" ).css( "background-color","#cfe8e4" );
	         	 	sessionStorage.setItem("Cliente", nombre);	
	         	 
	         	}
	        }
	    }, 200);
}

function seleccionarClienteConDobleClick(idCliente){
	location.href = '/tpv/clientes/'+idCliente+'/editar' ;
}

function seleccionarTodos(){

	if ($('.seleccionar_todo').prop("checked")==true){
	
		$('.seleccionar_todo').prop("checked",true);
		$('.input_fact').prop("checked",true);
	}else{
		$('.input_fact').prop("checked",false);
		$('.seleccionar_todo').prop("checked",false);
	}
	
}

function ordenarFacturas(){
	$("#tablaFacturas").tablesorter({
		cssAsc: 'headerSortUp',
		cssDesc: 'headerSortDown'
	});
}

function ordenarTablas(){
	$("#tablaLotes").tablesorter({
		cssAsc: 'headerSortUp',
		cssDesc: 'headerSortDown'
	});
}

//Presentar lote de facturas
function SeleccionarPresentadoLote(id){
	var sub=id.substring(10, id.lenght);
	$('#textopresentar').append('<p>¿Usted está seguro de presentar el Lote: '+sub+' </p>');
	$('#ModalPresentar').modal('show');
	sessionStorage.setItem("idPresentado", id);
}
function PresentarLoteSel(){
	var idPresentado=sessionStorage.getItem("idPresentado");
	var sub=idPresentado.substring(10, idPresentado.lenght);
	var fechaPresentado=$('#fechaPresentar').val();

	 if(fechaPresentado!="" ){
		 var fechaPresentado = $("#fechaPresentar").datepicker("getDate");
		 var busqueda = $.datepicker.formatDate("yy-mm-dd", fechaPresentado);
			$.ajax({
		   		type : 'POST',
		   		url : '/tpv/factura/lotes/presentar/'+sub+'/'+busqueda,
		   		success:function(data){
		   			location.href='/tpv/factura/lotes';
		   		}
		   		
			});
		 
	 }else{
		 $('#modalSinFecha').modal('show');
	 }

}
function FiltrarLotes(){
	
	
	 var arrdate = new Array();
	 var arrdateall = new Array();
	 
	 if( $("#filtroFechaDesde").val()!="" && $("#filtroFechaHasta").val()!=""){
		 var inputDesde = $("#filtroFechaDesde").datepicker("getDate");
		 var inputHasta = $("#filtroFechaHasta").datepicker("getDate");
		 var fechaDesde = $.datepicker.formatDate("yy-mm-dd", inputDesde);
		 var fechaHasta = $.datepicker.formatDate("yy-mm-dd", inputHasta);
		 arrdateall.push(fechaDesde);
		 arrdateall.push(fechaHasta);
		 
	 }else{
		 arrdateall.push("");
		 arrdateall.push("");
	 }
	 
	 
	 $('.datefe').each(function(){
		if($(this).val()!=""){
	
		 arrdate.push($(this).val());
		}
		});
	 if($("#filtroFechaEnvio").val()!=""){
		 var inputEnvio= $("#filtroFechaEnvio").datepicker("getDate");
		 var fechaEnvio = $.datepicker.formatDate("yy-mm-dd", inputEnvio);
		 arrdateall.push(fechaEnvio);
	 	}else{
	 		 arrdateall.push("");
	 	}
	 if($('#filtroEnviado').prop('checked') == true){
		 arrdateall.push("novacio");
	 }else{
		 arrdateall.push("");
	 }
	 if($('#filtroPresentado').prop('checked') == true){
		 arrdateall.push("novacio");
	 }else{
		 arrdateall.push("");
	 }
	 if($("#filtroFechaEnvio").val()=="" && arrdate.length<2 && $('#filtroEnviado').prop('checked') == false && $('#filtroPresentado').prop('checked') == false){
			$('#ModalNoCriterios').modal('show');
	 }else{
		 var criterios=arrdateall;
		 console.log(criterios);
		  $.ajax({
				        type: "POST",
				        url: "/tpv/factura/lotes/buscar/"+criterios+"/ajax",
				        success: function(data)
				        {
				        	 $(".lotes-ajax").html( data );
				        	$.datepicker.regional['es'] = {
				        			closeText: 'Cerrar',
				        			prevText: '< Ant',
				        			nextText: 'Sig >',
				        			currentText: 'Hoy',
				        			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				        			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
				        			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				        			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
				        			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
				        			weekHeader: 'Sm',
				        			dateFormat: 'dd/mm/yy',
				        			firstDay: 1,
				        			isRTL: false,
				        			showMonthAfterYear: false,
				        			yearSuffix: ''
				        		};
				        		$.datepicker.setDefaults($.datepicker.regional['es']);
				        		
				        		$('.datepicker').datepicker({
				        		    format: 'dd/mm/yy',
				        		    changeYear: true,
				        		    changeMonth: true,
				        		    autoclose: true
				        		});
				        }
				      });
		
	 }
	
};


function volverAnterio(){
	$('#modalSinFecha').modal('hide');
	 $('#ModalPresentar').modal('show');
}
function QuitarcheckPresentado(){
	var idPresentado=sessionStorage.getItem("idPresentado");
	if ($('#'+idPresentado).prop("checked")==true){
		$('#'+idPresentado).prop("checked",false);
	}
};
function SeleccionarFactura(valor){

	
	if($('.fact'+valor).prop('checked') == true){
		$('.fact'+valor).prop("checked",false);
		$('.seleccionar_todo').prop("checked",false);
	//	sessionStorage.setItem("OrigenFactura","" );
	}else{
	$('.fact'+valor).prop("checked",true);
//	sessionStorage.setItem("OrigenFactura",valor );
	
}
	if ($('.input_fact:checkbox:checked').length === 0) {
		$('.seleccionar_todo').prop("checked",false);
      
    }
	if ($('.input_fact:checkbox:checked').length ==$('.input_fact:checkbox').length) {
		$('.seleccionar_todo').prop("checked",true);
      
    }
	

}
function selventa(valor){
	sessionStorage.setItem("OrigenFactura",valor );
}
function SeleccionarDesAnular(valor){
	 $.ajax({
	        type: "POST",
	        url: "/tpv/factura/habilitar/ajax/"+valor,
	        success: function(data)
	        {
	        	location.href="/tpv/factura";
	        }
	 });
}


function GenerarFactura(){
	if($('.input_fact').is(':checked') && $('.facturas-ajax').is(':empty')==false){
		var checkedVals = $('.input_fact:checkbox:checked').map(function() {
		    return this.value;
		}).get();
		 var criterios=checkedVals.join(",");
		 
		 if (criterios.length==1){
			 criterios=criterios;
		 }else{
			 criterios=criterios.split();
		 }
		 $.ajax({
		        type: "POST",
		        url: "/tpv/factura/generar/ajax/"+criterios,
		        success: function(data)
		        {
		        	
		        	location.href="/tpv/factura";
		        	
		        }
		 });
	
		
	}else{
		 $('#ModalNoGenerar').modal('show');
	}
	
	
}

function IrAVentaDesdeFactura(){
	venta=sessionStorage.getItem("OrigenFactura");
	if (venta==""){
		 $('#modalSinSeleccionPendiente').modal('show');
	}else{ 
		
		location.href = '/tpv/ventas/'+venta+'/6';
			
		}
	
}


//Seleccionar o aceptar un cliente se muestran datos en panel derecho de calculardora
$("#seleccionarCli").click(function () {
	
	$( "span" ).remove(".panel-derecho");
	 $( "#client-right" ).append( '<span style="font-size: 0.9em;" class="panel-derecho">'+nombre+'</span>' );
});

var pago="";
//Seleccionar pago en panel izquierdo en pagos
$(".pago-sel").click(function () {
	if(pago==$(this).attr("id")){
		$( "tr" ).css("background-color","");
	}else{
	 pago=$(this).attr("id");
	 $( "tr" ).css("background-color","");
	 $( "#"+pago+".pago-sel" ).css( "background-color","#cfe8e4" ); 
	}
});

var venta="";
//Seleccionar venta a reabrir
$(".venta-sel").click(function () {
	if(venta==$(this).attr("id")){
		venta="";
		 sessionStorage.setItem("Venta", "");
		
		var modfactura=$(this).attr('class');
		if( modfactura.includes("gastonulo")){
			$(this).css("background-color","#DEDBDB");
		}else{
			$( "tr" ).css("background-color","");
			sessionStorage.setItem("idGastos", "");
		
			$('.gastonulo').css("background-color","#DEDBDB");
		}
	}else{
		venta=$(this).attr("id");
		 sessionStorage.setItem("Venta",venta);
	
	 $( "tr" ).css("background-color","");
	 $('.gastonulo').css("background-color","#DEDBDB");
	 $( "#"+venta+".venta-sel" ).css( "background-color","#cfe8e4" ); 
	}
});



function anularFacturaSeleccionado(){
	
	if($('.input_fact').is(':checked')){
		var checkedVals = $('.input_fact:checkbox:checked').map(function() {
			var idfact=$(this).val();
			return this.value;
		}).get();

		 var criterios=checkedVals.join(",");
		 if (criterios.length==1){
			 criterios=criterios;
		
		 }else{
			 criterios=criterios.split();
		 }

		 if(criterios>0){
			 $.ajax({
			        type: "POST",
			        url: "/tpv/factura/anular/"+criterios,
			        success: function(data)
			        {
			        	location.href="/tpv/factura";
			        }
			 });
		 }

	}
	
};

function anularLotesSeleccionado(){
	
	if($('.input_fact').is(':checked')){
		var checkedVals = $('.input_fact:checkbox:checked').map(function() {
			var idfact=$(this).val();
			return this.value;
		}).get();

		 var criterios=checkedVals.join(",");
		 if (criterios.length==1){
			 criterios=criterios;
		
		 }else{
			 criterios=criterios.split();
		 }

		 if(criterios>0){
			 $.ajax({
			        type: "POST",
			        url: "/tpv/factura/anular/"+criterios,
			        success: function(data)
			        {
			        	location.href="/tpv/factura";
			        }
			 });
		 }

		
		
		
	}
	
};

function verifyLotesElim(){
	 var arr = new Array();
	 $('.checkselect').each(function(){
		 	var idtemp= $(this).attr("value");
		    arr.push($(this).attr("value"));
		  
		})
		$('.presen').each(function(){
		 	
		    console.log($(this).attr("id"));
		})
};

function NoFacturaSeleccionada(){
	if($('.input_fact').is(':checked')){
		$('#ModalAnularFactura').modal('show');
		
	}else{
		$('#ModalAnularFacturaNo').modal('show');
		console.log('Debe seleccionar una opción');
	}
}



//Reabrir y escoger operacion si hay venta activa
function IrAVenta(){
	ticket_activo=$(".ticket_act").attr("id");
	if (venta==""){
		 $('#modalSinSeleccionPendiente').modal('show');
	}else{ 
			if(ticket_activo=="nullnro"){
				sessionStorage.setItem("Origen", "");
				location.href = '/tpv/ventas/'+venta+'/5';
			}else{ 
				 sessionStorage.setItem("Origen", "abrirpendiente");
				 location.href = '/tpv/ventas/'+venta+'/5';
			}
		}
	
}

function IrAVentaDev(){
	var venta=$(".ticket_dev").attr("id");
	
	location.href = '/tpv/ventasdev/'+venta;
		
}

//Confirmar uso de ticket vacío mas antiguo
function ConfirmarReutilizar(){
	location.href = '/tpv/ventas/reutilizar';
}


//Listar favoritos
function Favoritos(){
	location.href = '/tpv/ventas/favoritos';
}

function AsignarValue(valor){
	ticket_reutilizar=valor;
};
function VerificiarStatus(){
	 $.ajax({
        type: "GET",
        url: '/tpv/ventas/pendienteshoy/ajax',
        contentType: "application/json",
        success: function (data) {
       if(data=="lleno"){
       		
       		 ticket_reutilizar="ticket_vacio";
       		AsignarValue(ticket_reutilizar);
   
       	 }
       	
        }
      });
	
};


//Nuevo ticket
function NuevaVenta(){
	
	VerificiarStatus();
	
	origen= sessionStorage.getItem("Origen");
	if(ticket_reutilizar==""){
		ticket_reutilizar=$(".reutilizar").attr("id");
	}
	
	ticket_activo=$(".ticket_act").attr("id");
	var ticket_finalizado=$(".ticket_finalizado").attr("id");
	var nohayhoyvacia=$(".anterior_vacia").attr("id");
	if((ticket_activo!="nullnro" && ticket_finalizado!="Finalizada") || (ticket_activo!="nullnro" && ticket_finalizado!="Finalizada" && ticket_reutilizar!="ticket_lleno") ){
		
			sessionStorage.setItem("Origen", "");
			if(ticket_reutilizar=="ticket_vacio"){
				 $('#ModalReusarTicket').modal('show');
			}else{
				sessionStorage.setItem("Cliente", "");
				 sessionStorage.setItem("NuevaVenta", "Hay venta");
				sessionStorage.setItem("Origen", "Nueva Venta");
				 $.ajax({
			         type: "GET",
			         url: '/tpv/ventas/venta',
			         success: function(data)
			         {
			        	 location.href = '/tpv/ventas';
			         }
			       });
			}
		
		
		
	}else{
		if(ticket_reutilizar=="ticket_vacio"){
			 $('#ModalReusarTicket').modal('show');
		}else{ 
			 sessionStorage.setItem("Cliente", "");
			 sessionStorage.setItem("NuevaVenta", "Hay venta");
			sessionStorage.setItem("Origen", "Nueva Venta");
			 $.ajax({
		         type: "GET",
		         url: '/tpv/ventas/venta',
		         success: function(data)
		         {
		        	 
		        	 location.href = '/tpv/ventas';
		         }
		       });
			
			
			
		}	
		
	}
};


//Nuevo ticket y enviar item seleccionado a pendientes
function EnviarSeleccionado(){
	
	origen= sessionStorage.getItem("Origen");
	ticket_activo=$(".ticket_act").attr("id");
	venta=sessionStorage.getItem("Venta");
	if(ticket_activo!="nullnro" ){
		
		value_sel="";
		nombre="";
		sessionStorage.setItem("Cliente", "");
		if(origen=="abrirpendiente"){
			location.href = '/tpv/ventas/'+venta+'/4'
			sessionStorage.setItem("Origen", "");
			 sessionStorage.setItem("Venta", "");
		}else{
			location.href = '/tpv/ventas/'+ticket_activo+'/3';
			sessionStorage.setItem("Origen", "");
			 sessionStorage.setItem("Venta", "");
		}
		
	}else{
		 $.ajax({
	         type: "GET",
	         url: '/tpv/ventas/venta',
	         success: function(data)
	         {
	        	 location.href = '/tpv/ventas';
	         }
	       });
	}
	
	
};

//Cambio de cliente
function CambioCliente(){
	ticket_activo=$(".ticket_act").attr("id");
	$.ajax({
        type: "GET",
        url: "/tpv/ventas/clientedesc/ajax/"+value_sel,
        success: function(data)
        {
     	   console.log("Lo hiciste");
        }
      });
	location.href = '/tpv/ventas/'+value_sel+'/cliente/'+ticket_activo;
}

//Aceptar seleccionado para proceder con la venta
function SeleccionarVenta(){
	if (timeout) {
        clearTimeout(timeout);
    }
	cliente=$(".panel-derecho").attr("id");
	ticket_activo=$(".ticket_act").attr("id");
	console.log(cliente);
	if(nombre!=""){
		  sessionStorage.setItem("Cliente", nombre);
	}
	var clienteacep = sessionStorage.getItem("Cliente");
	console.log(clienteacep);
	var ticket_finalizado=$(".ticket_finalizado").attr("id");
	
	if(value_sel!=""){
		if(cliente=="nullcli"  && ticket_activo =="nullnro" ){
		
			
			 $('#modalSinSeleccionVenta').modal('show');
				
				
		}else{
			if(cliente=="nullcli" && clienteacep!="" && ticket_activo =="nullnro" && value_sel!=undefined ){
			
				 $('#modalSinSeleccionVenta').modal('show');
				
			
		}else{
			if(cliente=="nullcli" && clienteacep!="" && ticket_activo !="nullnro" && value_sel!=undefined && ticket_finalizado!="Finalizada"){
				$.ajax({
			        type: "GET",
			        url: "/tpv/ventas/clientedesc/ajax/"+value_sel,
			        success: function(data)
			        {
			     	   console.log("Lo hiciste");
			        }
			      });
				
					location.href = '/tpv/ventas/'+value_sel+'/cliente/'+ticket_activo;
				
		}else{
			if(cliente!="nullcli" && clienteacep!="" && ticket_activo !="nullnro" && value_sel!=undefined && ticket_finalizado!="Finalizada"){
				

				 $('#ModalCambioCliente').modal('show');
				
			
				}
			}
		}
	}
		
	
	}
	if(ticket_finalizado=="Finalizada" && value_sel!=""){
		 $('#modalSinSeleccionVentaFinal').modal('show');
	}

	if(ticket_activo !="nullnro" && cliente!="nullcli" && value_sel=="" && ticket_finalizado!="Finalizada"){
		sessionStorage.setItem("Cliente", "");
		
		location.href = '/tpv/ventas/'+0+'/cliente/'+ticket_activo;
		
	}
	if(ticket_activo !="nullnro" && cliente!="nullcli" && value_sel!="" && ticket_finalizado!="Finalizada" ){
		if(value_sel==undefined){
			value_sel=0;
		}
		
		 $('#ModalCambioCliente').modal('show');
		
	}
	if(ticket_activo !="nullnro" && cliente=="nullcli" && value_sel!="" && clienteacep=="null" && ticket_finalizado!="Finalizada"){
		if(value_sel==undefined){
			value_sel=0;
		}
		$.ajax({
	        type: "POST",
	        url: "/tpv/ventas/clientedesc/ajax/"+value_sel,
	        success: function(data)
	        {
	     	   console.log("Lo hiciste");
	        }
	      });
		location.href = '/tpv/ventas/'+value_sel+'/cliente/'+ticket_activo;
		
	}
	if(ticket_activo!="nullnro" && cliente=="nullcli" && (value_sel=="" || value_sel==null)){
		
		$('#modalSinSeleccionClienteVenta').modal('show');
	}
};

//SeleccionDeClientePara Venta
function SeleccionarClienteVenta(){
	
	cliente=$(".panel-derecho").attr("id");
	ticket_activo=$(".ticket_act").attr("id");

	if(nombre!=""){
		  sessionStorage.setItem("Cliente", nombre);
	}
	var clienteacep = sessionStorage.getItem("Cliente");
	console.log(clienteacep);
	var ticket_finalizado=$(".ticket_finalizado").attr("id");
	
	if(value_sel!=""){
		if(cliente=="nullcli"  && ticket_activo =="nullnro" ){
		
			
			 $('#modalSinSeleccionVenta').modal('show');
				
				
		}else{
			if(cliente=="nullcli" && clienteacep!="" && ticket_activo =="nullnro" && value_sel!=undefined ){
			
				 $('#modalSinSeleccionVenta').modal('show');
				
			
		}else{
			if(cliente=="nullcli" && clienteacep!="" && ticket_activo !="nullnro" && value_sel!=undefined && ticket_finalizado!="Finalizada"){
				$.ajax({
			        type: "GET",
			        url: "/tpv/ventas/clientedesc/ajax/"+value_sel,
			        success: function(data)
			        {
			     	   console.log("Lo hiciste");
			        }
			      });
				
					location.href = '/tpv/ventas/'+value_sel+'/cliente/'+ticket_activo;
				
		}else{
			if(cliente!="nullcli" && clienteacep!="" && ticket_activo !="nullnro" && value_sel!=undefined && ticket_finalizado!="Finalizada"){
				

				 $('#ModalCambioCliente').modal('show');
				
			
				}
			}
		}
	}
		
	
	}
	if(ticket_finalizado=="Finalizada" && value_sel!=""){
		 $('#modalSinSeleccionVentaFinal').modal('show');
	}
	

	if(ticket_activo !="nullnro" && cliente!="nullcli" && value_sel=="" && ticket_finalizado!="Finalizada"){
		sessionStorage.setItem("Cliente", "");
		
		location.href = '/tpv/ventas/'+0+'/cliente/'+ticket_activo;
		
	}
	if(ticket_activo !="nullnro" && cliente!="nullcli" && value_sel!="" && ticket_finalizado!="Finalizada" ){
		if(value_sel==undefined){
			value_sel=0;
		}
		
		 $('#ModalCambioCliente').modal('show');
		
	}
	if(ticket_activo !="nullnro" && cliente=="nullcli" && value_sel!="" && clienteacep=="null" && ticket_finalizado!="Finalizada"){
		if(value_sel==undefined){
			value_sel=0;
		}
		$.ajax({
	        type: "POST",
	        url: "/tpv/ventas/clientedesc/ajax/"+value_sel,
	        success: function(data)
	        {
	     	   console.log("Lo hiciste");
	        }
	      });
		location.href = '/tpv/ventas/'+value_sel+'/cliente/'+ticket_activo;
		
	}
	if(ticket_activo!="nullnro" && cliente=="nullcli" && (value_sel=="" || value_sel==null)){
		
		$('#modalSinSeleccionClienteVenta').modal('show');
	}
};


function SeleccionSinVenta(){
	location.href = '/tpv/ventas/'+value_sel+'/cliente/null';
}



//Volver a productos seleccionados
function VolverProductos(){
	 if (typeof(Storage) !== "undefined") {

		 	if(fam==""){
		 		fam = sessionStorage.getItem("FamSel");
		 		
		 	}
		    
	 } else {
		    document.getElementById("result").innerHTML = "Lo sentimos, su browser no soporta web storage";
		}    
	if(fam != -1){
		location.href = '/tpv/ventas/' + fam + '/productos';
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar un item');
	}
};


function networkInfo(){
	
	if ( typeof(document.macaddress_applet.isActive)  )
    {
        console.log( "Unable to detect MAC addresses." );
    }
    else
    {
        var interfaces = eval( String( document.macaddress_applet.getInterfacesJSON() ) );
        var interface_string = "";

        for( var idx = 0; idx < interfaces.length; idx++ )
        {
            var tmp = interfaces[idx].split(";");
            interface_string += "NIC " + (idx+1) + ": " + tmp[1] + " (" + tmp[0] + ")<br />\n ";
        }

        if ( interface_string.length > 0 )
        {
          
            console.log( interface_string);
        }
    }

	
	}

function verSeleccionado(dominio){
	var value = radioValue();
	
	var valor= value_sel; 
	if(valor!=undefined && value==-1){
		value=valor;
		console.log(value);
	}
	
	if(value != -1 ){
		location.href = dominio + value;
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
		
	}

};


function verSeleccionadoTPV(dominio, sucursal){
	value = radioValue();
	if(value != -1){
		location.href = dominio + value + "?sucursal="+ sucursal;
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};

function editarSeleccionado(dominio){
	var value = radioValue();
	// De la parte de Ventas-Cliente para no elaborar dos editar
	var valor= value_sel; 
	if(valor!=undefined && value==-1 && valor.length!=0){
		value=valor;
	}
	
	if(value != -1 ){
		location.href = dominio + value + '/editar';
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};
function IrAMovimientos(){
	location.href="/tpv/cajas/movimientos";
}
function ObtenerIdGasto(id){
		var esgasto=sessionStorage.getItem("idGastos");
		if(esgasto==id){
			
			if( modfactura.includes("gastonulo")){
				$("#"+id).css("background-color","#DEDBDB");
			}else{
				$( "tr" ).css("background-color","");
				sessionStorage.setItem("idGastos", "");
			
				$('.gastonulo').css("background-color","#DEDBDB");
			}
		}else{
		
			 sessionStorage.setItem("idGastos",esgasto);
		
		 $( "tr" ).css("background-color","");
		 $('.gastonulo').css("background-color","#DEDBDB");
		 $( "#"+id+".venta-sel" ).css( "background-color","#cfe8e4" ); 
		}
	
	sessionStorage.setItem("idGastos", id);
}
function editarGastoSeleccionado(){
	var value = sessionStorage.getItem("idGastos");
	
	if(value != -1 ){
		$.ajax({
	        type: "GET",
	        url: "/tpv/factura/gastos/editar/"+value,
	        success: function(data)
	        {
	        	$("#titulo_gasto").empty();
	        	$("#titulo_gasto").text("Editar Gastos");
	        	$("#nro_factura").val(data[0]);
	        	var fechaSplit = data[1].split("-");
				var fechaParaBuscar =fechaSplit[2]+"/"+fechaSplit[1]+"/"+fechaSplit[0];
				$("#fechaPagoGasto").val(fechaParaBuscar);
	        	$("#cif").val(data[2]);
	        	$("#proveedor").val(data[3]);
	        	$("#tipo_gasto").val(data[4]);
	        	$("#concepto").val(data[5]);
	        	$("#base_imponible").val(data[6]);
	        	$("#iva").val(data[7]);
	        	$("#porc_deduccion").val(data[8]);
	        	$("#comentarios").val(data[9]);
	        	console.log(data[10]);
	        	$("#empleado").val(data[10]);
	        	var confirm=$(".clickConfirmar").attr("id");
	        	if(confirm=="crearGastos"){
	        		$("#confirmarGasto").empty();
	        		$("#confirmarGasto").append(' <button id="guardarGastos" class="green-button btn btn-success m-b-3 clickConfirmar"  onclick="GuardarCambiosGastos();" ><i class="fa fa-btn fa-check"></i> Confirmar</button>' );
	        	}
	        	$('#modalCrearGasto').modal('show');
	        }
	      });
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};

function mostrarNombre(){
	$("#titulo_gasto").empty();
	$("#titulo_gasto").text("Crear Gastos");
	
	$('#nro_factura').val('');
	$('#fechaPagoGasto').val('');
	$('#cif').val('');
	$('#proveedor').val('');
	$('#tipo_gasto').val('');
	 $('#concepto').val('');
	 $('#base_imponible').val('');
	 $('#iva').val('');
	 $('#porc_deduccion').val('');
	 $('#comentarios').val('');
	 $('#empleado').val('');
	
	var confirm=$(".clickConfirmar").attr("id");
	if(confirm=="guardarGastos"){
		$("#confirmarGasto").empty();
		$("#confirmarGasto").append(' <button id="crearGastos" class="green-button btn btn-success m-b-3 clickConfirmar"  onclick="CrearGastos();" ><i class="fa fa-btn fa-check"></i> Confirmar</button>' );
	}
	
}
function editarSeleccionadoTPV(dominio,sucursal){
	value = radioValue();
	if(value != -1){
		location.href = dominio + value + '/editar?sucursal=' + sucursal;
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};

function editarActual(dominio, id){

	var value = $(id).text();
	location.href = dominio + value + '/editar';
};

function abrirModalEliminar(){
	var value = radioValue();
	// De la parte de Cliente para no elaborar dos funciones que hagan lo mismo
	var valor= value_sel; 
	if(valor!=undefined && value==-1){
		value=valor;
	}
	
	if(value != -1){
		
		$('#ModalEliminar').modal('show');
		//location.href = dominio + value + '/eliminar';
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};

function eliminarSeleccionado(dominio){
	value = radioValue();
	
	if(value != -1){
		location.href = dominio + value + '/eliminar';
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};

function eliminarSeleccionadoTPV(dominio, sucursal){
	value = radioValue();
	if(value != -1){
		location.href = dominio + value + '/eliminar?sucursal=' + sucursal;
	}else{
		$('#modalSinSeleccion').modal('show');
		console.log('Debe seleccionar una opción');
	}
	
};

// Funcion para confirmar la eliminación en el modal. Este modal está en la opciones de 'Ver'
// dominio: parámetro de entrada que indica parte del URL. Ejemplo: '/admin/empresas/'
// id: es el tag en donde se especifica el id a eliminar. Ejemplo: #idEmpresa
function eliminarActual(dominio, id){
	var value = $(id).text();
	location.href = dominio + value + '/eliminar';
};

function eliminarActualTPV(dominio, id,sucursal){
	var value = $(id).text();
	location.href = dominio + value + '/eliminar?sucursal=' + sucursal;
};


