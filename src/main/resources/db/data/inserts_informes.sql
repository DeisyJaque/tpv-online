----------------- Querys [dbo].[informes] -----------------------


/*INSERT INTO [dbo].[informes]([nombre], [query], [tipo]) VALUES ('Clientes','select c.nombre,c.apellido,c.email,c.telefono,c.descuento from cliente c JOIN sucursal s on c.sucursal_id=s.id and s.id=#SUCURSAL# and c.id=#CLIENTE#', 'básico');
INSERT INTO [dbo].[informes]([nombre], [query], [tipo]) VALUES ('Ventas por agente','SELECT id,total,agente,fecha  FROM venta WHERE sucursal_id=#SUCURSAL# and usuario_id=#AGENTE# AND fecha  >="#DESDE#" and fecha<="#HASTA#" and estado="Finalizada"', 'básico');
INSERT INTO [dbo].[informes]([nombre], [query], [tipo]) VALUES ('Productos','select nombre,descripcion from articulo where id=#FAMILIA#', 'básico');
INSERT INTO [dbo].[informes]([nombre], [query], [tipo]) VALUES ('Marcas','SELECT m.id,m.nombre FROM marca m JOIN mar_emp_suc_usu_rol mes on  mes.sucursal_id is null and mes.empresa_id is null and  mes.marca_id=m.id and m.id=#MARCA# JOIN usuario u on mes.usuario_id=u.id and u.id=#USUARIOLOG#', 'básico');
GO*/

INSERT INTO [dbo].[grupo_informes]([nombre], [tipo]) VALUES ('Información de Clientes', 'básico');
INSERT INTO [dbo].[grupo_informes]([nombre], [tipo]) VALUES ('Información de Ventas','básico');
INSERT INTO [dbo].[grupo_informes]([nombre], [tipo]) VALUES ('Información de Productos', 'básico');
INSERT INTO [dbo].[grupo_informes]([nombre], [tipo]) VALUES ('Información de Marcas', 'básico');
GO

INSERT INTO [dbo].[informes]([nombre], [query], [tipo], [grupo_id]) VALUES ('Clientes en general','select c.nombre,c.apellido,c.email,c.telefono,c.descuento from cliente c JOIN sucursal s on c.sucursal_id=s.id and s.id=#SUCURSAL# and c.id=#CLIENTE#', 'básico',1);
INSERT INTO [dbo].[informes]([nombre], [query], [tipo], [grupo_id]) VALUES ('Ventas mensuales por agente','SELECT id,total,agente,fecha  FROM venta WHERE sucursal_id=#SUCURSAL# and usuario_id=#AGENTE# AND fecha  >="#DESDE#" and fecha<="#HASTA#" and estado="Finalizada"', 'básico',2);
INSERT INTO [dbo].[informes]([nombre], [query], [tipo], [grupo_id]) VALUES ('Ventas por agente','SELECT id,total,agente,fecha  FROM venta WHERE sucursal_id=#SUCURSAL# and usuario_id=#AGENTE# and estado="Finalizada"', 'básico',2);
INSERT INTO [dbo].[informes]([nombre], [query], [tipo], [grupo_id]) VALUES ('Productos en general','select nombre,descripcion from articulo where id=#FAMILIA#', 'básico',3);
INSERT INTO [dbo].[informes]([nombre], [query], [tipo], [grupo_id]) VALUES ('Marcas en general','SELECT m.id,m.nombre FROM marca m JOIN mar_emp_suc_usu_rol mes on  mes.sucursal_id is null and mes.empresa_id is null and  mes.marca_id=m.id and m.id=#MARCA# JOIN usuario u on mes.usuario_id=u.id and u.id=#USUARIOLOG#', 'básico',4);

INSERT INTO [dbo].[informes_sucursal]([id], [sucursal_id]) VALUES (10011,1);
INSERT INTO [dbo].[informes_sucursal]([id], [sucursal_id]) VALUES (10012,1);
INSERT INTO [dbo].[informes_sucursal]([id], [sucursal_id]) VALUES (10013,1);
INSERT INTO [dbo].[informes_sucursal]([id], [sucursal_id]) VALUES (10014,1);
INSERT INTO [dbo].[informes_sucursal]([id], [sucursal_id]) VALUES (10015,1);

INSERT INTO [dbo].[grupo_informes_sucursal]([id], [sucursal_id]) VALUES (1,1);
INSERT INTO [dbo].[grupo_informes_sucursal]([id], [sucursal_id]) VALUES (2,1);
INSERT INTO [dbo].[grupo_informes_sucursal]([id], [sucursal_id]) VALUES (3,1);
INSERT INTO [dbo].[grupo_informes_sucursal]([id], [sucursal_id]) VALUES (4,1);

INSERT INTO [dbo].[grupo_ayuda]([nombre],[tipo]) VALUES ('Grupo de Ayuda','Ayuda');
INSERT INTO [dbo].[ayuda]([grupo_ayuda_id],[nombre], [url_page]) VALUES (2,'Información de Ayuda','http://saraworld.com/');


GO