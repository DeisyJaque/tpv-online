CREATE TABLE [dbo].[impresora](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[margen_izq] [float] NULL,
	[margen_der] [float] NULL,
	[alto] [float] NULL,
	[ancho] [float] NULL,
	[id_caja][bigint] NULL,
	[borrado] [datetime2](7) NULL,
	[fecha_creacion] [datetime2](7) NULL,
	[fecha_espejo] [datetime2](7) NULL,
	[fecha_modificacion] [datetime2](7) NULL,
	[corte] [varchar](100) NOT NULL,
	[cancelado] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[impresora_ordenador](
	[id] [bigint] NOT NULL,
	[ordenador_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[ordenador_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[impresora_ordenador]  WITH CHECK ADD  CONSTRAINT [FK__impresora_id__id] FOREIGN KEY([id])
REFERENCES [dbo].[impresora] ([id])
ALTER TABLE [dbo].[impresora_ordenador] CHECK CONSTRAINT [FK__impresora_id__id]

ALTER TABLE [dbo].[impresora_ordenador]  WITH CHECK ADD  CONSTRAINT [FK__ordenador_id__ordenador_id] FOREIGN KEY([ordenador_id])
REFERENCES [dbo].[ordenador] ([id])
ALTER TABLE [dbo].[impresora_ordenador] CHECK CONSTRAINT [FK__ordenador_id__ordenador_id]

--Alter table para impresora pero debe ir en otro script
/*
--ALTER TABLE [dbo].[impresora] DROP COLUMN empresa_id;
ALTER TABLE [dbo].[impresora] ADD empresa_id [bigint] NULL;
--ALTER TABLE [dbo].[impresora] DROP COLUMN sucursal_id;
ALTER TABLE [dbo].[impresora] ADD sucursal_id [bigint] NULL;
	--ALTER TABLE [dbo].[impresora] DROP COLUMN superior;
ALTER TABLE [dbo].[impresora] ADD superior [float] NULL;
	--ALTER TABLE [dbo].[impresora] DROP COLUMN inferior;
ALTER TABLE [dbo].[impresora] ADD inferior [float] NULL;
	--ALTER TABLE [dbo].[impresora] DROP COLUMN tipo_impresora;
ALTER TABLE [dbo].[impresora] ADD tipo_impresora [varchar] NULL;
	--ALTER TABLE [dbo].[impresora] DROP COLUMN predeterminado;
ALTER TABLE [dbo].[impresora] ADD predeterminado [bit] NULL;

ALTER TABLE [dbo].[impresora] DROP COLUMN id_caja;

ALTER TABLE [dbo].[impresora]  WITH CHECK ADD  CONSTRAINT [FK__empresa_id__id] FOREIGN KEY([empresa_id])
REFERENCES [dbo].[empresa] ([id])

ALTER TABLE [dbo].[impresora]  WITH CHECK ADD  CONSTRAINT [FK__sucursal_id__id] FOREIGN KEY([sucursal_id])
REFERENCES [dbo].[sucursal] ([id])

*/
