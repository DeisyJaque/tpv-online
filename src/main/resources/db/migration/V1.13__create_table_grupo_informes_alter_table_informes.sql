CREATE TABLE [dbo].[grupo_informes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[tipo][varchar](100) NOT NULL,
	[rowguid] [uniqueidentifier] NULL,
	[fecha_creacion] [datetime2](7) NULL,
	[fecha_espejo] [datetime2](7) NULL,
	[borrado] [datetime2](7) NULL,
	[fecha_modificacion] [datetime2](7) NULL,
	
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[grupo_informes_sucursal](
	[id] [bigint] NOT NULL,
	[sucursal_id] [bigint] NOT NULL,
	[rowguid] [uniqueidentifier] NULL,
	[fecha_creacion] [datetime2](7) NULL,
	[fecha_espejo] [datetime2](7) NULL,
	[borrado] [datetime2](7) NULL,
	[fecha_modificacion] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[sucursal_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[informes_sucursal](
	[id] [bigint] NOT NULL,
	[sucursal_id] [bigint] NOT NULL,
	[rowguid] [uniqueidentifier] NULL,
	[fecha_creacion] [datetime2](7) NULL,
	[fecha_espejo] [datetime2](7) NULL,
	[borrado] [datetime2](7) NULL,
	[fecha_modificacion] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[sucursal_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--Foreign key de grupo_informes_sucursal a grupo_informes y sucursal 
ALTER TABLE [dbo].[grupo_informes_sucursal]  WITH CHECK ADD  CONSTRAINT [FK__grupo_informes_id__id] FOREIGN KEY([id])
REFERENCES [dbo].[grupo_informes] ([id])
ALTER TABLE [dbo].[grupo_informes_sucursal] CHECK CONSTRAINT [FK__grupo_informes_id__id]


ALTER TABLE [dbo].[grupo_informes_sucursal]  WITH CHECK ADD  CONSTRAINT [FK__sucursal_grupo_id__sucursal_id] FOREIGN KEY([sucursal_id])
REFERENCES [dbo].[sucursal] ([id])
ALTER TABLE [dbo].[grupo_informes_sucursal] CHECK CONSTRAINT [FK__sucursal_grupo_id__sucursal_id]

--Foreign key de informes_sucursal a grupo_informes y sucursal 
ALTER TABLE [dbo].[informes_sucursal]  WITH CHECK ADD  CONSTRAINT [FK__informes_id__id] FOREIGN KEY([id])
REFERENCES [dbo].[informes] ([id])
ALTER TABLE [dbo].[informes_sucursal] CHECK CONSTRAINT [FK__informes_id__id]


ALTER TABLE [dbo].[informes_sucursal]  WITH CHECK ADD  CONSTRAINT [FK__informes_sucursal_id__sucursal_id] FOREIGN KEY([sucursal_id])
REFERENCES [dbo].[sucursal] ([id])
ALTER TABLE [dbo].[informes_sucursal] CHECK CONSTRAINT [FK__informes_sucursal_id__sucursal_id]

--ALTER TABLE [dbo].[informes] DROP borrado;*/
ALTER TABLE [dbo].[informes] ADD borrado [datetime2] NULL;

--ALTER TABLE [dbo].[informes] DROP COLUMN grupo_informe_id;*/
ALTER TABLE [dbo].[informes] ADD grupo_id [bigint] NULL;

ALTER TABLE [dbo].[informes]  WITH CHECK ADD  CONSTRAINT [FK__grupo_id__id] FOREIGN KEY([grupo_id])
REFERENCES [dbo].[grupo_informes] ([id])


IF EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'informes'
    AND column_name = 'query'
)
ALTER TABLE dbo.informes ALTER COLUMN query VARCHAR (MAX) ; 

IF EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'informes'
    AND column_name = 'query'
)
EXEC sp_rename 'informes.query', 'query_tmp';  

GO


