
IF EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'forma_pago_empresa'
    AND column_name = 'id'
)
EXEC sp_rename 'forma_pago_empresa.id', 'id_forma_pago';

IF NOT EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'forma_pago_empresa'
    AND column_name = 'id'
)
ALTER TABLE dbo.forma_pago_empresa ADD id INT IDENTITY CONSTRAINT PK_id PRIMARY KEY CLUSTERED;

IF EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'forma_pago_sucursal'
    AND column_name = 'id'
)
EXEC sp_rename 'forma_pago_sucursal.id', 'id_forma_pago';  

IF NOT EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'forma_pago_sucursal'
    AND column_name = 'id'
)
ALTER TABLE dbo.forma_pago_sucursal ADD id INT IDENTITY CONSTRAINT PK_id_forma_pago_sucursal PRIMARY KEY CLUSTERED;

IF NOT EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'lotes_factura'
    AND column_name = 'id'
)
ALTER TABLE dbo.lotes_factura ADD id INT IDENTITY CONSTRAINT PK_id_lotes_factura PRIMARY KEY CLUSTERED;