CREATE TABLE tipo_registro_articulo(
	id bigint IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre varchar(100) NOT NULL,
	borrado datetime2(7) NULL,
	fecha_creacion datetime2(7) NULL,
	fecha_espejo datetime2(7) NULL,
	fecha_modificacion datetime2(7) NULL,
	rowguid uniqueidentifier NULL
);

INSERT INTO tipo_registro_articulo(nombre) VALUES ('Artículo');
INSERT INTO tipo_registro_articulo(nombre) VALUES ('Servicio');

ALTER TABLE articulo DROP COLUMN tipo;

ALTER TABLE articulo ADD tipo_registro_articulo_id bigint;
ALTER TABLE articulo ADD FOREIGN KEY (tipo_registro_articulo_id) REFERENCES tipo_registro_articulo(id);

