IF NOT EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'lotes'
    AND column_name = 'fecha_anular'
)
ALTER TABLE [dbo].[lotes] ADD fecha_anular [date] NULL;

IF NOT EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'lotes'
    AND column_name = 'anular'
)
ALTER TABLE [dbo].[lotes] ADD anular [bit];

IF NOT EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'venta'
    AND column_name = 'anular'
)
ALTER TABLE [dbo].[venta] ADD anular [bit];

--Modificación para colocar el campo observaciones sin limite de caracteres
ALTER TABLE cliente ALTER COLUMN observaciones VARCHAR(max);

--Se agrega el campo menor_edad a la tabla cliente
ALTER TABLE cliente ADD menor_edad bit NOT NULL DEFAULT 0;

--Modificación para colocar el campo titulo sin limite de caracteres
ALTER TABLE agenda ALTER COLUMN titulo VARCHAR(max);

--Modificación para colocar el campo servicios sin limite de caracteres
ALTER TABLE agenda ALTER COLUMN servicios VARCHAR(max);

--Modificación para colocar el campo alerta sin limite de caracteres
ALTER TABLE agenda ALTER COLUMN alerta VARCHAR(max);

--Se quita la restricción de NO NULL en los campos de duracion de la tabla Coleccion
ALTER TABLE coleccion ALTER COLUMN duracion_inicio date NULL;

ALTER TABLE coleccion ALTER COLUMN duracion_fin date NULL;