ALTER TABLE [dbo].[informes] ADD sucursales_incluidos [integer] NULL;
ALTER TABLE [dbo].[informes] ADD sucursales_excluidos [VARCHAR](500) NULL;

ALTER TABLE [dbo].[grupo_informes] ADD sucursales_incluidos [integer] NULL;
ALTER TABLE [dbo].[grupo_informes] ADD sucursales_excluidos [VARCHAR](500) NULL;

ALTER TABLE [dbo].[grupo_informes] ADD nivel [integer] NULL;
ALTER TABLE [dbo].[informes] ADD nivel [integer] NULL;

IF EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'informes'
    AND column_name = 'query'
)
ALTER TABLE dbo.informes ALTER COLUMN query VARCHAR (MAX) ; 

IF EXISTS 
(
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = 'informes'
    AND column_name = 'query'
)
EXEC sp_rename 'informes.query', 'query_tmp';  



GO
