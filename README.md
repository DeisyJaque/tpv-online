# TPV Online

Proyecto realizado para compañía Saraworld de sistema de Punto de Venta Online. Módulos: Bitácora, Informes, Ventas (secciones de Clientes, Familia, Productos y Pendientes), Facturación, Ayuda, Formas de Pago

Para este proyecto trabajé tanto en el front-end de los módulos como en el back-end para el funcionamiento de cada uno, así como con la manipulación de los datos con SQL Server.